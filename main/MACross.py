# 2018/08/31 Initial to use pandas, matplotlib,TAlib
# 2018/09/01 Using pandas to calculate MA
#            Calculate golden and dead MA
#            Add class PandasDataAnalysis
#            Adapte from test_TALib.py
# 2018/09/02 Locate to main folder for excelfile03 #"景氣循環追蹤股"
# 2018/09/07 Add plotMAchart to log folder
########################################################

# -- coding: utf-8 --
import talib
import pandas as pd
import matplotlib.pyplot  as plt
import numpy as np
from datetime import datetime, timedelta
import time
import os
import sys

strabspath=os.path.abspath(__file__)
strdirname=os.path.dirname(strabspath)
str_split=os.path.split(strdirname)
prevdirname=str_split[0]
dirnamelib = os.path.join(prevdirname,"lib")
dirnamelog = os.path.join(prevdirname,"log")
dirdatafolder = os.path.join(prevdirname,'test','data')
    
sys.path.append(dirnamelib)

import readConfig as readConfig
import excelRW as excelrw
import dataAnalysis as data_analysis

configPath=os.path.join(strdirname,"config.ini")
localReadConfig = readConfig.ReadConfig(configPath)

excel_file01 = localReadConfig.get_SeymourExcel("excelfile01") #"循環投資追蹤股"
excel_file02 = localReadConfig.get_SeymourExcel("excelfile02") #"波段投機追蹤股"
excel_file03 = localReadConfig.get_SeymourExcel("excelfile03") #"景氣循環追蹤股"
excel_file04 = localReadConfig.get_SeymourExcel("excelfile04") #"公用事業追蹤股"
list_excel_Seymour = [excel_file03]

FOLDER = 'data'
#stkidx = '2617'#台航

list_all_stockidx = []
# get all stock idx from excelfile03 #"景氣循環追蹤股"
localexcelrw = excelrw.ExcelRW()
list_all_stockidx=localexcelrw.get_all_stockidx_SeymourExcel(dirnamelog,list_excel_Seymour)

for stkidx in list_all_stockidx:
    print('\n將讀取 {}/{}.csv 的資料作 90Days MA CrossOver分析。'.format(dirdatafolder,stkidx[0]))
    localDA = data_analysis.PandasDataAnalysis(stkidx[0],dirnamelog,dirdatafolder)
    localDA.MACrossOver()

for stkidx in list_all_stockidx:
    print('\n將讀取 {}/{}.csv 的資料作 90Days MA CrossOver圖形。'.format(dirdatafolder,stkidx[0]))
    localDA = data_analysis.PandasDataAnalysis(stkidx[0],dirnamelog,dirdatafolder)
    localDA.plotMACrossOver()    