## 2018/08/09 Modify test_twstock03.ipynb
## 2018/08/10 Modify test_3accounts .ipynb to .py
## 2018/08/11 Modify test_3accounts.py to test_GSpreadAmy1210.py
## 2018/08/12 Add def update_sheet_celllist
##            Add def update_sheet_insertrow
##            Add def  update_GSpreadworksheet
## 2018/08/13 prevent ErrorCode:429, Exhaust Resoure
## 2018/08/14 import module from googleSS.py
## 2018/08/16 Add def auth_gss_client() and def open_GSworksheet() to googleSS.py
## 2018/10/17 Update update_GSpreadworksheet_datafolderCSV to solve 
##              6024 群益期 that can't get close by def update_GSpreadworksheet
#################################################################################
# Import library to know the time
import time,datetime

# Import personal library
import os
import sys

strabspath=os.path.abspath(__file__)
strdirname=os.path.dirname(strabspath)
str_split=os.path.split(strdirname)
prevdirname=str_split[0]
dirnamelib=os.path.join(prevdirname,"lib")
dirnamelog=os.path.join(prevdirname,"log")
dirdatafolder = os.path.join(prevdirname,'test','data')

sys.path.append(dirnamelib)

import googleSS as googleSS
import readConfig as readConfig

configPath=os.path.join(strdirname,"config.ini")
localReadConfig = readConfig.ReadConfig(configPath)
spread01 = localReadConfig.get_GSpread("sheet01")
spread02 = localReadConfig.get_GSpread("sheet02")

worksheet_spread01 = localReadConfig.get_WorkSheet_Amy1210("worksheet01")
worksheet_spread02 = localReadConfig.get_WorkSheet_Amy1210("worksheet02")#"""Phli_IBIT"""
worksheet_spread03 = localReadConfig.get_WorkSheet_Amy1210("worksheet03")#"""Phil_SKC"""
delay_sec = localReadConfig.get_WorkSheet_Amy1210("delay_sec")
#2018/11/17(Sat) excute this code meet from Mon. to Fri unreamrk below.
str_first_year_month_day = datetime.date.today().strftime('%Y,%m,%d')# ex:2018,10,16 get today date form system
#2018/11/17(Sat) excute this code dosen't meet from Mon. to Fri unremak below.
#str_first_year_month_day = localReadConfig.get_SeymourExcel("first_year_month_day")

GDriveJSON = 'PythonGSheetUpload.json'
GSpreadSheet = spread02
print('將讀取試算表' ,GSpreadSheet , '的資料')

# Get present time
local_time = time.localtime(time.time())
print('Start Time is ', local_time.tm_year,'/',local_time.tm_mon,'/',local_time.tm_mday,' ',local_time.tm_hour,":",local_time.tm_min,":",local_time.tm_sec)

scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
# Declare GoogleSS() from googleSS.py
localGoogleSS=googleSS.GoogleSS()
localGoogleSS.auth_gss_client(GDriveJSON, scope)

list_worksheet_spread = [worksheet_spread03,worksheet_spread02,worksheet_spread01]
#list_worksheet_spread = [worksheet_spread02,worksheet_spread01]
#list_worksheet_spread = [worksheet_spread01]

for worksheet_spread in list_worksheet_spread:
    localGoogleSS.open_GSworksheet(GSpreadSheet,worksheet_spread)

    print('將讀取及更新試算表', GSpreadSheet, '中WorkSheet:', worksheet_spread, '的資料')
    #inital row count value 2
    row_count = 2
    
    #2018/10/17 Update update_GSpreadworksheet_datafolderCSV to solve 
    #           6024 群益期 that can't get close by def update_GSpreadworksheet
    #localGoogleSS.update_GSpreadworksheet(row_count,delay_sec)
    localGoogleSS.update_GSpreadworksheet_datafolderCSV(row_count,delay_sec,dirnamelog,dirdatafolder,\
                                                        str_first_year_month_day)

    # Get the last time
    final_time = time.localtime(time.time())
    str_FinalTime = 'Final Time is ' + str(final_time.tm_year) + '/' + str(final_time.tm_mon) + '/' + str(
        final_time.tm_mday) + ' ' + str(final_time.tm_hour) + ":" + str(final_time.tm_min) + ":" + str(
        final_time.tm_sec)
    print(str_FinalTime)

    final_time = time.localtime(time.time())