# 2018/08/31 Initial to use pandas, matplotlib,TAlib
# 2018/09/01 Using pandas to calculate MA
#            Calculate golden and dead MA
#            Add class PandasDataAnalysis
#            Slove  matplotlib to show Chinese charter
# 2018/09/02 Match '---' and '--' in close price by using regular express
#            Caluclate uprate and downrate by rolling max min of dataframe
# 2018/09/03 add def file1_updownrate_LastMonthYear()
# 2018/09/04 dtype of close price icluding '---' and '--' is object except float64
#            Save as test_SeymourTarget.py
#            Add def file1_updownrate_LastMonthYear()
# 2018/09/05 Add def get_tradedays_dfinfo () and def file2_updownrate_QuarterYear
#            in class PandasDataAnalysis
#            Save as test_SeymourTarget.py
# 2018/09/06 Debug output CSV dulplicated rows.
#            Add def file2_updownrate_threeYearoneYear in class PandasDataAnalysis
# 2018/09/14 Add def file1_call, file1_put, file2_call, file2_put nad percent2float
# 2018/09/15 Add def file3_call and file3_put
#            Adapte from test_SeymourTarget.py
# 2018/09/19 Update from test_SeymourTarget.py again
# 2018/09/21 Update file3 #"景氣循環追蹤股" routine from test_SeymourTarget.py
# 2018/09/24 Add excute file4 #"公用事業追蹤股"
# 2018/10/02 Add  to  delete prvious candle stick jpg files routine.
#########################################################################

# -- coding: utf-8 --
import talib
import pandas as pd
import matplotlib.pyplot  as plt
import numpy as np
from datetime import datetime, timedelta
import time
import os
import sys
import matplotlib.pylab as mpl
#2018/11/19 Only available on matplotlib 2.x
mpl.rcParams['font.sans-serif'] = ['SimHei'] #將預設字體改用SimHei字體for中文

strabspath=os.path.abspath(__file__)
strdirname=os.path.dirname(strabspath)
str_split=os.path.split(strdirname)
prevdirname=str_split[0]
dirnamelib=os.path.join(prevdirname,"lib")
dirnamelog=os.path.join(prevdirname,"log")
dirdatafolder = os.path.join(prevdirname,'test','data')

sys.path.append(dirnamelib)
import readConfig as readConfig
import excelRW as excelrw
import dataAnalysis as data_analysis
import googleDrive as google_drive

if __name__ == '__main__':

    configPath=os.path.join(strdirname,"config.ini")
    localReadConfig = readConfig.ReadConfig(configPath)

    excel_file01 = localReadConfig.get_SeymourExcel("excelfile01") #"循環投資追蹤股"
    excel_file02 = localReadConfig.get_SeymourExcel("excelfile02") #"波段投機追蹤股"
    excel_file03 = localReadConfig.get_SeymourExcel("excelfile03") #"景氣循環追蹤股"
    excel_file04 = localReadConfig.get_SeymourExcel("excelfile04") #"公用事業追蹤股"
    stkidx_call_file01 = localReadConfig.get_SeymourExcel('stkidx_call_file01')
    stkidx_put_file01 = localReadConfig.get_SeymourExcel('stkidx_put_file01')
    stkidx_call_file02 = localReadConfig.get_SeymourExcel('stkidx_call_file02')
    stkidx_put_file02 = localReadConfig.get_SeymourExcel('stkidx_put_file02')
    stkidx_call_file03 = localReadConfig.get_SeymourExcel('stkidx_call_file03')
    stkidx_put_file03 = localReadConfig.get_SeymourExcel('stkidx_put_file03')
    stkidx_call_file04 = localReadConfig.get_SeymourExcel('stkidx_call_file04')
    stkidx_put_file04 = localReadConfig.get_SeymourExcel('stkidx_put_file04')
    str_first_year_month_day = localReadConfig.get_SeymourExcel("first_year_month_day")
    str_color_ma = localReadConfig.get_SeymourExcel('color_ma05_ma20_ma30')
    list_color_ma = str_color_ma.split(',')
    str_candlestick_weekly_subfolder = localReadConfig.get_SeymourExcel("candlestick_weekly_subfolder")

    #2018/10/31 remark casuse purge jpg files in def plotCandlestickandMA() 
    #Delete prvious candle stick jpg files.
    ###############################
    #str_candlestick_filepath=os.path.join(dirnamelog,str_candlestick_weekly_subfolder)
    #localgoogle_drive = google_drive.GoogleCloudDrive(str_candlestick_filepath)
    #re_exp = r'\.jpg$'
    #list_filename = localgoogle_drive.querylocalfiles(re_exp)
    #localgoogle_drive.purgelocalfiles(re_exp)

    ###############################
    # excute file1 #"循環投資追蹤股"
    ###############################
    list_excel_Seymour = [excel_file01]
    list_stkidx_call_file01 = stkidx_call_file01.split(',')
    list_stkidx_put_file01 = stkidx_put_file01.split(',')    
    
    #str_dirlogcsv_file01 = os.path.join(dirnamelog,'924循環投資追蹤bothprices.csv')
    #str_dirlogcsv_file01 = file1_main(list_excel_Seymour,dirnamelog,dirdatafolder)
    str_dirlogcsv_file01 = data_analysis.file1_main(list_excel_Seymour,dirnamelog,dirdatafolder,str_first_year_month_day)
    
    str_buysell_opt = 'call_循環'
    localdata_analysis = data_analysis.PandasDA_Excel(dirnamelog,list_excel_Seymour)

    # to filter whick stock to buy or to sell #"循環投機追蹤股"
    df_file01_stock_call = data_analysis.file1_call(str_dirlogcsv_file01)
    
    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA(df_file01_stock_call,dirnamelog,dirdatafolder,str_first_year_month_day,
                            list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)    
    
    print('Seymour suggests to buy in at {}:'.format(excel_file01))
    df_rtu = localdata_analysis.SeymourExcel01_call()
    df_file01_Seymour_call = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_call_file01,df_rtu)
    
    #to get stock index by diff df_file03_Seymour_call btw df_file03_stock_call
    df_file01_Seymour_call_compare = localdata_analysis.compare_twoarrarys(df_file01_Seymour_call,df_file01_stock_call)
    #print(df_file01_Seymour_call)

    # and then plot Candlestick and MA cruve if
    if len(df_file01_Seymour_call_compare) > 0:
        data_analysis.file_plotCandlestickMA(df_file01_Seymour_call_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    str_buysell_opt = 'put_循環'
    # to filter whick stock to buy or to sell #"循環投機追蹤股"
    df_file01_stock_put = data_analysis.file1_put(str_dirlogcsv_file01)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA(df_file01_stock_put,dirnamelog,dirdatafolder,str_first_year_month_day,
                            list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to sell out at {}:'.format(excel_file01))
    df_rtu = localdata_analysis.SeymourExcel01_put()
    df_file01_Seymour_put = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_put_file01,df_rtu)
    
    #to get stock index by diff df_file03_Seymour_put btw df_file03_stock_put
    df_file01_Seymour_put_compare = localdata_analysis.compare_twoarrarys(df_file01_Seymour_put,df_file01_stock_put)

    # and then plot Candlestick and MA cruve if
    if len(df_file01_Seymour_put_compare) > 0:
        data_analysis.file_plotCandlestickMA(df_file01_Seymour_put_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)
    ###############################
    # excute file2 #"波段投機追蹤股"
    ###############################
    list_excel_Seymour = [excel_file02]
    list_stkidx_call_file02 = stkidx_call_file02.split(',')#['2376','3450','3515','4144','4746','5388','6261','6271','8039','8042']
    list_stkidx_put_file02 = stkidx_put_file02.split(',')#['2439','2488','3653','5371','6202']
    
    #str_dirlogcsv_file02 = os.path.join(dirnamelog,'924波段投機追蹤bothprices.csv')
    str_dirlogcsv_file02 = data_analysis.file2_main(list_excel_Seymour,dirnamelog,dirdatafolder,str_first_year_month_day)
    
    str_buysell_opt = 'call_波段'
    localdata_analysis = data_analysis.PandasDA_Excel(dirnamelog,list_excel_Seymour)
    
    # to filter whick stock to buy or to sell #"波段投機追蹤股"
    df_file02_stock_call = data_analysis.file2_call(str_dirlogcsv_file02)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA(df_file02_stock_call,dirnamelog,dirdatafolder,str_first_year_month_day,
                                list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('Seymour suggests to buy in at {}:'.format(excel_file02))
    df_rtu = localdata_analysis.SeymourExcel02_call()
    df_file02_Seymour_call = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_call_file02,df_rtu)

    #to get stock index by diff df_file03_Seymour_call btw df_file03_stock_call
    df_file02_Seymour_call_compare = localdata_analysis.compare_twoarrarys(df_file02_Seymour_call,df_file02_stock_call)
    #print(df_file02_Seymour_call)

    # and then plot Candlestick and MA cruve if
    if len(df_file02_Seymour_call_compare) > 0:
        data_analysis.file_plotCandlestickMA(df_file02_Seymour_call_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)        

    str_buysell_opt = 'put_波段'
    # to filter whick stock to buy or to sell #"波段投機追蹤股"
    df_file02_stock_put = data_analysis.file2_put(str_dirlogcsv_file02)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA(df_file02_stock_put,dirnamelog,dirdatafolder,str_first_year_month_day,
                            list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)
    
    print('\nSeymour suggests to sell out at {}:'.format(excel_file02))
    df_rtu = localdata_analysis.SeymourExcel02_put()
    df_file02_Seymour_put = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_put_file02,df_rtu)
    
    #to get stock index by diff df_file03_Seymour_put btw df_file03_stock_put
    df_file02_Seymour_put_compare = localdata_analysis.compare_twoarrarys(df_file02_Seymour_put,df_file02_stock_put)

    # and then plot Candlestick and MA cruve if
    if len(df_file02_Seymour_put_compare) > 0:
        data_analysis.file_plotCandlestickMA(df_file02_Seymour_put_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)


    ###############################
    # excute file3 #"景氣循環追蹤股"
    ###############################
    list_excel_Seymour = [excel_file03]
    list_stkidx_call_file03 = stkidx_call_file03.split(',')
    list_stkidx_put_file03 = stkidx_put_file03.split(',')    

    #str_dirlogcsv_file03 = os.path.join(dirnamelog,'921景氣循環追蹤bothprices.csv')
    str_dirlogcsv_file03 = data_analysis.file3_main(list_excel_Seymour,dirnamelog,dirdatafolder,str_first_year_month_day)

    str_buysell_opt = 'call_景氣'
    localdata_analysis = data_analysis.PandasDA_Excel(dirnamelog,list_excel_Seymour)
    
    # to filter whick stock to buy or to sell #"景氣循環追蹤股"
    df_file03_stock_call = data_analysis.file3_call(str_dirlogcsv_file03)
    
    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA(df_file03_stock_call,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to buy in at {}:'.format(excel_file03))
    df_rtu = localdata_analysis.SeymourExcel03_call()
    df_file03_Seymour_call = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_call_file03,df_rtu)

    #to get stock index by diff df_file03_Seymour_call btw df_file03_stock_call
    df_file03_Seymour_call_compare = localdata_analysis.compare_twoarrarys(df_file03_Seymour_call,df_file03_stock_call)
    #print(df_file03_Seymour_call)

    # and then plot Candlestick and MA cruve if
    if len(df_file03_Seymour_call_compare) > 0:
        data_analysis.file_plotCandlestickMA(df_file03_Seymour_call_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)                                    

    str_buysell_opt = 'put_景氣'
    # to filter whick stock to buy or to sell #"景氣循環追蹤股"
    df_file03_stock_put = data_analysis.file3_put(str_dirlogcsv_file03)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA(df_file03_stock_put,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to sold out at {}:'.format(excel_file03))
    df_rtu = localdata_analysis.SeymourExcel03_put()
    df_file03_Seymour_put = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_put_file03,df_rtu)

    #to get stock index by diff df_file03_Seymour_put btw df_file03_stock_put
    df_file03_Seymour_put_compare = localdata_analysis.compare_twoarrarys(df_file03_Seymour_put,df_file03_stock_put)

    # and then plot Candlestick and MA cruve if
    if len(df_file03_Seymour_put_compare) > 0:
        data_analysis.file_plotCandlestickMA(df_file03_Seymour_put_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    ###############################
    # excute file4 #"公用事業追蹤股"
    ###############################
    list_excel_Seymour = [excel_file04]
    list_stkidx_call_file04 = stkidx_call_file04.split(',')
    list_stkidx_put_file04 = stkidx_put_file04.split(',')    

    #str_dirlogcsv_file04 = os.path.join(dirnamelog,'924公用事業追蹤bothprices.csv')
    str_dirlogcsv_file04 = data_analysis.file4_main(list_excel_Seymour,dirnamelog,dirdatafolder,str_first_year_month_day)
    
    str_buysell_opt = 'call_公用'
    localdata_analysis = data_analysis.PandasDA_Excel(dirnamelog,list_excel_Seymour)
    
    # to filter whick stock to buy or to sell #"景氣循環追蹤股"
    df_file04_stock_call = data_analysis.file4_call(str_dirlogcsv_file04)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA(df_file04_stock_call,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to buy in at {}:'.format(excel_file04))
    df_rtu = localdata_analysis.SeymourExcel04_call()
    df_file04_Seymour_call = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_call_file04,df_rtu)

    #to get stock index by diff df_file04_Seymour_call btw df_file04_stock_call
    df_file04_Seymour_call_compare = localdata_analysis.compare_twoarrarys(df_file04_Seymour_call,df_file04_stock_call)
    #print(df_file03_Seymour_call)

    # and then plot Candlestick and MA cruve if
    if len(df_file04_Seymour_call_compare) > 0:
        data_analysis.file_plotCandlestickMA(df_file04_Seymour_call_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    str_buysell_opt = 'put_公用'
    # to filter whick stock to buy or to sell #"公用事業追蹤股"
    df_file04_stock_put = data_analysis.file4_put(str_dirlogcsv_file04)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA(df_file04_stock_put,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to sold out at {}:'.format(excel_file04))
    df_rtu = localdata_analysis.SeymourExcel04_put()
    df_file04_Seymour_put = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_put_file04,df_rtu)

    #to get stock index by diff df_file04_Seymour_put btw df_file04_stock_put
    df_file04_Seymour_put_compare = localdata_analysis.compare_twoarrarys(df_file04_Seymour_put,df_file04_stock_put)

    # and then plot Candlestick and MA cruve if
    if len(df_file04_Seymour_put_compare) > 0:
        data_analysis.file_plotCandlestickMA(df_file04_Seymour_put_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)                                   