# 2018/08/23 Move to .\main to do trace code
# 2018/08/25 Treat case "trade volume zero"
######################################
import os
from datetime import datetime
import sys

FOLDER = 'data'

def string_to_time(string):
    year, month, day = string.split('/')
    return datetime(int(year) + 1911, int(month), int(day))

def is_same(row1, row2):
    if not len(row1) == len(row2):
        return False

    for index in range(len(row1)):
        if row1[index] != row2[index]:
            return False
    else:
        return True

def main():
    file_names = os.listdir(FOLDER)
    for file_name in file_names:
        if not file_name.endswith('.csv'):
            continue

        dict_rows = {}

        # Load and remove duplicates (use newer)
        with open('{}/{}'.format(FOLDER, file_name), 'r') as file:
            for line in file.readlines():
                dict_rows[line.split(',', 1)[0]] = line
                

        # Sort by date
        rows = [row for date, row in sorted(
            dict_rows.items(), key=lambda x: string_to_time(x[0]))]
            # 2015/08/21 date shift to column 3
            #dict_rows.items(), key=lambda x: string_to_time(x[2]))]

        with open('{}/{}'.format(FOLDER, file_name), 'w') as file:
            file.writelines(rows)

def main_test():
    file_names = os.listdir(FOLDER)
    for file_name in file_names:
        if not file_name.endswith('.csv'):
            continue

        dict_rows = {}

        # Load and remove duplicates (use newer)
        with open('{}/{}'.format(FOLDER, file_name), 'r') as file:
            for line in file.readlines():
                #print(line.split(',', 1)[0])
                dict_rows[line.split(',', 1)[0]] = line
                #print(dict_rows)

        # Sort by date
        rows = [row for date, row in sorted(
            dict_rows.items(), key=lambda x: string_to_time(x[0]))]
        
        # trace code purpose
        #for key,value in dict_rows.items():
        #    print(key, value)
        #for row in rows:
        #    print(row)

        #with open('{}/{}'.format(FOLDER, file_name), 'w') as file:
        #    file.writelines(rows)

def flatten_list(input_list):
    new_list = []
    for i in range(len(input_list)):
        inp = input_list[i]
        if inp.__class__ == list:
            contains_list = any(map(lambda x: x.__class__ == list, inp))
            if contains_list:
                flatten_list(inp, new_list)
            else:
                new_list.extend(inp)
        else:
            new_list.append(inp)

    return new_list        

if __name__ == '__main__':
    strabspath=os.path.abspath(__file__)
    strdirname=os.path.dirname(strabspath)
    str_split=os.path.split(strdirname)
    prevdirname=str_split[0]
    dirnamelib=os.path.join(prevdirname,"lib")
    dirnamelog=os.path.join(prevdirname,"log")
    
    sys.path.append(dirnamelib)

    #main()
    #main_test()

    import twstockInfo as twstockInfo

    excel_file = '1258'# '1535' #'9951'
    list_excel_file = ['{}/{}.csv'.format(FOLDER, excel_file)]
    #print(list_excel_file)

    # declare
    localcrawlTSEOTC=twstockInfo.CrawlTSEOTC(list_excel_file)
    list_sortedbydate=localcrawlTSEOTC.get_stockprice_datafoldercsv()
    list_sortedbydate_withoutfinalprice=localcrawlTSEOTC.exclude_nofinalprice_stockprice_datafoldercsv()

    # two neted lists difference
    # lst = [[1, 2], [3, 4], [1, 2], [5, 6], [8, 3], [2, 7]]
    #  sublst = [[1, 2], [8, 3]]
    # filter(lambda x:x not in sublst,lst) 
    # Out: [[3, 4], [5, 6], [2, 7]]
    #[x for x in list_sortedbydate if x not in list_sortedbydate_withoutZeroTradeVolume]
    filter_diff_sortedbydate = list(filter(lambda x:x not in list_sortedbydate_withoutfinalprice, list_sortedbydate)) 
    print(filter_diff_sortedbydate)
    print(len(filter_diff_sortedbydate))

    print(len(list_sortedbydate))       
    print(len(list_sortedbydate_withoutfinalprice))
    #print(list_sortedbydate[-59:-1])
    #print(list_sortedbydate[-1])
    print("{}{} final price of current day {}: {}".format(list_sortedbydate[-1][-2],
                                                            list_sortedbydate[-1][-1],
                                                            list_sortedbydate[-1][0],
                                                            list_sortedbydate[-1][1]))

    list_min_max_current_month = localcrawlTSEOTC.get_MinMax_byMonth_fromLastday()
    list_min_max_current_quarter = localcrawlTSEOTC.get_MinMax_byQuarter_fromLastday()
    list_min_max_current_year = localcrawlTSEOTC.get_MinMax_byYaer_fromLastday()
    

    print("Final price of Min, Max, Current by month from last day: {}, {}, {}".format(list_min_max_current_month[0],
                                                                                list_min_max_current_month[1],
                                                                                list_min_max_current_month[2] ))

    print("Final price of Min, Max, Current by quarter from last day: {}, {}, {}".format(list_min_max_current_quarter[0],
                                                                                list_min_max_current_quarter[1],
                                                                                list_min_max_current_quarter[2]) )

    print("Final price of Min, Max, Current by year from last day: {}, {}, {}".format(list_min_max_current_year[0],
                                                                                list_min_max_current_year[1],
                                                                                list_min_max_current_year[2]) )

    list_rate_month = localcrawlTSEOTC.stockprice_raiserate_decreaserate(list_min_max_current_month)
    list_rate_quarter = localcrawlTSEOTC.stockprice_raiserate_decreaserate(list_min_max_current_quarter)
    list_rate_year =  localcrawlTSEOTC.stockprice_raiserate_decreaserate(list_min_max_current_year)

    print("Raise/Decrease Rate by month: {:.3%} / {:.3%}".format(list_rate_month[0],list_rate_month[1]))
    print("Raise/Decrease Rate by quarter: {:.3%} / {:.3%}".format(list_rate_quarter[0],list_rate_quarter[1]))
    print("Raise/Decrease Rate by year: {:.3%} / {:.3%}".format(list_rate_year[0],list_rate_year[1]))    

    #print(len(list_sortedbydate[-59:-1]))
    #print(len(list_sortedbydate[-250:-1]))

    