# 2018/08/31 Initial to use pandas, matplotlib,TAlib
# 2018/09/01 Using pandas to calculate MA
#            Calculate golden and dead MA
#            Add class PandasDataAnalysis
#            Slove  matplotlib to show Chinese charter
# 2018/09/02 Match '---' and '--' in close price by using regular express
#            Caluclate uprate and downrate by rolling max min of dataframe
# 2018/09/03 add def file1_updownrate_LastMonthYear()
# 2018/09/04 dtype of close price icluding '---' and '--' is object except float64
#            Save as test_SeymourTarget.py
#            Add def file1_updownrate_LastMonthYear()
# 2018/09/05 Add def get_tradedays_dfinfo () and def file2_updownrate_QuarterYear
#            in class PandasDataAnalysis
#            Save as test_SeymourTarget.py
# 2018/09/06 Debug output CSV dulplicated rows.
#            Add def file2_updownrate_threeYearoneYear in class PandasDataAnalysis
# 2018/09/14 Add def file1_call, file1_put, file2_call, file2_put nad percent2float
# 2018/09/15 Add def file3_call and file3_put
# 2018/10/28 Adapte from test_SeymourTarget.py
# 2018/12/23 Adapte from test_test_SMTargetSqilte.py
# 2019/02/16 update to 低波固收 filter citeria
#########################################################################

# -- coding: utf-8 --
from datetime import datetime, timedelta
import time, os, sys, re
from io import StringIO

import logging,datetime
import urllib.request
from lxml import html
import httplib2
from apiclient import discovery
import matplotlib.pylab as mpl
#2018/11/19 Only available on matplotlib 2.x
mpl.rcParams['font.sans-serif'] = ['SimHei'] #將預設字體改用SimHei字體for中文

class Flag:
    auth_host_name = 'localhost'
    noauth_local_webserver = False
    auth_host_port = [8080, 8090]
    logging_level = 'ERROR'

try:
    import argparse

    # flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
    flags = Flag()
except ImportError:
    flags = None

strabspath=os.path.abspath(__file__)
strdirname=os.path.dirname(strabspath)
str_split=os.path.split(strdirname)
prevdirname=str_split[0]
dirnamelib=os.path.join(prevdirname,"lib")
dirnamelog=os.path.join(prevdirname,"log")
dirdatafolder = os.path.join(prevdirname,'test','data')

sys.path.append(dirnamelib)

import readConfig as readConfig
import excelRW as excelrw
import dataAnalysis as data_analysis
import googleDrive as google_drive


if __name__ == '__main__':
    
    configPath=os.path.join(strdirname,"config.ini")
    localReadConfig = readConfig.ReadConfig(configPath)

    stkidx_call_file01 = localReadConfig.get_SeymourExcel('stkidx_call_file01')
    stkidx_put_file01 = localReadConfig.get_SeymourExcel('stkidx_put_file01')
    stkidx_call_file02 = localReadConfig.get_SeymourExcel('stkidx_call_file02')
    stkidx_put_file02 = localReadConfig.get_SeymourExcel('stkidx_put_file02')
    stkidx_call_file03 = localReadConfig.get_SeymourExcel('stkidx_call_file03')
    stkidx_put_file03 = localReadConfig.get_SeymourExcel('stkidx_put_file03')
    stkidx_call_file04 = localReadConfig.get_SeymourExcel('stkidx_call_file04')
    stkidx_put_file04 = localReadConfig.get_SeymourExcel('stkidx_put_file04')
    str_first_year_month_day = localReadConfig.get_SeymourExcel("first_year_month_day")
    str_color_ma = localReadConfig.get_SeymourExcel('color_ma05_ma20_ma30')
    list_color_ma = str_color_ma.split(',')
    str_candlestick_weekly_subfolder = localReadConfig.get_SeymourExcel("candlestick_weekly_subfolder")

    #excel_file01 = localReadConfig.get_SeymourExcel("excelfile01") #"循環投資追蹤股"
    #excel_file02 = localReadConfig.get_SeymourExcel("excelfile02") #"波段投機追蹤股"
    #excel_file03 = localReadConfig.get_SeymourExcel("excelfile03") #"景氣循環追蹤股"
    #excel_file04 = localReadConfig.get_SeymourExcel("excelfile04") #"公用事業追蹤股"
    print("Check latest Excel files name from Money Hunter blog....")
    url_moneyhunter =localReadConfig.get_SeymourExcel('url_moneyhunterblog')#'http://twmoneyhunter.blogspot.com/'
    #xpath_url_file01 = '//*[@id="LinkList1"]/div/ul/li[4]/a/@href'#循環投資
    #xpath_url_file02 = '//*[@id="LinkList1"]/div/ul/li[3]/a/@href'#波段投機
    #xpath_url_file03 = '//*[@id="LinkList1"]/div/ul/li[5]/a/@href'#景氣循環
    #xpath_url_file04 = '//*[@id="LinkList1"]/div/ul/li[6]/a/@href'#公用事業
    #
    #2019/09/01 MoneyHuter blog webpage layout reversion, value of xpath changed
    #穩定成長，3;指數ETF, 4;債券ETF, 5;波段投機, 6;循環投資, 7;景氣循環, 8
    xpath_url_file01 = '//*[@id="LinkList1"]/div/ul/li[7]/a/@href'#循環投資
    xpath_url_file02 = '//*[@id="LinkList1"]/div/ul/li[6]/a/@href'#波段投機
    xpath_url_file03 = '//*[@id="LinkList1"]/div/ul/li[8]/a/@href'#景氣循環
    xpath_url_file04 = '//*[@id="LinkList1"]/div/ul/li[3]/a/@href'#穩定成長

    #Python urllib urlopen not working
    #https://stackoverflow.com/questions/25863101/python-urllib-urlopen-not-working
    ###########################################
    with urllib.request.urlopen(url_moneyhunter) as response:
        raw = response.read()
    html_doc = html.fromstring(raw)

    credential_dir = os.getcwd()

    localgoogle_drive = google_drive.GoogleDrivebyFileID(dirnamelog,flags)
    credentials = localgoogle_drive.get_credentials(credential_dir)
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)

    fileid_file01 = localgoogle_drive.ggdrive_fileid(html_doc,xpath_url_file01)
    fileid_file02 = localgoogle_drive.ggdrive_fileid(html_doc,xpath_url_file02)
    fileid_file03 = localgoogle_drive.ggdrive_fileid(html_doc,xpath_url_file03)
    fileid_file04 = localgoogle_drive.ggdrive_fileid(html_doc,xpath_url_file04)

    list_xlsfile, excel_file01 = localgoogle_drive.check_xlsfile_MHunterblog_logfolder(service,fileid_file01,dirnamelog)#"循環投資追蹤股"
    list_xlsfile, excel_file02 = localgoogle_drive.check_xlsfile_MHunterblog_logfolder(service,fileid_file02,dirnamelog)#"波段投機追蹤股"
    list_xlsfile, excel_file03 = localgoogle_drive.check_xlsfile_MHunterblog_logfolder(service,fileid_file03,dirnamelog)#"景氣循環追蹤股"
    list_xlsfile, excel_file04 = localgoogle_drive.check_xlsfile_MHunterblog_logfolder(service,fileid_file04,dirnamelog)#"公用事業追蹤股"

    #2018/10/31 remark casuse purge jpg files in def plotCandlestickandMA() 
    #Delete prvious candle stick jpg files.
    ###############################
    str_candlestick_filepath=os.path.join(dirnamelog,str_candlestick_weekly_subfolder)
    localgoogle_drive = google_drive.GoogleCloudDrive(str_candlestick_filepath)
    re_exp = r'\.jpg$'
    #list_filename = localgoogle_drive.querylocalfiles(re_exp)
    localgoogle_drive.purgelocalfiles(re_exp)

    # Initial to sqlite database code
    path_db = os.path.join(dirnamelog,'TWTSEOTCDaily.db')
    
    ###############################
    # excute file1 #"循環投機追蹤股"
    ###############################
    list_excel_Seymour = [excel_file01]
    list_stkidx_call_file01 = stkidx_call_file01.split(',')
    list_stkidx_put_file01 = stkidx_put_file01.split(',')    
    
    #stkidx = '1258'#"其祥-KY" '9951'
    debug_verbose ='ON'
    #localdata_analysis = data_analysis.PandasSqliteAnalysis(stkidx,dirnamelog,path_db,str_first_year_month_day,debug_verbose)
    
    #str_dirlogcsv_file01 = os.path.join(dirnamelog,'1029循環投資追蹤股bothprices.csv')#debug purpose
    str_dirlogcsv_file01 = data_analysis.file1_main_fromsqlite(list_excel_Seymour,dirnamelog,path_db,str_first_year_month_day,debug_verbose)

    str_buysell_opt = 'call_循環'
    localdata_analysis = data_analysis.PandasDA_Excel(dirnamelog,list_excel_Seymour)

    # to filter whick stock to buy or to sell #"循環投機追蹤股"
    df_file01_stock_call = data_analysis.file1_call(str_dirlogcsv_file01)
    
    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA_fromsqlite(df_file01_stock_call,dirnamelog,path_db,str_first_year_month_day,
                            list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)    
    
    print('Seymour suggests to buy in at {}:'.format(excel_file01))
    df_rtu = localdata_analysis.SeymourExcel01_call()
    df_file01_Seymour_call = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_call_file01,df_rtu)
    
    # to get stock index by diff df_file03_Seymour_call btw df_file03_stock_call
    df_file01_Seymour_call_compare = localdata_analysis.compare_twoarrarys(df_file01_Seymour_call,df_file01_stock_call)
    print(df_file01_Seymour_call)

    # and then plot Candlestick and MA cruve if
    if len(df_file01_Seymour_call_compare) > 0:
        data_analysis.file_plotCandlestickMA_fromsqlite(df_file01_Seymour_call_compare,dirnamelog,path_db,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    str_buysell_opt = 'put_循環'
    # to filter whick stock to buy or to sell #"循環投機追蹤股"
    df_file01_stock_put = data_analysis.file1_put(str_dirlogcsv_file01)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA_fromsqlite(df_file01_stock_put,dirnamelog,path_db,str_first_year_month_day,
                            list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to sell out at {}:'.format(excel_file01))
    df_rtu = localdata_analysis.SeymourExcel01_put()
    df_file01_Seymour_put = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_put_file01,df_rtu)
    
    # to get stock index by diff df_file03_Seymour_put btw df_file03_stock_put
    df_file01_Seymour_put_compare = localdata_analysis.compare_twoarrarys(df_file01_Seymour_put,df_file01_stock_put)

    # and then plot Candlestick and MA cruve if
    if len(df_file01_Seymour_put_compare) > 0:
        data_analysis.file_plotCandlestickMA_fromsqlite(df_file01_Seymour_put_compare,dirnamelog,path_db,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)
    
    ###############################
    # excute file2 #"波段投機追蹤股"
    ###############################
    list_excel_Seymour = [excel_file02]
    list_stkidx_call_file02 = stkidx_call_file02.split(',')
    list_stkidx_put_file02 = stkidx_put_file02.split(',')
    
    debug_verbose ='ON'
    #str_dirlogcsv_file02 = os.path.join(dirnamelog,'1029波段投機追蹤股bothprices.csv')#debug purpose
    str_dirlogcsv_file02 = data_analysis.file2_main_fromsqlite(list_excel_Seymour,dirnamelog,path_db,str_first_year_month_day,debug_verbose)

    str_buysell_opt = 'call_波段'
    localdata_analysis = data_analysis.PandasDA_Excel(dirnamelog,list_excel_Seymour)
    
    # to filter whick stock to buy or to sell #"波段投機追蹤股"
    df_file02_stock_call = data_analysis.file2_call(str_dirlogcsv_file02)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA_fromsqlite(df_file02_stock_call,dirnamelog,path_db,str_first_year_month_day,
                                list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('Seymour suggests to buy in at {}:'.format(excel_file02))
    df_rtu = localdata_analysis.SeymourExcel02_call()
    df_file02_Seymour_call = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_call_file02,df_rtu)

    # to get stock index by diff df_file03_Seymour_call btw df_file03_stock_call
    df_file02_Seymour_call_compare = localdata_analysis.compare_twoarrarys(df_file02_Seymour_call,df_file02_stock_call)
    print(df_file02_Seymour_call)

    # and then plot Candlestick and MA cruve if
    if len(df_file02_Seymour_call_compare) > 0:
        data_analysis.file_plotCandlestickMA_fromsqlite(df_file02_Seymour_call_compare,dirnamelog,path_db,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)        

    str_buysell_opt = 'put_波段'
    # to filter whick stock to buy or to sell #"波段投機追蹤股"
    df_file02_stock_put = data_analysis.file2_put(str_dirlogcsv_file02)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA_fromsqlite(df_file02_stock_put,dirnamelog,path_db,str_first_year_month_day,
                            list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)
    
    print('\nSeymour suggests to sell out at {}:'.format(excel_file02))
    df_rtu = localdata_analysis.SeymourExcel02_put()
    df_file02_Seymour_put = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_put_file02,df_rtu)
    
    # to get stock index by diff df_file03_Seymour_put btw df_file03_stock_put
    df_file02_Seymour_put_compare = localdata_analysis.compare_twoarrarys(df_file02_Seymour_put,df_file02_stock_put)

    # and then plot Candlestick and MA cruve if
    if len(df_file02_Seymour_put_compare) > 0:
        data_analysis.file_plotCandlestickMA_fromsqlite(df_file02_Seymour_put_compare,dirnamelog,path_db,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    ###############################
    # excute file3 #"景氣循環追蹤股"
    ###############################
    list_excel_Seymour = [excel_file03]
    list_stkidx_call_file03 = stkidx_call_file03.split(',')
    list_stkidx_put_file03 = stkidx_put_file03.split(',')    

    #str_dirlogcsv_file03 = os.path.join(dirnamelog,'1012景氣循環追蹤股bothprices.csv')
    str_dirlogcsv_file03 = data_analysis.file3_main_fromsqlite(list_excel_Seymour,dirnamelog,path_db,str_first_year_month_day,debug_verbose)

    str_buysell_opt = 'call_景氣'
    localdata_analysis = data_analysis.PandasDA_Excel(dirnamelog,list_excel_Seymour)
    
    # to filter whick stock to buy or to sell #"景氣循環追蹤股"
    df_file03_stock_call = data_analysis.file3_call(str_dirlogcsv_file03)
    
    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA_fromsqlite(df_file03_stock_call,dirnamelog,path_db,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to buy in at {}:'.format(excel_file03))
    df_rtu = localdata_analysis.SeymourExcel03_call()
    df_file03_Seymour_call = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_call_file03,df_rtu)

    #to get stock index by diff df_file03_Seymour_call btw df_file03_stock_call
    df_file03_Seymour_call_compare = localdata_analysis.compare_twoarrarys(df_file03_Seymour_call,df_file03_stock_call)
    print(df_file03_Seymour_call)

    # and then plot Candlestick and MA cruve if
    if len(df_file03_Seymour_call_compare) > 0:
        data_analysis.file_plotCandlestickMA_fromsqlite(df_file03_Seymour_call_compare,dirnamelog,path_db,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)                                    

    str_buysell_opt = 'put_景氣'
    # to filter whick stock to buy or to sell #"景氣循環追蹤股"
    df_file03_stock_put = data_analysis.file3_put(str_dirlogcsv_file03)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA_fromsqlite(df_file03_stock_put,dirnamelog,path_db,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to sold out at {}:'.format(excel_file03))
    df_rtu = localdata_analysis.SeymourExcel03_put()
    df_file03_Seymour_put = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_put_file03,df_rtu)

    #to get stock index by diff df_file03_Seymour_put btw df_file03_stock_put
    df_file03_Seymour_put_compare = localdata_analysis.compare_twoarrarys(df_file03_Seymour_put,df_file03_stock_put)

    # and then plot Candlestick and MA cruve if
    if len(df_file03_Seymour_put_compare) > 0:
        data_analysis.file_plotCandlestickMA_fromsqlite(df_file03_Seymour_put_compare,dirnamelog,path_db,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    ###############################
    # excute file4 #"call_低波固收" "公用事業追蹤股"
    ###############################
    list_excel_Seymour = [excel_file04]
    list_stkidx_call_file04 = stkidx_call_file04.split(',')
    list_stkidx_put_file04 = stkidx_put_file04.split(',')    

    debug_verbose ='ON'
    #str_dirlogcsv_file04 = os.path.join(dirnamelog,'1029公用事業追蹤股bothprices.csv')#debug purpose
    #str_dirlogcsv_file04 = data_analysis.file4_main_fromsqlite(list_excel_Seymour,dirnamelog,path_db,str_first_year_month_day,debug_verbose)
    
    #2019/02/16 update to 低波固收
    str_dirlogcsv_file04 = data_analysis.file4_01_main_fromsqlite(list_excel_Seymour,dirnamelog,path_db,str_first_year_month_day,debug_verbose)

    #str_buysell_opt = 'call_公用'
    #str_buysell_opt = 'call_低波固收'#2019/1/26 rename
    str_buysell_opt = 'call_穩定成長'#2019/8/30 rename
    localdata_analysis = data_analysis.PandasDA_Excel(dirnamelog,list_excel_Seymour)
    
    # to filter whick stock to buy or to sell #"公用事業追蹤股"
    #df_file04_stock_call = data_analysis.file4_call(str_dirlogcsv_file04)
    #2019/02/16 update to 低波固收
    df_file04_stock_call = data_analysis.file4_01_call(str_dirlogcsv_file04)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA_fromsqlite(df_file04_stock_call,dirnamelog,path_db,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to buy in at {}:'.format(excel_file04))
    df_rtu = localdata_analysis.SeymourExcel04_call()
    df_file04_Seymour_call = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_call_file04,df_rtu)

    # to get stock index by diff df_file04_Seymour_call btw df_file04_stock_call
    df_file04_Seymour_call_compare = localdata_analysis.compare_twoarrarys(df_file04_Seymour_call,df_file04_stock_call)
    print(df_file04_Seymour_call)

    # and then plot Candlestick and MA cruve if
    if len(df_file04_Seymour_call_compare) > 0:
        data_analysis.file_plotCandlestickMA_fromsqlite(df_file04_Seymour_call_compare,dirnamelog,path_db,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    #str_buysell_opt = 'put_公用'
    #str_buysell_opt = 'put_低波固收'#2019/1/26 rename
    str_buysell_opt = 'put_穩定成長'#2019/8/30 rename
    # to filter whick stock to buy or to sell #"公用事業追蹤股"
    #df_file04_stock_put = data_analysis.file4_put(str_dirlogcsv_file04)
    #2019/02/16 update to 低波固收
    df_file04_stock_put = data_analysis.file4_01_put(str_dirlogcsv_file04)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA_fromsqlite(df_file04_stock_put,dirnamelog,path_db,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to sold out at {}:'.format(excel_file04))
    df_rtu = localdata_analysis.SeymourExcel04_put()
    df_file04_Seymour_put = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_put_file04,df_rtu)

    # to get stock index by diff df_file04_Seymour_put btw df_file04_stock_put
    df_file04_Seymour_put_compare = localdata_analysis.compare_twoarrarys(df_file04_Seymour_put,df_file04_stock_put)

    # and then plot Candlestick and MA cruve if
    if len(df_file04_Seymour_put_compare) > 0:
        data_analysis.file_plotCandlestickMA_fromsqlite(df_file04_Seymour_put_compare,dirnamelog,path_db,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)    