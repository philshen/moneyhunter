#2018/10/04 Intial test code to crawl weekly targets csv files.
#2018/10/12 Get xls file name from MoneyHunter blog and then
#           check if download it or not.
#2018/10/13 Adapte from test_MoneyHunterBlogCrawl.py
####################################################
import requests
import sys,datetime,os
from bs4 import BeautifulSoup
import urllib.request
from lxml import html
from urllib.parse import urlparse
import httplib2
from apiclient import discovery

class Flag:
    auth_host_name = 'localhost'
    noauth_local_webserver = False
    auth_host_port = [8080, 8090]
    logging_level = 'ERROR'

try:
    import argparse

    # flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
    flags = Flag()
except ImportError:
    flags = None

strabspath=os.path.abspath(__file__)
strdirname=os.path.dirname(strabspath)
str_split=os.path.split(strdirname)
prevdirname=str_split[0]
dirnamelib=os.path.join(prevdirname,"lib")
dirnamelog=os.path.join(prevdirname,"log")

sys.path.append(dirnamelib)
import googleDrive as google_drive
import readConfig as readConfig
import excelRW as excelrw
import dataAnalysis as data_analysis

def ggdrive_fileid(html_doc,xpath_url_file):
    parsed = urlparse(html_doc.xpath(xpath_url_file)[0])# 1st item fo list
    
    #ParseResult(scheme='https', netloc='drive.google.com', path='/file/d/1YaK7owM9M37fnEeXTxoSW_N3JZU5K4Ba/view', params='', query='usp=sharing', fragment='')
    #/file/d/1YaK7owM9M37fnEeXTxoSW_N3JZU5K4Ba/view
    # get fileid by path parm
    fileid = parsed.path.split('/')[-2]
    return fileid

#Get filename by fileid from google drive
#Chk current xls files under log folder.
#########################################
def check_xlsfile_logfolder(credential_dir,fileid_file,dirnamelog,flags):
    #get filename by fileid from google drive
    #########################################
    localgoogle_drive = google_drive.GoogleDrivebyFileID(dirnamelog,flags)
    credentials = localgoogle_drive.get_credentials(credential_dir)
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=http)

    filename_fileid_file = localgoogle_drive.getfilename_byfileid(service,fileid_file)

    #Chk current xls files under log folder.
    ######################################
    localgoogle_drive = google_drive.GoogleCloudDrive(dirnamelog)
    #re_exp = r'\.xls$'
    re_exp = r'{}'.format(filename_fileid_file)
    list_xls_filename = localgoogle_drive.querylocalfiles(re_exp)

    return list_xls_filename,filename_fileid_file

#Download filename by fileid from google drive
#########################################
def download_xlsfile_fromblog(list_filename,credential_dir,fileid_file,dirnamelog,flags):
    # if files not exist
    if len(list_filename) == 0:
        
        localgoogle_drive = google_drive.GoogleDrivebyFileID(dirnamelog,flags)
        credentials = localgoogle_drive.get_credentials(credential_dir)
        http = credentials.authorize(httplib2.Http())
        service = discovery.build('drive', 'v3', http=http)
        localgoogle_drive.download(service,fileid_file)
    else:# if files exist
        for filename in list_filename:
            print('{} are already on {}.'.format(filename,dirnamelog))


configPath=os.path.join(strdirname,"_diffExcel.ini")
localReadConfig = readConfig.ReadConfig(configPath)

url_moneyhunter =localReadConfig.get_SeymourExcel('url_moneyhunterblog')#'http://twmoneyhunter.blogspot.com/'

#xpath_url_file01 = '//*[@id="LinkList1"]/div/ul/li[4]/a/@href'#循環投資
#xpath_url_file02 = '//*[@id="LinkList1"]/div/ul/li[3]/a/@href'#波段投機
#xpath_url_file03 = '//*[@id="LinkList1"]/div/ul/li[5]/a/@href'#景氣循環
#xpath_url_file04 = '//*[@id="LinkList1"]/div/ul/li[6]/a/@href'#公用事業
#
#2019/09/01 MoneyHuter blog webpage layout reversion, value of xpath changed
#穩定成長，3;指數ETF, 4;債券ETF, 5;波段投機, 6;循環投資, 7;景氣循環, 8
xpath_url_file01 = '//*[@id="LinkList1"]/div/ul/li[7]/a/@href'#循環投資
xpath_url_file02 = '//*[@id="LinkList1"]/div/ul/li[6]/a/@href'#波段投機
xpath_url_file03 = '//*[@id="LinkList1"]/div/ul/li[8]/a/@href'#景氣循環
xpath_url_file04 = '//*[@id="LinkList1"]/div/ul/li[3]/a/@href'#穩定成長

#Python urllib urlopen not working
#https://stackoverflow.com/questions/25863101/python-urllib-urlopen-not-working
###########################################
with urllib.request.urlopen(url_moneyhunter) as response:
    raw = response.read()
html_doc = html.fromstring(raw)

fileid_file01 = ggdrive_fileid(html_doc,xpath_url_file01)
fileid_file02 = ggdrive_fileid(html_doc,xpath_url_file02)
fileid_file03 = ggdrive_fileid(html_doc,xpath_url_file03)
fileid_file04 = ggdrive_fileid(html_doc,xpath_url_file04)
#print(fileid_file01)

credential_dir = os.getcwd()
list_xlsfile, excel_file01_1st = check_xlsfile_logfolder(credential_dir,fileid_file01,dirnamelog,flags)#"循環投資追蹤股"
download_xlsfile_fromblog(list_xlsfile,credential_dir,fileid_file01,dirnamelog,flags)

list_xlsfile, excel_file02_1st = check_xlsfile_logfolder(credential_dir,fileid_file02,dirnamelog,flags)#"波段投機追蹤股"
download_xlsfile_fromblog(list_xlsfile,credential_dir,fileid_file02,dirnamelog,flags)

list_xlsfile, excel_file03_1st = check_xlsfile_logfolder(credential_dir,fileid_file03,dirnamelog,flags)#"景氣循環追蹤股"
download_xlsfile_fromblog(list_xlsfile,credential_dir,fileid_file03,dirnamelog,flags)

list_xlsfile, excel_file04_1st = check_xlsfile_logfolder(credential_dir,fileid_file04,dirnamelog,flags)#"公用事業追蹤股"
download_xlsfile_fromblog(list_xlsfile,credential_dir,fileid_file04,dirnamelog,flags)
#print('\n {}'.format(list_filename_file01))

#excel_file01_1st = localReadConfig.get_SeymourExcel("excelfile01_1st") #"循環投資追蹤股"
#excel_file02_1st = localReadConfig.get_SeymourExcel("excelfile02_1st") #"波段投機追蹤股"
#excel_file03_1st = localReadConfig.get_SeymourExcel("excelfile03_1st") #"景氣循環追蹤股"
#excel_file04_1st = localReadConfig.get_SeymourExcel("excelfile04_1st") #"公用事業追蹤股"

excel_file01_2nd = localReadConfig.get_SeymourExcel("excelfile01_2nd") #"循環投資追蹤股"
excel_file02_2nd = localReadConfig.get_SeymourExcel("excelfile02_2nd") #"波段投機追蹤股"
excel_file03_2nd = localReadConfig.get_SeymourExcel("excelfile03_2nd") #"景氣循環追蹤股"
excel_file04_2nd = localReadConfig.get_SeymourExcel("excelfile04_2nd") #"公用事業追蹤股"

list_excel_file01 = [excel_file01_1st,excel_file01_2nd]
localpdda_excel= data_analysis.PandasDA_Excel(dirnamelog,list_excel_file01)
localpdda_excel.diff_twodataframes()

list_excel_file02 = [excel_file02_1st,excel_file02_2nd]
localpdda_excel= data_analysis.PandasDA_Excel(dirnamelog,list_excel_file02)
localpdda_excel.diff_twodataframes()

list_excel_file03 = [excel_file03_1st,excel_file03_2nd]
localpdda_excel= data_analysis.PandasDA_Excel(dirnamelog,list_excel_file03)
localpdda_excel.diff_twodataframes()

list_excel_file04 = [excel_file04_1st,excel_file04_2nd]
localpdda_excel= data_analysis.PandasDA_Excel(dirnamelog,list_excel_file04)
localpdda_excel.diff_twodataframes()                                  