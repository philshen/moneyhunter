## 2018/12/02 Adapter from GSpreadAmy1210.py
## 2018/12/18 Adapter from updateGSheets.py of google_sheet_update              
#################################################################################
# Import library to know the time
import time,datetime
import os,sys

strabspath=os.path.abspath(__file__)
strdirname=os.path.dirname(strabspath)
str_split=os.path.split(strdirname)
prevdirname=str_split[0]
dirnamelib=os.path.join(prevdirname,"lib")
dirnamelog=os.path.join(prevdirname,"log")

path_db = os.path.join(dirnamelog,'TWTSEOTCDaily.db')
sys.path.append(dirnamelib)

import googleSS as googleSS
import readConfig as readConfig

configPath=os.path.join(strdirname,"config.ini")
localReadConfig = readConfig.ReadConfig(configPath)

gspreadsheet = localReadConfig.get_GSpread("sheet")
GDriveJSON =  localReadConfig.get_GSpread("gsheetJSON")
list_worksheet_spread = localReadConfig.get_WorkSheet_Amy1210("worksheet").split(',')
str_delay_sec = localReadConfig.get_WorkSheet_Amy1210("delay_sec")
#2018/11/17(Sat) excute this code meet from Mon. to Fri unreamrk below.
str_first_year_month_day = datetime.date.today().strftime('%Y,%m,%d')# ex:2018,10,16 get today date form system
#print('str_first_year_month_day= {}'.format(str_first_year_month_day))

#2018/11/17(Sat) excute this code dosen't meet from Mon. to Fri unremak below.
#str_first_year_month_day = localReadConfig.get_SeymourExcel("first_year_month_day")

print('將讀取試算表' ,gspreadsheet , '的資料')

scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
# Declare GoogleSS() from googleSS.py
localGoogleSS=googleSS.GoogleSS()
localGoogleSS.auth_gss_client(GDriveJSON, scope)

for worksheet_spread in list_worksheet_spread:
    # measure time consumption
    start = time.time()

    localGoogleSS.open_GSworksheet(gspreadsheet,worksheet_spread)

    print('將讀取', gspreadsheet, '中WorkSheet:', worksheet_spread, '的資料')
    #inital row count value 2
    row_count = 2

    localGoogleSS.update_GSpreadworksheet_logfolderdb(row_count,str_delay_sec,
                                                       dirnamelog,path_db,str_first_year_month_day)

    duration = time.time() - start
    print('Update data of {} duration: {:.2f} seconds'.format(worksheet_spread,duration)) 
