## 2018/08/14 Update from test_GSpreadAmy1210-03.py
## 2018/08/16 Modify def auth_gss_client() and def open_GSworksheet() 
##            from test_GSpreadAmy1210-04.py
## 2018/08/22 Update def update_GSpreadworksheet() if final price doesn't exist 
## 2018/10/17 Add def update_GSpreadworksheet_datafolderCSV to solve 
#              6024 群益期 that can't get close by def update_GSpreadworksheet
################################################################
import gspread
from oauth2client.service_account import ServiceAccountCredentials as SAC

# Import library to Read TW stock
import twstock
from twstock import BestFourPoint

import time,re
import dataAnalysis as data_analysis

class GoogleSS:
    #def __init__(self,gss_client_worksheet):
        #self.gss_client_worksheet=gss_client_worksheet

    def auth_gss_client(self,path,scopes):
        key = SAC.from_json_keyfile_name(path, scopes)
        self.gc = gspread.authorize(key)        

    def open_GSworksheet(self, gspreadsheet,worksheet_spread):
        gss_client_worksheet = self.gc.open(gspreadsheet).worksheet(worksheet_spread)
        self.gss_client_worksheet=gss_client_worksheet

    def update_sheet_celllist(self,str_cellrange,list_cellvalue):
        # print("Cell Range string:", str_cellrange)
        cell_list = self.gss_client_worksheet.range(str_cellrange)

        # 2018/8/12 Solve by issue:483; https://github.com/burnash/gspread/issues/483
        # cell_list[0].value = stock_price_final
        # cell_list[1].value = stock_price_open
        # cell_list[2].value = stock_price_high
        # cell_list[3].value = stock_price_low

        cell_list[0].value = list_cellvalue[0]
        cell_list[1].value = list_cellvalue[1]
        cell_list[2].value = list_cellvalue[2]
        cell_list[3].value = list_cellvalue[3]
        # print("cell_list:", cell_list)
        self.gss_client_worksheet.update_cells(cell_list)

    def update_sheet_insertrow(self,row_count,list_cellvalue):

        self.gss_client_worksheet.insert_row(list_cellvalue,row_count)

    def update_GSpreadworksheet(self,row_count,str_delay_sec):
    
        list_Gworksheet_rowvalue = self.gss_client_worksheet.row_values(row_count)
        #print (list_Gworksheet_rowvalue)

        while len(list_Gworksheet_rowvalue) > 0:
            ## Get stock number
            ## 2018/08/06 reject to use cause timeout frequently
            ##stock = twstock.Stock(exampleData[stock_index][0])
            ## get string of '代碼'
            #print(list_Gworksheet_rowvalue[1])
            stock = twstock.realtime.get(str(list_Gworksheet_rowvalue[1]))

            ## Check 4 stock prices: 1.final, 2.open, 3.high, 4.low
            stock_price_final = stock['realtime']['latest_trade_price']
            stock_price_open = stock['realtime']['open']
            stock_price_high = stock['realtime']['high']
            stock_price_low = stock['realtime']['low']
            print(list_Gworksheet_rowvalue[0], list_Gworksheet_rowvalue[1],
                  stock_price_final, stock_price_open, stock_price_high,stock_price_low)

            # 2018/8/11 ErrorCode:429, Exhaust Resoure
            # worksheet03.update_cell(row_count, 4, stock_price_final)
            # worksheet03.update_cell(row_count, 5, stock_price_open)
            # worksheet03.update_cell(row_count, 6, stock_price_high)
            # worksheet03.update_cell(row_count, 7, stock_price_low)

            # update by Cell Range
            str_range = 'D' + str(row_count) + ":" + 'G' + str(row_count)
            # print("Cell Range string:", str_range)
            ## clean cell content; remark by saving time.
            #list_cellvalue = ['','','','']
            #update_sheet_celllist(gss_client_worksheet,str_range,list_cellvalue)

            ## update cell content
            #2018/08/22 if final price doesn't exist    
            if type(stock_price_final) != None.__class__:
                list_cellvalue = [float(stock_price_final), float(stock_price_open),
                                  float(stock_price_high), float(stock_price_low)]
                self.update_sheet_celllist(str_range, list_cellvalue)

            # delay delay_sec secs
            # 2018/8/13 prevent ErrorCode:429, Exhaust Resoure
            #print ("Delay ", str_delay_sec, "secs to prevent Google Error Code:429, Exhaust Resoure")
            time.sleep(int(str_delay_sec))

            row_count += 1
            list_Gworksheet_rowvalue = self.gss_client_worksheet.row_values(row_count)
            #print (list_Gworksheet_rowvalue)
    
    #To solve 6024 群益期 that can't get close by def update_GSpreadworksheet
    ####################################################################
    def update_GSpreadworksheet_datafolderCSV(self,row_count,str_delay_sec,dirnamelog,dirdatafolder,str_first_year_month_day):

        list_Gworksheet_rowvalue = self.gss_client_worksheet.row_values(row_count)

        while len(list_Gworksheet_rowvalue) > 0:
            stkidx = str(list_Gworksheet_rowvalue[1])
            str_todaydate = re.sub(r",", "-", str_first_year_month_day)#2018-10-16

            localdata_analysis = data_analysis.PandasDataAnalysis(stkidx,dirnamelog,dirdatafolder,str_first_year_month_day)
            
            # 2018/11/5 5209 daily info as below:
            # 2018/11/5	0	0	---	---	---	---	---	0	5209	新鼎
            # to slove this issu to update get_tradedaysANDnonetradeday_dfinfo()
            #df_delduplicates_sortasc_tradeday = localdata_analysis.get_tradedays_dfinfo()
            df_delduplicates_sortasc_tradeday = localdata_analysis.get_tradedaysANDnonetradeday_dfinfo()

            #           date  open  high   low  close  Stkidx CmpName
            #329 2018-10-16  44.1  44.9  43.7  43.95    6024     群益期
            df_today_tradeinof = df_delduplicates_sortasc_tradeday[df_delduplicates_sortasc_tradeday['date'] == str_todaydate]
            
            #['43.95']
            #print(df_today_tradeinof['close'].values.astype(str)[0])
            ## Check 4 stock prices: 1.final, 2.open, 3.high, 4.low
            stock_price_final = df_today_tradeinof['close'].values.astype(str)[0]
            stock_price_open = df_today_tradeinof['open'].values.astype(str)[0]
            stock_price_high = df_today_tradeinof['high'].values.astype(str)[0]
            stock_price_low = df_today_tradeinof['low'].values.astype(str)[0]
            print(list_Gworksheet_rowvalue[0], list_Gworksheet_rowvalue[1],
                  stock_price_final, stock_price_open, stock_price_high,stock_price_low)

            # update by Cell Range
            str_range = 'D' + str(row_count) + ":" + 'G' + str(row_count)

            ## update cell content
            #2018/08/22 if final price doesn't exist    
            if bool(re.match(r'^-+-$',stock_price_final)) == False:
                list_cellvalue = [float(stock_price_final), float(stock_price_open),
                                  float(stock_price_high), float(stock_price_low)]
                self.update_sheet_celllist(str_range, list_cellvalue)      

            # delay delay_sec secs
            # 2018/8/13 prevent ErrorCode:429, Exhaust Resoure
            #print ("Delay ", str_delay_sec, "secs to prevent Google Error Code:429, Exhaust Resoure")
            time.sleep(int(str_delay_sec))

            row_count += 1
            list_Gworksheet_rowvalue = self.gss_client_worksheet.row_values(row_count)
    
    '''
       2018/12/04 Get OHLC price from db file
    ''' 
    def update_GSpreadworksheet_logfolderdb(self,row_count,str_delay_sec,dirnamelog,path_db,str_first_year_month_day):
        list_Gworksheet_rowvalue = self.gss_client_worksheet.row_values(row_count)

        while len(list_Gworksheet_rowvalue) > 0:
            stkidx = str(list_Gworksheet_rowvalue[1])
            str_todaydate = re.sub(r",", "-", str_first_year_month_day)#2018-10-16

            localdata_analysis = data_analysis.PandasSqliteAnalysis(stkidx,dirnamelog,path_db,
                                                                    str_first_year_month_day)

            # 2018/11/05 5209 daily info as below:
            # 2018/11/05	0	0	---	---	---	---	---	0	5209	新鼎
            # to slove this issu to update get_tradedaysANDnonetradeday_dfinfo()
            #df_delduplicates_sortasc_tradeday = localdata_analysis.get_tradedays_dfinfo()
            df_delduplicates_sortasc_tradeday = localdata_analysis.get_tradedaysANDnonetradeday_dfinfo()
            #print(df_delduplicates_sortasc_tradeday)

            #           date  open  high   low  close  Stkidx CmpName
            #329 2018-10-16  44.1  44.9  43.7  43.95    6024     群益期
            df_today_tradeinof = df_delduplicates_sortasc_tradeday[df_delduplicates_sortasc_tradeday['date'] == str_todaydate]
            #print(df_today_tradeinof)                                                        
                                                                    
            ## Check 4 stock prices: 1.final, 2.open, 3.high, 4.low
            stock_price_final = df_today_tradeinof['close'].values.astype(str)[0]
            stock_price_open = df_today_tradeinof['open'].values.astype(str)[0]
            stock_price_high = df_today_tradeinof['high'].values.astype(str)[0]
            stock_price_low = df_today_tradeinof['low'].values.astype(str)[0]
            print(list_Gworksheet_rowvalue[0], list_Gworksheet_rowvalue[1],
                  stock_price_final, stock_price_open, stock_price_high,stock_price_low)

            # update by Cell Range
            str_range = 'D' + str(row_count) + ":" + 'G' + str(row_count)

            ## update cell content
            #2018/08/22 if final price doesn't exist    
            if bool(re.match(r'^-+-$',stock_price_final)) == False:
                list_cellvalue = [float(stock_price_final), float(stock_price_open),
                                  float(stock_price_high), float(stock_price_low)]
                self.update_sheet_celllist(str_range, list_cellvalue)
            

            # delay delay_sec secs
            # 2018/8/13 prevent ErrorCode:429, Exhaust Resoure
            #print ("Delay ", str_delay_sec, "secs to prevent Google Error Code:429, Exhaust Resoure")
            time.sleep(int(str_delay_sec))

            row_count += 1
            list_Gworksheet_rowvalue = self.gss_client_worksheet.row_values(row_count)

    def plotCandlestickMA_GSpreadworksheet_logfolderdb(self,row_count,str_delay_sec,dirnamelog,path_db,str_first_year_month_day,
                                        list_color_ma, str_candlestick_weeklysubfolder,str_buysell_opt=''):
        list_Gworksheet_rowvalue = self.gss_client_worksheet.row_values(row_count)
        
        list_stkidx=[]
        # to get all stock index from google sheets to store list
        while len(list_Gworksheet_rowvalue) > 0:
            stkidx = str(list_Gworksheet_rowvalue[1])

            print(list_Gworksheet_rowvalue[0], list_Gworksheet_rowvalue[1])
            # delay delay_sec secs
            # 2018/8/13 prevent ErrorCode:429, Exhaust Resoure
            #print ("Delay ", str_delay_sec, "secs to prevent Google Error Code:429, Exhaust Resoure")
            time.sleep(int(str_delay_sec))

            list_stkidx.append(stkidx)    
            row_count += 1
            list_Gworksheet_rowvalue = self.gss_client_worksheet.row_values(row_count)

        # to get stock index then plot Candlestick and MA cruve
        for stkidx in list_stkidx:
            localsql_analysis = data_analysis.PandasSqliteAnalysis(stkidx,dirnamelog,path_db,str_first_year_month_day)
            localsql_analysis.plotCandlestickandMA(list_color_ma,str_candlestick_weeklysubfolder,str_buysell_opt)                                