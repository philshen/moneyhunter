# 2018/08/18 Initial
# 2018/08/24 add class CrawlTSEOTC adopted by test_craw.py and test_post_process.py
# 2018/08/25 add def exclude_nofinalprice_stockprice_datafoldercsv()
#            add def get_MinMax_byMonth_fromLastday()
# 2018/08/28 add def get_OpenHighLowClose_stockprice()
# 2018/10/27 add def create_tseotcdaily_many() in class DB_sqlite   
#################################################
import twstock
import os,sys,logging,time
import csv,re
import requests
from datetime import datetime, timedelta
from os import mkdir
from os.path import isdir

import json, yaml
import sqlite3
from sqlite3 import Error
from pymongo import MongoClient
from logger import logger

class DB_sqlite:
    def __init__(self, path_db_file):
        self.path_db_file = path_db_file

    def create_connection(self):
        """ create a database connection to a SQLite database """
        try:
            conn = sqlite3.connect(self.path_db_file)
            #print(sqlite3.version)
            return conn
        except Error as e:
            print(e)
        
        return None

    def create_table(self,conn, create_table_sql):
        """ create a table from the create_table_sql statement
        :param conn: Connection object
        :param create_table_sql: a CREATE TABLE statement
        :return:
        """
        try:
            c = conn.cursor()
            c.execute(create_table_sql)
        except Error as e:
            print(e)

    def create_tseotcdaily(self, conn, task):

        sql = ''' INSERT INTO TseOtcDaily(trade_date,
                                        trade_volumn,
                                        trade_amount,
                                        open_price,
                                        high_price,
                                        low_price,
                                        close_price,
                                        change,
                                        tran_saction,
                                        stkidx,
                                        cmp_name)
                    VALUES(?,?,?,?,?,?,?,?,?,?,?) '''
        try:
            cur = conn.cursor()
            cur.execute(sql, task)
        except Error as err:
            print("Error occurred: %s" % err)

        return cur.lastrowid

    def create_tseotcdaily_many(self, conn, list_tseotcdailyinfo_s):

        sql = ''' INSERT INTO TseOtcDaily(trade_date,
                                        trade_volumn,
                                        trade_amount,
                                        open_price,
                                        high_price,
                                        low_price,
                                        close_price,
                                        change,
                                        tran_saction,
                                        stkidx,
                                        cmp_name)
                    VALUES(?,?,?,?,?,?,?,?,?,?,?) '''
        try:
            cur = conn.cursor()
            # How to insert a list of lists into a table? [Python]
            #https://stackoverflow.com/questions/51503490/how-to-insert-a-list-of-lists-into-a-table-python

            # How do I use prepared statements for inserting MULTIPLE records in SQlite using Python / Django?
            # To use parameterized queries, and to provide more than one set of parameters
            #https://stackoverflow.com/questions/5616895/how-do-i-use-prepared-statements-for-inserting-multiple-records-in-sqlite-using
            list_tuple_tseotcdailyinfo_s = [tuple(l) for l in list_tseotcdailyinfo_s]
            #print(list_tuple_tseotcdailyinfo_s)
            cur.executemany(sql, list_tuple_tseotcdailyinfo_s)

        except sqlite3.Error as err:
            print("Error occurred: %s" % err)
        else:
            print('Total {} record(s) to insert table.'.format(cur.rowcount))
        
        return cur.lastrowid

    def select_all_tseotcdaily(self,conn):
        """
        Query all rows in the TseOtcDaily table
        :param conn: the Connection object
        :return:
        """
        cur = conn.cursor()
        cur.execute("SELECT * FROM TseOtcDaily")
        rows = cur.fetchall()
 
        for row in rows:
            print(row)
    
    def select_all_tseotcdaily_bydate(self,conn,str_date):
        """
        Query all rows in the TseOtcDaily table when trade_date like str_date
        :param conn: the Connection object
        :return:
        """
        sql = '''SELECT trade_date,
                        trade_volumn,
                        trade_amount,
                        open_price,
                        high_price,
                        low_price,
                        close_price,
                        change,
                        tran_saction,
                        stkidx,
                        cmp_name 
                        FROM TseOtcDaily WHERE trade_date LIKE ? ORDER BY stkidx  ASC'''
        
        cur = conn.cursor()
        cur.execute(sql, (str_date,))
        rows = cur.fetchall()

        #for row in rows:
        #    print(row)

        return rows

    def delete_tseotcdaily(self,conn, id):
        """
        Delete a tseotcdaily by tseotcdaily id
        :param conn:  Connection to the SQLite database
        :param id: id of the tseotcdaily
        :return:
        """
        sql = 'DELETE FROM tseotcdaily WHERE id=?'
        cur = conn.cursor()
        cur.execute(sql, (id,))

    def delete_all_tseotcdaily(self,conn):
        """
        Delete all rows in the tseotcdaily table
        :param conn: Connection to the SQLite database
        :return:
        """
        sql = 'DELETE FROM tseotcdaily'
        cur = conn.cursor()
        cur.execute(sql)

        print('Delete all rows in Table tseotcdaily.') 

class DB_mongo:

    def connect_mongo(arg, *args):  #連線資料庫
        
        mongo_host=args[0]
        mongo_db=args[1]
        mongo_collection=args[2]
        #print(arg,args[0],args[1])
        mongo_username=args[3]
        mongo_password=args[4]

        uri_mongo = "mongodb://{}:{}@{}:27017" 

        #print('mongo_db: {}'.format(mongo_db))
        client = MongoClient(uri_mongo.format(mongo_username,mongo_password,mongo_host))
        db = client[mongo_db]
        collection = db[mongo_collection]
        print(collection)

        return collection

    def insert_tseotcdaily_many(collection, list_tseotcdailyinfo_s):
        '''list_tse_dailyinfo.append( [date_str_AD,# 西元日期
                                                yaml_dump[2],# 成交股數
                                                yaml_dump[4],# 成交金額
                                                yaml_dump[5],# 開盤價
                                                yaml_dump[6],# 最高價
                                                yaml_dump[7],# 最低價
                                                yaml_dump[8],# 收盤價
                                                sign + yaml_dump[10],# 漲跌價差
                                                yaml_dump[3],# 成交筆數
                                                yaml_dump[0],#代碼
                                                yaml_dump[1]] )#名稱]
        '''                                        
        count_element = 0
        list_element = []

        for item in list_tseotcdailyinfo_s:#
            #print(item)
            if collection.find({ "date": item[0],   #找尋該交易資料是否不存在
                            "stockno": item[-2]} ).count() == 0:

                dic_element={'date':item[0], 'stockno':item[-2], 'cmpname':item[-1],'shares':item[1], 
                                    'amount':item[2], 'open':item[3], 'high':item[4], 
                                    'low':item[5], 'close':item[6], 'diff':item[7], 'turnover':item[8]};  #製作MongoDB的插入元素    
                list_element.append(dic_element)#append all dic_element for bluck insert

                count_element += 1   #caluate element numbers
                #print(list_element)

        if list_element != []:
            collection.insert_many(list_element)#bulk insert all documents
    
        return count_element        


class TWStockInfo:
    def __init__(self,twstockidx):
        self.twstockidx=twstockidx

    def fetch_closeprice_fromYearMonth(self, year:int, month:int):
        stock = twstock.Stock(self.twstockidx)
        # 擷取自該年、月至今日之歷史股票資料。
        list_fetch_info=stock.fetch_from(year,month)

        list_rtu_stkprice_close=[]

        for fetch_info in list_fetch_info:
            # Get close price
            stkprice_close=fetch_info[6]
            list_rtu_stkprice_close.append(stkprice_close)

        return list_rtu_stkprice_close

    def fetch_openprice_fromYearMonth(self, year:int, month:int):
        stock = twstock.Stock(self.twstockidx)
        # 擷取自該年、月至今日之歷史股票資料。
        list_fetch_info=stock.fetch_from(year,month)

        list_rtu_stkprice_close=[]

        for fetch_info in list_fetch_info:
            # Get close price
            stkprice_close=fetch_info[3]
            list_rtu_stkprice_close.append(stkprice_close)

        return list_rtu_stkprice_close

    def fetch_highprice_fromYearMonth(self, year:int, month:int):
        stock = twstock.Stock(self.twstockidx)
        # 擷取自該年、月至今日之歷史股票資料。
        list_fetch_info=stock.fetch_from(year,month)

        list_rtu_stkprice_close=[]

        for fetch_info in list_fetch_info:
            # Get close price
            stkprice_close=fetch_info[4]
            list_rtu_stkprice_close.append(stkprice_close)

        return list_rtu_stkprice_close

    def fetch_lowprice_fromYearMonth(self, year:int, month:int):
        stock = twstock.Stock(self.twstockidx)
        # 擷取自該年、月至今日之歷史股票資料。
        list_fetch_info=stock.fetch_from(year,month)

        list_rtu_stkprice_close=[]

        for fetch_info in list_fetch_info:
            # Get close price
            stkprice_close=fetch_info[5]
            list_rtu_stkprice_close.append(stkprice_close)

        return list_rtu_stkprice_close

    def stockprice_raiserate(self,list_stkprice_duration):
        
        stkprice_current=list_stkprice_duration[-1]
        stkprice_low=sorted(list_stkprice_duration)[0]
        raiserate=(stkprice_current-stkprice_low)/stkprice_low

        return raiserate

    def stockprice_decreaserate(self,list_stkprice_duration):

        stkprice_current=list_stkprice_duration[-1]
        stkprice_high=sorted(list_stkprice_duration)[-1]
        decreaserate=(stkprice_current-stkprice_high)/stkprice_high

        return decreaserate

class Crawler:
    def __init__(self,list_stkidx_stkname, opt_verbose='OFF', prefix="data"):
        ''' Make directory if not exist when initialize '''
        if not isdir(prefix):
            mkdir(prefix)
        self.prefix = prefix
        self.list_stkidx_stkname = list_stkidx_stkname
        self.opt_verbose= opt_verbose
    ''' 7/20/2019 record
    ['0050', '元大台灣50', '9,780,922', '3,437', '814,129,776', '83.00', '83.45', '83.00', '83.00', 'X', '0.00', '83.00', '323', '83.05', '30', '0.00'], 
    ['0051', '元大中型100', '15,400', '12', '512,940', '33.35', '33.50', '33.25', '33.29', '<p style= color:red>+</p>', '0.14', '33.29', '19', '33.48', '2', '0.00'], 
    ['0052', '富邦科技', '117,908', '22', '6,708,795', '56.45', '56.95', '56.45', '56.85', '<p style= color:red>+</p>', '0.95', '56.85', '1', '56.95', '1', '0.00'], 
    ['0053', '元大電子', '12,000', '5', '431,420', '36.00', '36.14', '35.89', '35.89', '<p style= color:red>+</p>', '0.44', '35.81', '30', '36.03', '30', '0.00'],
    ['0054', '元大台商50', '1,000', '1', '22,600', '22.60', '22.60', '22.60', '22.60', 'X', '0.00', '22.60', '1', '22.70', '6', '0.00'], 
    ['0055', '元大MSCI金融', '36,000', '10', '667,460', '18.52', '18.59', '18.52', '18.53', '<p style= color:red>+</p>', '0.01', '18.53', '9', '18.57', '11', '0.00'], 
    ['0056', '元大高股息', '5,264,174', '1,846', '142,772,960', '27.13', '27.17', '27.03', '27.06', ' ', '0.00', '27.06', '4', '27.07', '18', '0.00'], 
    ['0061', '元大寶滬深', '415,564', '155', '7,564,043', '18.08', '18.29', '18.08', '18.23', '<p style= color:red>+</p>', '0.15', '18.22', '23', '18.23', '32', '0.00'], 
    ['006205', '富邦上証', '1,761,093', '228', '55,969,255', '31.55', '31.91', '31.55', '31.74', '<p style= color:red>+</p>', '0.26', '31.72', '53', '31.74', '89', '0.00'], 
    ['00736', '國泰新興市場', '124,000', '13', '2,505,010', '20.20', '20.25', '20.18', '20.21', '<p style= color:green>-</p>', '0.12', '20.19', '12', '20.21', '48', '0.00'], 
    ['00710B', 'FH彭博高收益債', '1,056,000', '208', '21,971,560', '20.82', '20.82', '20.79', '20.79', '<p style= color:green>-</p>', '0.05', '20.79', '4', '20.80', '2', '0.00'], 

    ['1101', '台泥', '18,104,510', '5,577', '808,328,336', '44.70', '44.90', '44.55', '44.80', '<p style= color:red>+</p>', '0.20', '44.75', '9', '44.80', '401', '10.72'], 
    ['1101B', '台泥乙特', '15,254', '15', '797,806', '52.30', '52.40', '52.20', '52.40', ' ', '0.00', '52.20', '5', '52.40', '26', '0.00'], 
    ['1102', '亞泥', '11,560,108', '3,257', '535,613,275', '46.20', '46.65', '46.10', '46.20', '<p style= color:green>-</p>', '0.20', '46.20', '145', '46.25', '4', '12.80'],
    ['1103', '嘉泥', '813,103', '214', '13,648,077', '16.85', '16.85', '16.75', '16.85', ' ', '0.00', '16.80', '4', '16.85', '70', '16.05'], 
    ['1104', '環泥', '462,508', '192', '8,814,502', '19.05', '19.10', '19.00', '19.05', ' ', '0.00', '19.00', '167', '19.05', '60', '10.30'], 
    ['1108', '幸福', '62,002', '17', '430,713', '6.92', '6.98', '6.92', '6.97', '<p style= color:red>+</p>', '0.06', '6.97', '2', '6.98', '15', '0.00'], 
    ['1109', '信大', '740,250', '290', '13,421,423', '18.35', '18.35', '18.00', '18.10', '<p style= color:green>-</p>', '0.25', '18.05', '36', '18.10', '5', '7.33'], 
    ['1110', '東泥', '100,000', '18', '1,672,300', '16.80', '16.80', '16.70', '16.80', ' ', '0.00', '16.75', '1', '16.80', '6', '120.00'], 
    ['1201', '味全', '2,801,653', '1,252', '89,454,660', '31.65', '32.25', '31.55', '31.80', '<p style= color:red>+</p>', '0.10', '31.80', '73', '31.90', '2', '8.11'],
    '''
    def _clean_row(self, row):
        ''' Clean comma and spaces '''
        for index, content in enumerate(row):
            row[index] = re.sub(",", "", content.strip())
        return row

    def _record(self, stock_id, row):
        ''' Save row to csv file '''
        f = open('{}/{}.csv'.format(self.prefix, stock_id), 'a')
        cw = csv.writer(f, lineterminator='\n')
        cw.writerow(row)
        f.close()
    
    def _dump_json2file(self,path_file,jsondata):
        with open(path_file, 'w', encoding='utf-8') as f:
            json.dump(jsondata, f, ensure_ascii=False)
### 
# 2020/10/04 by WireShark http can'work bu https can work
# URI: http://www.twse.com.tw/exchangeReport/MI_INDEX?date=20200930&response=json&type=ALL&_=1601807588776
### 

    def _get_tse_data(self, date_tuple):
        date_str = '{0}{1:02d}{2:02d}'.format(date_tuple[0], date_tuple[1], date_tuple[2])
        #url = 'http://www.twse.com.tw/exchangeReport/MI_INDEX'
        url = 'https://www.twse.com.tw/exchangeReport/MI_INDEX';# 2020/10/04 can't work

        query_params = {
            'date': date_str,
            'response': 'json',
            'type': 'ALL',
            '_': str(round(time.time() * 1000) - 500)
        }

        if self.opt_verbose.lower()== 'on':
            msg = 'query_params: {}.'
            logger.info(msg.format(query_params))

        # Get json data
        page = requests.get(url, params=query_params)

        if self.opt_verbose.lower()== 'on':
            msg = 'page: {}.'
            logger.info(msg.format(page))

        if not page.ok:
            #logging.error("Can not get TSE data at {}".format(date_str))
            msg = "Can not get TSE data at {}."
            logger.info(msg.format(date_str))
            return

        content = page.json()
        #print(content)
        # For compatible with original data
        #date_str_mingguo = '{0}/{1:02d}/{2:02d}'.format(date_tuple[0] - 1911, date_tuple[1], date_tuple[2])
        date_str_AD = '{0}/{1:02d}/{2:02d}'.format(date_tuple[0], date_tuple[1], date_tuple[2])

        #2018/10/18 apply josn and yaml
        list_json_dump = json.dumps(content)
        list_yaml_dump = yaml.safe_load(list_json_dump)
        
        #2018/10/22 JSON parser of data5
        #''' "6582",
        #    "申豐",
        #    "0",
        #    "0",
        #    "0",
        #    "--",
        #    "--",
        #    "--",
        #    "--",
        #    " ",
        #    "0.00",
        #    "48.60",
        #    "1",
        #    "48.95",
        #    "2",
        #    "14.95"'''
        #''' "6605",
        #    "帝寶",
        #    "84,221",
        #    "73",
        #    "5,731,437",
        #    "69.60",
        #    "69.60",
        #    "67.60",
        #    "68.30",
        #    "<p style= color:green>-</p>",
        #    "1.40",
        #    "68.30",
        #    "4",
        #    "68.50",
        #    "2",
        #    "10.01"'''
        
        print("Check TSE stock")
        list_tse_dailyinfo = []

        #7/20/2019 cause can't get TSE stock info
        #print(content['data5']) 

        # 2018/10/22 using module json,yaml 
        #for yaml_dump in list_yaml_dump['data5']:

        #7/20/2019 cause can't get TSE stock info
        for yaml_dump in list_yaml_dump['data9']:     
            # Check stock idx if meets from Seymour's
            for stkidx in self.list_stkidx_stkname:
                
                #if stkidx[0] == yaml_dump[0]:
                #2019/07/20 to check stock name not sotock idx
                if stkidx[1] == yaml_dump[1]:
                    sign = '-' if yaml_dump[9].find('green') > 0 else ''
                    #print(date_str_AD,# 西元日期
                    #        yaml_dump[2],# 成交股數
                    #        yaml_dump[4],# 成交金額
                    #        yaml_dump[5],# 開盤價
                    #        yaml_dump[6],# 最高價
                    #        yaml_dump[7],# 最低價
                    #        yaml_dump[8],# 收盤價
                    #        sign + yaml_dump[10],# 漲跌價差
                    #        yaml_dump[3],# 成交筆數
                    #        yaml_dump[0],#代碼
                    #        yaml_dump[1])#名稱
                    list_tse_dailyinfo.append( [date_str_AD,# 西元日期
                                                yaml_dump[2],# 成交股數
                                                yaml_dump[4],# 成交金額
                                                yaml_dump[5],# 開盤價
                                                yaml_dump[6],# 最高價
                                                yaml_dump[7],# 最低價
                                                yaml_dump[8],# 收盤價
                                                sign + yaml_dump[10],# 漲跌價差
                                                yaml_dump[3],# 成交筆數
                                                yaml_dump[0],#代碼
                                                yaml_dump[1]] )#名稱]

        #path_file = os.path.join(self.prefix,'{}{}{}.json'.format(date_tuple[0], date_tuple[1], date_tuple[2]))
        #self._dump_json2file(path_file,content)

        #print(list_tse_dailyinfo)
        
        #for data in content['data5']:
        #    sign = '-' if data[9].find('green') > 0 else ''
        #    row = self._clean_row([
                #date_str_mingguo, # 日期
        #        date_str_AD, # 西元日期
        #        data[2], # 成交股數
        #        data[4], # 成交金額
        #        data[5], # 開盤價
        #        data[6], # 最高價
        #        data[7], # 最低價
        #        data[8], # 收盤價
        #        sign + data[10], # 漲跌價差
        #        data[3], # 成交筆數
        #        data[0],#代碼
        #        data[1]#名稱
        #    ])

            #print(self.list_stkidx_stkname)
            
            # Check stock idx if meets from Seymour's 
            # 2018/10/18 remark by testing purpose
            #for stkidx in self.list_stkidx_stkname:
                #print("stkidx:",stkidx[0],"data[0]:",data[0])
            #    if stkidx[0] == data[0]:
            #        print("TSE stock idx:",data[0],"stock name:",data[1])
            #        self._record(data[0].strip(), row)

        # for web refuse connection
        time.sleep(1)

        return list_tse_dailyinfo
    
    def _get_otc_data(self, date_tuple):
        date_str = '{0}/{1:02d}/{2:02d}'.format(date_tuple[0] - 1911, date_tuple[1], date_tuple[2])
        ttime = str(int(time.time()*100))
        url = 'http://www.tpex.org.tw/web/stock/aftertrading/daily_close_quotes/stk_quote_result.php?l=zh-tw&d={}&_={}'.format(date_str, ttime)
        page = requests.get(url)

        if not page.ok:
            logging.error("Can not get OTC data at {}".format(date_str))
            return

        #print(page)
        result = page.json()
        #print(result)

        if result['reportDate'] != date_str:
            logging.error("Get error date OTC data at {}".format(date_str))
            return

        print("Check OTC stock")
        list_otc_dailyinfo = []

        date_str_AD = '{0}/{1:02d}/{2:02d}'.format(date_tuple[0], date_tuple[1], date_tuple[2])
        
        #2018/10/22 apply josn and yaml
        list_json_dump = json.dumps(result)
        list_yaml_dump = yaml.safe_load(list_json_dump)
        #path_file = os.path.join(self.prefix,'{}{}{}_OTC.json'.format(date_tuple[0], date_tuple[1], date_tuple[2]))
        #self._dump_json2file(path_file,result)
        #'''"mmData":[
        #    [
        #    "4415",
        #    "台原藥",
        #    "3.70",
        #    "0.00 ",
        #    "3.70",
        #    "3.70",
        #    "3.70",
        #    "3.70",
        #    "3,000",
        #    "11,100",
        #    "1",
        #    "3.50",
        #    "3.88",
        #    "36,627,168",
        #    "3.70",
        #    "4.07",
        #    "3.33"
        #    ]
        #],'''
        #'''"mmData":[
        #    [
        #    "9951",
        #    "皇田",
        #    "72.10",
        #    "+1.00",
        #    "70.40",
        #    "72.80",
        #    "70.20",
        #    "71.67",
        #    "112,200",
        #    "8,041,400",
        #    "109",
        #    "72.10",
        #    "72.20",
        #    "74,900,000",
        #    "72.10",
        #    "79.30",
        #    "64.90"
        #],'''
        
        # 2018/10/22 using module json,yaml 
        for table in [list_yaml_dump['mmData'],list_yaml_dump['aaData']]:
            for yaml_dump in table:
                # Check stock idx if meets from Seymour's 
                for stkidx in self.list_stkidx_stkname:

                    #2019/07/20 to check stock name not sotock idx                
                    #if stkidx[0] == yaml_dump[0]:
                    if stkidx[1] == yaml_dump[1]:
                        #print(date_str_AD,
                        #        yaml_dump[8],# 成交股數
                        #        yaml_dump[9],# 成交金額
                        #        yaml_dump[4],# 開盤價
                        #        yaml_dump[5],# 最高價
                        #        yaml_dump[6],# 最低價
                        #        yaml_dump[2],# 收盤價
                        #        yaml_dump[3],# 漲跌價差
                        #        yaml_dump[10],# 成交筆數
                        #        yaml_dump[0],#代碼
                        #        yaml_dump[1])#名稱
                        
                        list_otc_dailyinfo.append( [date_str_AD,
                                                    yaml_dump[8],# 成交股數
                                                    yaml_dump[9],# 成交金額
                                                    yaml_dump[4],# 開盤價
                                                    yaml_dump[5],# 最高價
                                                    yaml_dump[6],# 最低價
                                                    yaml_dump[2],# 收盤價
                                                    yaml_dump[3],# 漲跌價差
                                                    yaml_dump[10],# 成交筆數
                                                    yaml_dump[0],#代碼
                                                    yaml_dump[1]] )#名稱

        #for table in [result['mmData'], result['aaData']]:
        #    for tr in table:
        #        row = self._clean_row([
                    #date_str,
        #            date_str_AD,
        #            tr[8], # 成交股數
        #            tr[9], # 成交金額
        #            tr[4], # 開盤價
        #            tr[5], # 最高價
        #            tr[6], # 最低價
        #            tr[2], # 收盤價
        #            tr[3], # 漲跌價差
        #            tr[10], # 成交筆數
        #            tr[0],#代碼
        #            tr[1]#名稱
        #        ])

                # Check stock idx if meets from Seymour's 
                # 2018/10/18 remark by testing purpose
                #for stkidx in self.list_stkidx_stkname:
                #    if stkidx[0] == tr[0]:
                #        print("OTC stock idx:",tr[0],"stock name:",tr[1])
                #        self._record(tr[0], row)

        # for web refuse connection
        time.sleep(1)

        return list_otc_dailyinfo
    
    def get_data(self, date_tuple):
        print('Crawling {}'.format(date_tuple))
        self._get_tse_data(date_tuple)
        self._get_otc_data(date_tuple)

    def get_data_tosqilte(self, date_tuple):
        #print('Crawling {} '.format(date_tuple))
        msg = 'Crawling {}.'
        logger.info(msg.format(date_tuple))

        list_tse_dailytradeinfo_s = self._get_tse_data(date_tuple)
        list_otc_dailytradeinfo_s = self._get_otc_data(date_tuple)

        return list_tse_dailytradeinfo_s, list_otc_dailytradeinfo_s

    def get_data_tomongodb(self, date_tuple):
        print('Crawling {} '.format(date_tuple))

        list_tse_dailytradeinfo_s = self._get_tse_data(date_tuple)
        list_otc_dailytradeinfo_s = self._get_otc_data(date_tuple)

        return list_tse_dailytradeinfo_s, list_otc_dailytradeinfo_s

class CrawlTSEOTC:

    def __init__(self,list_dir_file,log_path,str_first_y_m_d,str_last_y_m_d, opt_verbose='OFF'):
        self.list_dir_file = list_dir_file
        self.log_path = log_path
        self.str_first_y_m_d = str_first_y_m_d
        self.str_last_y_m_d = str_last_y_m_d    
        self.opt_verbose= opt_verbose

    # crawl TSE and OTC stock price to limit from first to last day 
    def crawl_date_fromfirst_tolast(self,list_stockidx_stockname):
        # Set logging
        if not os.path.isdir(self.log_path):
            os.makedirs(self.log_path)
        logging.basicConfig(filename='{}/crawl-error.log'.format(self.log_path),
            level=logging.ERROR,
            format='%(asctime)s\t[%(levelname)s]\t%(message)s',
            datefmt='%Y/%m/%d %H:%M:%S')

        # treat first date and last date
        first_date = self.str_first_y_m_d.split(',')
        last_date = self.str_last_y_m_d.split(',')
        #print(last_date)
        first_day = datetime(int(first_date[0]), int(first_date[1]), int(first_date[2]))
        last_day = datetime(int(last_date[0]), int(last_date[1]), int(last_date[2]))
        first_date_str_AD = '{0}/{1:02d}/{2:02d}'.format(first_date[0], int(first_date[1]), int(first_date[2]))
        last_date_str_AD = '{0}/{1:02d}/{2:02d}'.format(last_date[0], int(last_date[1]), int(last_date[2]))

        print("From First Date:", first_date_str_AD, "To Last Date:", last_date_str_AD)

        crawler = Crawler(list_stockidx_stockname)    
        
        max_error = 5
        error_times = 0

        while error_times < max_error and first_day >= last_day:
            try:
                crawler.get_data((first_day.year, first_day.month, first_day.day))
                error_times = 0
            except:
                date_str = first_day.strftime('%Y/%m/%d')
                logging.error('Crawl raise error {}'.format(date_str))
                error_times += 1
                continue
            finally:
                first_day -= timedelta(1)
    
    # crawl TSE and OTC stock price to limit from first to last day 
    def crawl_date_fromfirst_tolast_tosqlite(self,list_stockidx_stockname,pt_db_sqlite,conn):
        # Set logging
        #if not os.path.isdir(self.log_path):
        #    os.makedirs(self.log_path)
        #logging.basicConfig(filename='{}/crawl-error.log'.format(self.log_path),
        #    level=logging.ERROR,
        #    format='%(asctime)s\t[%(levelname)s]\t%(message)s',
        #    datefmt='%Y/%m/%d %H:%M:%S')

        # treat first date and last date
        first_date = self.str_first_y_m_d.split(',')
        last_date = self.str_last_y_m_d.split(',')
        #print(last_date)
        first_day = datetime(int(first_date[0]), int(first_date[1]), int(first_date[2]))
        last_day = datetime(int(last_date[0]), int(last_date[1]), int(last_date[2]))
        first_date_str_AD = '{0}/{1:02d}/{2:02d}'.format(first_date[0], int(first_date[1]), int(first_date[2]))
        last_date_str_AD = '{0}/{1:02d}/{2:02d}'.format(last_date[0], int(last_date[1]), int(last_date[2]))

        #print("From First Date:", first_date_str_AD, "To Last Date:", last_date_str_AD, "Store to TWTSEOTCDaily.db")
        msg = "From First Date:{} To Last Date:{} Store to TWTSEOTCDaily.db"
        logger.info(msg.format(first_date_str_AD,last_date_str_AD))

        crawler = Crawler(list_stockidx_stockname, self.opt_verbose)    
        
        max_error = 5
        error_times = 0
        list_tse_dailytradeinfo_s = []
        list_otc_dailytradeinfo_s = []

        while error_times < max_error and first_day >= last_day:
            try:
                # Get daily TSE OTC tradeinfo 
                list_tse_dailytradeinfo_s, list_otc_dailytradeinfo_s = \
                    crawler.get_data_tosqilte((first_day.year, first_day.month, first_day.day))

                # Insert total lists to sqlite directly
                pt_db_sqlite.create_tseotcdaily_many(conn, list_tse_dailytradeinfo_s)
                pt_db_sqlite.create_tseotcdaily_many(conn, list_otc_dailytradeinfo_s)    
                
                # Save (commit) the changes daily
                conn.commit()

                error_times = 0
            except:
                date_str = first_day.strftime('%Y/%m/%d')
                #logging.error('Crawl raise error {}'.format(date_str))
                msg = "Crawl raise error {}"
                logger.info(msg.format(date_str))

                error_times += 1
                continue
            finally:
                first_day -= timedelta(1)

    # crawl TSE and OTC stock price to limit from first to last day 
    def crawl_date_fromfirst_tolast_tomongodb(self,list_stockidx_stockname,collection):
        # Set logging
        if not os.path.isdir(self.log_path):
            os.makedirs(self.log_path)
        logging.basicConfig(filename='{}/crawl-error.log'.format(self.log_path),
            level=logging.ERROR,
            format='%(asctime)s\t[%(levelname)s]\t%(message)s',
            datefmt='%Y/%m/%d %H:%M:%S')

        # treat first date and last date
        first_date = self.str_first_y_m_d.split(',')
        last_date = self.str_last_y_m_d.split(',')
        #print(last_date)
        first_day = datetime(int(first_date[0]), int(first_date[1]), int(first_date[2]))
        last_day = datetime(int(last_date[0]), int(last_date[1]), int(last_date[2]))
        first_date_str_AD = '{0}/{1:02d}/{2:02d}'.format(first_date[0], int(first_date[1]), int(first_date[2]))
        last_date_str_AD = '{0}/{1:02d}/{2:02d}'.format(last_date[0], int(last_date[1]), int(last_date[2]))

        print("From First Date:", first_date_str_AD, "To Last Date:", last_date_str_AD, "Store to Collection:TWTSEOTCDaily")

        crawler = Crawler(list_stockidx_stockname)    
        
        max_error = 5
        error_times = 0
        list_tse_dailytradeinfo_s = []
        list_otc_dailytradeinfo_s = []

        while error_times < max_error and first_day >= last_day:
            try:
                # Get daily TSE OTC tradeinfo 
                list_tse_dailytradeinfo_s, list_otc_dailytradeinfo_s = \
                    crawler.get_data_tomongodb((first_day.year, first_day.month, first_day.day))

                #print(list_otc_dailytradeinfo_s)

                # Insert total lists to sqlite directly
                count_tse_documents = DB_mongo.insert_tseotcdaily_many(collection, list_tse_dailytradeinfo_s)
                print('Total {} document(s) of TSE to insert collection.'.format(count_tse_documents))
                
                count_otc_documents = DB_mongo.insert_tseotcdaily_many(collection, list_otc_dailytradeinfo_s)    
                print('Total {} document(s) of OTC to insert collection.'.format(count_otc_documents))
                
                error_times = 0

            except:
                date_str = first_day.strftime('%Y/%m/%d')
                logging.error('Crawl raise error {}'.format(date_str))
                error_times += 1
                continue

            finally:
                first_day -= timedelta(1)    

    # query TSE and OTC stock price to limit from first to last day from sqlite
    def query_date_fromfirst_tolast_fromsqlite(self,pt_db_sqlite,conn):
        # Set logging
        if not os.path.isdir(self.log_path):
            os.makedirs(self.log_path)
        logging.basicConfig(filename='{}/crawl-error.log'.format(self.log_path),
            level=logging.ERROR,
            format='%(asctime)s\t[%(levelname)s]\t%(message)s',
            datefmt='%Y/%m/%d %H:%M:%S')

        # treat first date and last date
        first_date = self.str_first_y_m_d.split(',')
        last_date = self.str_last_y_m_d.split(',')
        #print(last_date)
        first_day = datetime(int(first_date[0]), int(first_date[1]), int(first_date[2]))
        last_day = datetime(int(last_date[0]), int(last_date[1]), int(last_date[2]))
        first_date_str_AD = '{0}/{1:02d}/{2:02d}'.format(first_date[0], int(first_date[1]), int(first_date[2]))
        last_date_str_AD = '{0}/{1:02d}/{2:02d}'.format(last_date[0], int(last_date[1]), int(last_date[2]))

        print("From First Date:", first_date_str_AD, "To Last Date:", last_date_str_AD, "Query data from TWTSEOTCDaily.db")

        tuple_tseotc_dailytradeinfo_s = ()
        list_rtu_tseotc_dailytradeinfo_s = []

        while first_day >= last_day:
            try:
                msg  ="Query all data by date: {}"
                print(msg.format(first_date_str_AD))
                # Select total lists from sqlite directly
                tuple_tseotc_dailytradeinfo_s = pt_db_sqlite.select_all_tseotcdaily_bydate(conn,first_date_str_AD)

                list_rtu_tseotc_dailytradeinfo_s.append(tuple_tseotc_dailytradeinfo_s)    
                
                #msg  ="After query all data by date: {}"
                #print(msg.format(first_date_str_AD))

            except:
                date_str = first_day.strftime('%Y/%m/%d')
                logging.error('Crawl raise error {}'.format(date_str))
                continue

            finally:
                first_day -= timedelta(1)
                first_date_str_AD = '{0}/{1:02d}/{2:02d}'.format(first_day.year, first_day.month, first_day.day)
                #msg  ="Finally by date: {}"
                #print(msg.format(first_date_str_AD))

        return list_rtu_tseotc_dailytradeinfo_s

    # insert TSE and OTC stock price to limit from first to last day 
    def insert_date_fromfirst_tolast_tomongodb(self,pt_db_sqlite,conn,collection):
        # Set logging
        if not os.path.isdir(self.log_path):
            os.makedirs(self.log_path)
        logging.basicConfig(filename='{}/crawl-error.log'.format(self.log_path),
            level=logging.ERROR,
            format='%(asctime)s\t[%(levelname)s]\t%(message)s',
            datefmt='%Y/%m/%d %H:%M:%S')

        # treat first date and last date
        first_date = self.str_first_y_m_d.split(',')
        last_date = self.str_last_y_m_d.split(',')
        #print(last_date)
        first_day = datetime(int(first_date[0]), int(first_date[1]), int(first_date[2]))
        last_day = datetime(int(last_date[0]), int(last_date[1]), int(last_date[2]))
        first_date_str_AD = '{0}/{1:02d}/{2:02d}'.format(first_date[0], int(first_date[1]), int(first_date[2]))
        last_date_str_AD = '{0}/{1:02d}/{2:02d}'.format(last_date[0], int(last_date[1]), int(last_date[2]))

        print("From First Date:", first_date_str_AD, "To Last Date:", last_date_str_AD, "Store to Collection:TWTSEOTCDaily")    
        
        tuple_tseotc_dailytradeinfo = ()
        
        while first_day >= last_day:
            try:
                msg  ="Query from SqilteDB all data by date: {}"
                print(msg.format(first_date_str_AD))

                # Select total lists from sqlite directly
                tuple_tseotc_dailytradeinfo = pt_db_sqlite.select_all_tseotcdaily_bydate(conn,first_date_str_AD)

                # Insert total lists to sqlite directly
                count_tse_documents = DB_mongo.insert_tseotcdaily_many(collection, tuple_tseotc_dailytradeinfo)
                print('Total {} document(s) of TSE to insert collection.'.format(count_tse_documents))

            except:
                date_str = first_day.strftime('%Y/%m/%d')
                logging.error('Crawl raise error {}'.format(date_str))
                continue

            finally:
                first_day -= timedelta(1)
                first_date_str_AD = '{0}/{1:02d}/{2:02d}'.format(first_day.year, first_day.month, first_day.day)
                #msg  ="Finally by date: {}"
                #print(msg.format(first_date_str_AD))
                     
    def string_to_time(self,string):
        year, month, day = string.split('/')
        return datetime(int(year) + 1911, int(month), int(day))

    def get_stockprice_datafoldercsv(self):

        for dir_file in self.list_dir_file:
            dict_rows = {}

            # element of line as below:
            #date_str_AD, # 西元日期
            #    data[2], # 成交股數
            #    data[4], # 成交金額
            #    data[5], # 開盤價
            #    data[6], # 最高價
            #    data[7], # 最低價
            #    data[8], # 收盤價
            #    sign + data[10], # 漲跌價差
            #    data[3], # 成交筆數
            #    data[0],#代碼
            #    data[1]#名稱

            # Load and remove duplicates (use newer)
            with open(dir_file, 'r') as file:

                for line in file.readlines():
                    # str.split(separator, maxsplit)
                    list_line = line.split(',', 11)
                    list_target_element = [list_line[0],list_line[-5],list_line[-2],list_line[-1].strip('\n')] 
                    dict_rows[line.split(',', 1)[0]] = list_target_element
                    #print(list_target_element)
                    #print(dict_rows)
                    
            # Sort by date
            rows = [row for date, row in sorted(
                    dict_rows.items(), key= lambda x:self.string_to_time(x[0]))]
            #print(rows)

        return rows    

    def get_OpenHighLowClose_stockprice(self):
        for dir_file in self.list_dir_file:
            dict_rows = {}

            # Load and remove duplicates (use newer)
            with open(dir_file, 'r') as file:
                for line in file.readlines():
                    # str.split(separator, maxsplit)
                    list_line = line.split(',', 11)
                    list_target_element = [list_line[0],
                                        list_line[3],list_line[4],list_line[5],list_line[6],#Open,High,Low,Close
                                        list_line[-2],list_line[-1].strip('\n')]
                    dict_rows[line.split(',', 1)[0]] = list_target_element

            # Sort by date
            rows = [row for date, row in sorted(
                    dict_rows.items(), key= lambda x:self.string_to_time(x[0]))]

        return rows                                     

    # filter no finalprice day stockprice
    def exclude_nofinalprice_stockprice_datafoldercsv(self):

        for dir_file in self.list_dir_file:
            dict_rows = {}

            # element of line as below:
            #date_str_AD, # 西元日期
            #    data[2], # 成交股數
            #    data[4], # 成交金額
            #    data[5], # 開盤價
            #    data[6], # 最高價
            #    data[7], # 最低價
            #    data[8], # 收盤價
            #    sign + data[10], # 漲跌價差
            #    data[3], # 成交筆數
            #    data[0],#代碼
            #    data[1]#名稱

            # Load and remove duplicates (use newer)
            with open(dir_file, 'r') as file:

                for line in file.readlines():
                    # str.split(separator, maxsplit)
                    list_line = line.split(',', 11)
                    
                    #print(list_line)
                    # check close price if equal '---' and '--'
                    if list_line[-5] != '---' and list_line[-5] != '--' :
                        list_target_element = [list_line[0],list_line[-5],list_line[-2],list_line[-1].strip('\n')]
                        dict_rows[line.split(',', 1)[0]] = list_target_element
                        #print(list_target_element)
                        #print(dict_rows)
                    
            # Sort by date
            rows = [row for date, row in sorted(
                    dict_rows.items(), key= lambda x:self.string_to_time(x[0]))]
            #print(rows)

        return rows

    def get_MinMax_byMonth_fromLastday(self):
        list_sortedbydate=self.exclude_nofinalprice_stockprice_datafoldercsv()

        #print(len(list_sortedbydate[-31:-1]))
        list_month_lastday_sortedbydate = list_sortedbydate[-31:-1]
        list_finalprice_month_lastday_sortedbydate = []

        # get final price  in month period for last day
        for sortedbydate in list_month_lastday_sortedbydate:
            #print("finalprice of {}: {}".format(sortedbydate[0],sortedbydate[1]) )
            list_finalprice_month_lastday_sortedbydate.append(float(sortedbydate[1]))

        list_min_max_current = [min(list_finalprice_month_lastday_sortedbydate),
                                max(list_finalprice_month_lastday_sortedbydate),
                                list_sortedbydate[-1][1]]

        return list_min_max_current

    def get_MinMax_byQuarter_fromLastday(self):
        
        #list_sortedbydate=self.get_stockprice_datafoldercsv()
        # 2018/08/25 chnge to exclude_nofinalprice_stockprice_datafoldercsv()
        list_sortedbydate=self.exclude_nofinalprice_stockprice_datafoldercsv()

        #print(len(list_sortedbydate[-59:-1]))

        list_quarter_lastday_sortedbydate = list_sortedbydate[-59:-1]
        list_finalprice_quarter_lastday_sortedbydate = []        

        # get final price  in quarter period for last day
        for sortedbydate in list_quarter_lastday_sortedbydate:
            #print("finalprice of {}: {}".format(sortedbydate[0],sortedbydate[1]) )
            list_finalprice_quarter_lastday_sortedbydate.append(float(sortedbydate[1]))

        #print("final price of current day: {}".format(list_sortedbydate[-1][1]) )
        #print("Min final price of quarter from last day: {}".format(min(list_finalprice_quarter_lastday_sortedbydate)) )
        #print("Max final price of quarter from last day: {}".format(max(list_finalprice_quarter_lastday_sortedbydate)) )

        list_rtu_min_max_current = [min(list_finalprice_quarter_lastday_sortedbydate),
                                    max(list_finalprice_quarter_lastday_sortedbydate),
                                    list_sortedbydate[-1][1]]

        return list_rtu_min_max_current

    def get_MinMax_byYaer_fromLastday(self):
        #list_sortedbydate=self.get_stockprice_datafoldercsv()
        # 2018/08/25 chnge to exclude_nofinalprice_stockprice_datafoldercsv()
        list_sortedbydate=self.exclude_nofinalprice_stockprice_datafoldercsv()

        list_year_lastday_sortedbydate = list_sortedbydate[-249:-1]
        list_finalprice_year_lastday_sortedbydate = []
        
        #print(len(list_sortedbydate[-250:-1]))

        #  get final price  in year period for last day
        for sortedbydate in list_year_lastday_sortedbydate:
            list_finalprice_year_lastday_sortedbydate.append(float(sortedbydate[1]))

        #print("final price of current day: {}".format(list_sortedbydate[-1][1]) )
        #print("Min final price of year from last day: {}".format(min(list_finalprice_year_lastday_sortedbydate)) )
        #print("Max final price of year from last day: {}".format(max(list_finalprice_year_lastday_sortedbydate)) )

        list_rtu_min_max_current = [min(list_finalprice_year_lastday_sortedbydate),
                                    max(list_finalprice_year_lastday_sortedbydate),
                                    list_sortedbydate[-1][1]]

        return list_rtu_min_max_current

    def stockprice_raiserate_decreaserate(self,list_stkprice_duration):

        # 201808/28 check close price if equal '---' and '--'
        if list_stkprice_duration[2] != '---' and list_stkprice_duration[2] != '--':
            stkprice_current=float(list_stkprice_duration[2])
            stkprice_lowest=float(list_stkprice_duration[0])
            stkprice_highest=float(list_stkprice_duration[1])
            raiserate=(stkprice_current-stkprice_lowest)/stkprice_lowest
            decreaserate=(stkprice_current-stkprice_highest)/stkprice_highest
        else:
            raiserate=0
            decreaserate=0

        list_rtu_rate = [raiserate,decreaserate]
        return list_rtu_rate


   