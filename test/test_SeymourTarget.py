# 2018/08/31 Initial to use pandas, matplotlib,TAlib
# 2018/09/01 Using pandas to calculate MA
#            Calculate golden and dead MA
#            Add class PandasDataAnalysis
#            Slove  matplotlib to show Chinese charter
# 2018/09/02 Match '---' and '--' in close price by using regular express
#            Caluclate uprate and downrate by rolling max min of dataframe
# 2018/09/03 add def file1_updownrate_LastMonthYear()
# 2018/09/04 dtype of close price icluding '---' and '--' is object except float64
#            Save as test_SeymourTarget.py
#            Add def file1_updownrate_LastMonthYear()
# 2018/09/05 Add def get_tradedays_dfinfo () and def file2_updownrate_QuarterYear
#            in class PandasDataAnalysis
#            Save as test_SeymourTarget.py
# 2018/09/06 Debug output CSV dulplicated rows.
#            Add def file2_updownrate_threeYearoneYear in class PandasDataAnalysis
# 2018/09/14 Add def file1_call, file1_put, file2_call, file2_put nad percent2float
# 2018/09/15 Add def file3_call and file3_put
# 2018/10/30 remark file_plotCandlestickMA() in 景氣 and 公用 cause time consumption waste 
#####################################################################################

# -- coding: utf-8 --
import talib
import pandas as pd
import matplotlib.pyplot  as plt
import numpy as np
from datetime import datetime, timedelta
import time
import os
import sys
from io import StringIO
import re

def plotchart(ptr_df,str_label,str_title):
    ptr_df.close.plot(color='#177A96', label=str_label)
    plt.title(str_title)
    plt.grid(True)
    plt.show()

def plotMAchart(list_ptr_df,list_label,str_title):
    plt.figure(figsize=(20,5))    
    plt.plot(list_ptr_df[0], color='#DE5B49', label=list_label[0], alpha=0.8, linewidth=3)
    plt.plot(list_ptr_df[1], color='#324D5C', label=list_label[1], alpha=0.8, linewidth=3)
    plt.plot(list_ptr_df[2], color='#46B29D', label=list_label[2], alpha=0.8, linewidth=3)
    plt.legend(loc='upper left')
    plt.grid(True)
    plt.title(str_title)
    plt.show()

def getMidvalue_lengthNum(inp_num):
    list_lengthNum = []
    # from 0 to inp_num not including inp_num
    for i in range(inp_num):
        list_lengthNum.append(i)
    
    #四捨五入中位數取其位置
    median_value = int(round(np.median(list_lengthNum)))
    return median_value

def plotMA05MA20MA30(data_frame,str_title):
    #在talib中，輸入輸出都需要用array，參數二則是你要選擇的n天，第三參數選擇均線的類型
    #chose start position from startpos_idx
    SMA_05 = talib.MA(np.array(data_frame.close), timeperiod=5, matype=0)
    SMA_20 = talib.MA(np.array(data_frame.close), timeperiod=20, matype=0)
    SMA_30 = talib.MA(np.array(data_frame.close), timeperiod=30, matype=0)

    #使用matplotlib繪圖之前先將array轉成DataFrame
    df_SMA_05 = pd.DataFrame(SMA_05, index = data_frame.index, columns = ['SMA05'])
    df_SMA_20 = pd.DataFrame(SMA_20, index = data_frame.index, columns = ['SMA20'])
    df_SMA_30 = pd.DataFrame(SMA_30, index = data_frame.index, columns = ['SMA30'])

    list_ptr_df = [df_SMA_05,df_SMA_20,df_SMA_30]
    list_label = ['SMA_05','SMA_20','SMA_30']
    plotMAchart(list_ptr_df,list_label,str_title)

def main01(csv_datafolder):
    # get date, open, high, low, close price and volume from csv file
    ##################index_col = [0] ###############
    # dataframe as below:
    #               volume   open   high    low  close CmpName
    #date
    #2017-07-03    773400  13.75  13.85  13.70  13.80      台航
    #2017-07-04   2154492  13.85  14.30  13.75  13.75      台航
    #2017-07-05    504327  13.75  13.80  13.55  13.75      台航
    csv_stockfile = pd.read_csv(csv_datafolder, header = None, encoding = 'cp950', 
                                usecols = [0,1,3,4,5,6,10], index_col = [0], 
                                names = ['date', 'volume','open', 'high', 'low', 'close', 'CmpName'],
                                parse_dates = [0],
                                date_parser = lambda x:pd.datetime.strptime(x,'%Y/%m/%d'))
    df = csv_stockfile.copy()

    #df.sort_index(ascending=0,inplace=True)
    df.sort_index(ascending=1,inplace=True)

    #print(df.to_string())
    # get row count after sort index
    print("original row counts: {}".format(len(df.index)))

    df_delduplicates = df.drop_duplicates()
    
    # get row count after delet duplicated row
    print("row counts after drop duplicated rows: {}".format(len(df_delduplicates.index)) )
    #print(df.duplicated().to_string())

    # row idx from 0 to extend 10 items; column idx from 1 to extend 5 items including close price
    #print(df_delduplicates.iloc[0:10,1:5])
    title = stkidx + df_delduplicates.iloc[0,5]
    #title = df_delduplicates.iloc[0,5]
    print(title)

    len_df_delduplicates = len(df_delduplicates.index)

    # get last day value
    ts_endday = df_delduplicates[-1:].index.tolist()[0]
    print(ts_endday)
    # Pandas: Convert Timestamp to datetime.date
    dt_endday = pd.Timestamp(ts_endday).date()
    #print(dt_endday)

    #subtract 90 days
    dt_startdate = dt_endday - timedelta(days=90)
    print("Start Date:{} End Date:{}".format(dt_startdate,dt_endday))
    #print(df_delduplicates.index[0])
    #print(df_delduplicates.to_string())

    startpos_idx = getMidvalue_lengthNum(len_df_delduplicates)
    #print("Half start position: {}".format(startpos_idx))
    plotMA05MA20MA30(df_delduplicates.iloc[startpos_idx:], title)

class PandasDataAnalysis:
    
    def __init__(self,stkidx,dirnamelog,dirdatafolder):
        
        FOLDER = dirdatafolder
        csv_datafolder = '{}/{}.csv'.format(FOLDER,stkidx)
        self.stkidx = stkidx
        self.dirnamelog = dirnamelog

        # get date, open, high, low, close price and volume from csv file
        ################## remark index_col = [0] ###############
        ## then 'date' become a column name
        #          date    volume   open   high    low  close CmpName
        #0   2018-05-02   4715058  17.20  18.10  17.00  17.05      台航
        #1   2018-05-03    956738  16.85  16.95  16.65  16.85      台航
        #2   2018-05-04    612524  17.00  17.30  16.90  16.95      台航
        #3   2018-05-07    776401  17.15  17.25  16.70  16.75      台航

        # get date and close from csv file
        csv_stockfile = pd.read_csv(csv_datafolder, header = None, encoding = 'cp950', 
                            usecols = [0,3,4,5,6,10], #index_col = [0], 
                            names = ['date', 'open', 'high', 'low', 'close', 'CmpName'],
                            parse_dates = [0],
                            date_parser = lambda x:pd.datetime.strptime(x,'%Y/%m/%d'))
        df = csv_stockfile.copy()
        self.df = df
        #self.df.sort_index(ascending=1,inplace=True)

        # get row count after sort index
        print("original row counts: {}".format(len(self.df.index)))
        # check close price if match '---' and '--' or not
        if self.df['close'].dtype == np.float64:
            print("dtype of column 'close': {}".format(self.df['close'].dtype) )
        elif self.df['close'].dtype == np.object:
            print("column 'close' includes none trading records.")     

    def MACrossOver(self):
        # Get present time
        local_time = time.localtime(time.time())

        df_delduplicates = self.df.drop_duplicates()
        # get row count after delet duplicated row
        print("row counts after drop duplicated rows: {}".format(len(df_delduplicates.index)) )
        #print(df.duplicated().to_string())

        # sort pandas dataframe from one column
        df_delduplicates_sortasc = df_delduplicates.sort_values('date',ascending=1)
        # filter rows of pandas dataframe by timestamp column backward 90 days.
        df_delduplicates_back90D = df_delduplicates_sortasc.iloc[-90:,0:6]
        #print(df_delduplicates_back90D)

        # to add the calculated Moving Average as a new column to the right after 'Value'
        # to get 2 digitals after point by using np
        df_delduplicates_back90D['SMA_05'] = np.round(df_delduplicates_back90D['close'].rolling(window=5).mean(),2 )
        df_delduplicates_back90D['SMA_20'] = np.round(df_delduplicates_back90D['close'].rolling(window=20).mean(),2 )
        df_delduplicates_back90D['SMA_30'] = np.round(df_delduplicates_back90D['close'].rolling(window=30).mean(),2 )
        #print(df_delduplicates_back90D)

        # calculate SMA_05 Moving Average Crossover SMA_20
        previous_05 = df_delduplicates_back90D['SMA_05'].shift(1)
        previous_20 = df_delduplicates_back90D['SMA_20'].shift(1)
        crossing = (((df_delduplicates_back90D['SMA_05'] <= df_delduplicates_back90D['SMA_20']) & (previous_05 >= previous_20))
                    | ((df_delduplicates_back90D['SMA_05'] >= df_delduplicates_back90D['SMA_20']) & (previous_05 <= previous_20)))
        
        golden_crossing = ((df_delduplicates_back90D['SMA_05'] >= df_delduplicates_back90D['SMA_20']) 
                            & (previous_05 <= previous_20))
        dead_crossing = ((df_delduplicates_back90D['SMA_05'] <= df_delduplicates_back90D['SMA_20']) 
                            & (previous_05 >= previous_20))

        #crossing_dates = df_delduplicates_back90D.loc[crossing, 'date']
        #print(crossing_dates)
        crossing = df_delduplicates_back90D.loc[crossing]
        golden_crossing = df_delduplicates_back90D.loc[golden_crossing]
        dead_crossing = df_delduplicates_back90D.loc[dead_crossing]
        #print(crossing)
        print('MA Godlen CrossOver')
        print(golden_crossing)
        print('\n')
        print('MA Deaded CrossOver')
        print(dead_crossing)

        # Output CSV file including path
        filename_csv_macross=str(local_time.tm_mon)+str(local_time.tm_mday)+'_'+self.stkidx+'_'+"MACrossOver"+".csv"
        dirlog_csv_macross=os.path.join(self.dirnamelog,filename_csv_macross)
        golden_crossing.to_csv(dirlog_csv_macross, mode = 'w',sep='\t', header='Golden Crossing',encoding='cp950')
        dead_crossing.to_csv(dirlog_csv_macross, mode = 'a',sep='\t', header='Dead Crossing',encoding='cp950')

    def file1_updownrate_LastMonthYear(self,valuerate):#"循環投資追蹤股"

        df_delduplicates_sortasc_tradeday = self.get_tradedays_dfinfo()
        # filter Pandas Dataframe rolling max min backward Month,Quarter,Year    
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_30D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=30).min()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_30D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=30).max()
        #df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_60D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=60).min()
        #df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_60D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=60).max()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_250D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=250).min()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_250D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=250).max()

        #calcuate raiserate_decreaserate
        df_delduplicates_sortasc_tradeday.loc[:,'uprate_01D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['low'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['low'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_01D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['high'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['high'].astype(float) )
    
        df_delduplicates_sortasc_tradeday.loc[:,'uprate_30D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_min_30D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_min_30D'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_30D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_max_30D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_max_30D'].astype(float) )

        df_delduplicates_sortasc_tradeday.loc[:,'uprate_250D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_min_250D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_min_250D'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_250D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_max_250D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_max_250D'].astype(float) )

        # 2018/08/31,0,0,---,---,---,---,---,0,4747,強生
        # 2018/09/03,0,0,---,---,---,---,---,0,4747,強生
        # 2018/09/04 last trade day maybe no trade happening, so forget assign date to index
        # Assigning an index column (and drop index column) to pandas dataframe to filter specific row
        #df_delduplicates_sortasc_tradeday_dateidx = df_delduplicates_sortasc_tradeday.set_index("date", drop = True)
        #print(df_delduplicates_sortasc_tradeday_dateidx)

        #df_delduplicates_sortasc_tradeday_dateidx_lastday = df_delduplicates_sortasc_tradeday_dateidx.loc[str_lastday,:]

        df_delduplicates_sortasc_tradeday_lastday = df_delduplicates_sortasc_tradeday.iloc[-1:,:]

        # flatten the lists then get its value like [['27.70']]-->27.7
        #list_temp = df_delduplicates_sortasc_tradeday_lastday[['close']].values.flatten()[0]
        #print( list_temp)
        
        #list_rows_bothprices=[]
        #head_rows=["代碼","公司","市價","1Y下跌率","1M下跌率","Lastday下跌率",
                    #    "1Y上昇率","1M上昇率","Lastday上昇率",
                    #    "價格比","last trade day"]
        list_row_value_finalprice = [self.stkidx,
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['CmpName']],
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['close']],
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['downrate_250D']],
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['downrate_30D']],
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['downrate_01D']],
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['uprate_250D']],
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['uprate_30D']],
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['uprate_01D']],
                                     df_delduplicates_sortasc_tradeday_lastday[['CmpName']].values.flatten()[0],
                                    df_delduplicates_sortasc_tradeday_lastday[['close']].values.flatten()[0],
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_250D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_30D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_01D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_250D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_30D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_01D']].values.flatten()[0] *100),
                                    valuerate,
                                    df_delduplicates_sortasc_tradeday_lastday[['date']].values.flatten()[0]
                                    ]
        
        return list_row_value_finalprice

    # delete dataframe of both duplicates and nonetradeday
    def get_tradedays_dfinfo(self):

        df_delduplicates = self.df.drop_duplicates()
        # get row count after delet duplicated row
        print("row counts after drop duplicated rows: {}".format(len(df_delduplicates.index)) )

        # sort pandas dataframe from column 'date'
        df_delduplicates_sortasc = df_delduplicates.sort_values('date',ascending=1)

        # check clsoe price if includes '---' or '--' or not, but
        # 2018/09/04 dtype of close price icluding '---' and '--' is object except float64
        # convert value to string if value does have digitals
        if self.df['close'].dtype == np.object:
            # DataFrame filter close column by regex
            df_delduplicates_sortasc_nonetradeday = df_delduplicates_sortasc.loc[
                                                    df_delduplicates_sortasc['close'].str.contains(r'^-+-$')]
            #print(df_delduplicates_sortasc_nonetradeday)
            print("row counts with none trade: {}".format(len(df_delduplicates_sortasc_nonetradeday)) )

            # df_delduplicates_sortasc['close'] exclude (r'^-+-$')
            df_delduplicates_sortasc_tradeday = df_delduplicates_sortasc[~df_delduplicates_sortasc['close'].str.contains(r'^-+-$')]
        elif self.df['close'].dtype == np.float64:
            df_delduplicates_sortasc_tradeday = df_delduplicates_sortasc

        print("row counts with trade: {}".format(len(df_delduplicates_sortasc_tradeday)) )

        return df_delduplicates_sortasc_tradeday

    def file2_updownrate_QuarterYear(self,valuerate):#"波段投機追蹤股"

        df_delduplicates_sortasc_tradeday = self.get_tradedays_dfinfo()

        # filter Pandas Dataframe rolling max min backward Month,Quarter,Year    
        #df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_30D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=30).min()
        #df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_30D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=30).max()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_60D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=60).min()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_60D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=60).max()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_250D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=250).min()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_250D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=250).max()
        
        #calcuate raiserate_decreaserate
        df_delduplicates_sortasc_tradeday.loc[:,'uprate_01D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['low'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['low'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_01D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['high'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['high'].astype(float) )
    
        df_delduplicates_sortasc_tradeday.loc[:,'uprate_60D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_min_60D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_min_60D'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_60D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_max_60D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_max_60D'].astype(float) )

        df_delduplicates_sortasc_tradeday.loc[:,'uprate_250D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_min_250D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_min_250D'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_250D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_max_250D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_max_250D'].astype(float) )

        df_delduplicates_sortasc_tradeday_lastday = df_delduplicates_sortasc_tradeday.iloc[-1:,:]

        #list_rows_bothprices=[]
        #head_rows=["代碼","公司","市價","1Q上昇率","1Y下跌率","Lastday上昇率",
                    #    "1Q下跌率","1Y上昇率","Lastday下跌率",
                    #    "價格比","last trade day"]
        list_row_value_finalprice = [self.stkidx,
                                    df_delduplicates_sortasc_tradeday_lastday[['CmpName']].values.flatten()[0],
                                    df_delduplicates_sortasc_tradeday_lastday[['close']].values.flatten()[0],
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_60D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_250D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_01D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_60D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_250D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_01D']].values.flatten()[0] *100),
                                    valuerate,
                                    df_delduplicates_sortasc_tradeday_lastday[['date']].values.flatten()[0]
                                    ]
        
        return list_row_value_finalprice

    def file3_updownrate_threeYearoneYear(self,pbr):#"景氣循環追蹤股"
        df_delduplicates_sortasc_tradeday = self.get_tradedays_dfinfo()

        # filter Pandas Dataframe rolling max min backward Quarter,Year, 3Year
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_60D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=60).min()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_60D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=60).max()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_250D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=250).min()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_250D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=250).max()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_730D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=730).min()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_730D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=730).max()

        #calcuate raiserate_decreaserate
        df_delduplicates_sortasc_tradeday.loc[:,'uprate_60D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_min_60D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_min_60D'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_60D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_max_60D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_max_60D'].astype(float) )

        df_delduplicates_sortasc_tradeday.loc[:,'uprate_250D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_min_250D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_min_250D'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_250D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_max_250D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_max_250D'].astype(float) )

        df_delduplicates_sortasc_tradeday.loc[:,'uprate_730D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_min_730D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_min_730D'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_730D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_max_730D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_max_730D'].astype(float) )
    
        df_delduplicates_sortasc_tradeday_lastday = df_delduplicates_sortasc_tradeday.iloc[-1:,:]

        #list_rows_bothprices=[]
        #head_rows=["代碼","公司","市價","3Y下跌率","1Y下跌率","1Q下跌率",
                    #    "3Y上昇率","1Y上昇率","1Q上昇率",
                    #    "PBR","last trade day"]
        list_row_value_finalprice = [self.stkidx,
                                    df_delduplicates_sortasc_tradeday_lastday[['CmpName']].values.flatten()[0],
                                    df_delduplicates_sortasc_tradeday_lastday[['close']].values.flatten()[0],
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_730D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_250D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_60D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_730D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_250D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_60D']].values.flatten()[0] *100),
                                    pbr,
                                    df_delduplicates_sortasc_tradeday_lastday[['date']].values.flatten()[0]
                                    ]
        
        return list_row_value_finalprice



def file1_main(list_excel_Seymour,dirnamelog,dirdatafolder):#"循環投資追蹤股"
    # Get present time
    local_time = time.localtime(time.time())

    localexcelrw = excelrw.ExcelRW()

    for excel_Seymour in list_excel_Seymour:
        print('將讀取Excel file:', excel_Seymour, '的資料')

        # Excel file including path
        dirlog_ExcelFile=os.path.join(dirnamelog,excel_Seymour)
        # Read values of each row
        list_row_value_price=localexcelrw.readExcel(dirlog_ExcelFile)

        # Output CSV file including path
        filename_csv_bothprices=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"bothprices"+".csv"
        filename_csv_belowprice=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"belowprice"+".csv"
        dirlog_csv_bothprices=os.path.join(dirnamelog,filename_csv_bothprices)
        dirlog_csv_belowprice=os.path.join(dirnamelog,filename_csv_belowprice)

        # Declare output contents
        list_rows_bothprices=[]
        list_rows_belowprice=[]
        head_rows=["代碼","公司","市價","1Y下跌率(%)","1M下跌率(%)","Lastday下跌率(%)",
                    "1Y上昇率(%)","1M上昇率(%)","Lastday上昇率(%)","價值比"]
        list_rows_bothprices.append(head_rows)
        list_rows_belowprice.append(head_rows)

        dict_rows = {}
        # get  all CSV files name under data folder
        for list_row_value in list_row_value_price:
            # get key=idx value=價值比 to store in dict
            dict_rows[list_row_value[0]] = list_row_value[2]

        list_temp2 =[]#to store return list
        # by key=idx value=價值比
        for key,value in dict_rows.items():
            print("\nStkIdx:{}, ValueRate:{}".format(key,value))
            local_pdDA = PandasDataAnalysis(key,dirnamelog,dirdatafolder)
            list_temp = local_pdDA.file1_updownrate_LastMonthYear(value)
            list_temp2.append(list_temp)
            #print(list_temp2)

        list_rows_bothprices.extend(list_temp2)

        #print(list_rows_bothprices)
        print("Output file(s): {}".format(dirlog_csv_bothprices))
        # Output results to CSV files
        localexcelrw.writeCSVbyTable(dirlog_csv_bothprices,list_rows_bothprices)      

    
    return dirlog_csv_bothprices
    

# custom function taken from https://stackoverflow.com/questions/12432663/what-is-a-clean-way-to-convert-a-string-percent-to-a-float
def percent2float(x):
    return float(x.strip('%'))/100

# sorting stock to buy
def file1_call(str_dirlogcsv):#"循環投資追蹤股"
    
    # read daily csv file of 循環投資追蹤股
    # # pass to convertes param as a dict
    df_csv = pd.read_csv(str_dirlogcsv, encoding = 'cp950', engine='python',
                        #header = 0, 
                        index_col = False,
                        usecols = [0,1,2,3,4,5,9,10],
                        converters={'1Y下跌率(%)':percent2float,
                                    '1M下跌率(%)':percent2float,
                                    'Lastday下跌率(%)':percent2float} )#sep=',',
    
    # sort by below citeria
    df_csv_call = df_csv.sort_values(['1Y下跌率(%)','1M下跌率(%)','Lastday下跌率(%)','價值比'], ascending=[False, False, False, True])
    #df_csv_call = df_csv.sort_values(['1Y下跌率','1M下跌率','Lastday下跌率'], ascending=[False, False, False])
    #print(df_csv_call)

    # convert float to percentage
    df_csv_call['1Y下跌率(%)'] = df_csv_call[['1Y下跌率(%)']].values *100
    df_csv_call['1M下跌率(%)'] = df_csv_call[['1M下跌率(%)']].values *100
    df_csv_call['Lastday下跌率(%)'] = df_csv_call[['Lastday下跌率(%)']].values *100
    #print("%.3f%%" %(df_csv_call[['1Y下跌率']].values*100))
    #print(df_csv_call)

    #1. 技術滿足
    #   1. 一年回跌率 < -25%
    #   2. 一個月回跌率 < -10%
    #   3. 當日跌幅超過 2%
    #   4. 大盤季線下彎
    #   5. 價值比大於 60
    df_csv_call_stock=df_csv_call.loc[(df_csv_call['1Y下跌率(%)'] > -25) & 
                                      (df_csv_call['1M下跌率(%)'] > -10) & 
                                      (df_csv_call['Lastday下跌率(%)'] < -2)]
    print('Stock to buy by {}'.format(str_dirlogcsv))
    print(df_csv_call_stock)

    # output *_buy.csv
    str_dirlogcsv_buy = re.sub(r"bothprices", "_buyranking", str_dirlogcsv)
    #str_dirlogcsv_buy = re.search(r"both+?", str_dirlogcsv)
    df_csv_call.to_csv(str_dirlogcsv_buy, encoding = 'cp950')

    str_dirlogcsv_buy_stock = re.sub(r"bothprices", "_buystock", str_dirlogcsv)
    df_csv_call_stock.to_csv(str_dirlogcsv_buy_stock, encoding = 'cp950')


# sorting stock to to sell
def file1_put(str_dirlogcsv):#"循環投資追蹤股"
    # read daily csv file of 循環投資追蹤股
    # # pass to convertes param as a dict
    df_csv = pd.read_csv(str_dirlogcsv, encoding = 'cp950', engine='python',
                        #header = 0, 
                        index_col = False,
                        usecols = [0,1,2,6,7,8,9,10],
                        converters={'1Y上昇率(%)':percent2float,
                                    '1M上昇率(%)':percent2float,
                                    'Lastday上昇率(%)':percent2float} )#sep=',',

    # sort by below citeria
    df_csv_put = df_csv.sort_values(['1Y上昇率(%)','1M上昇率(%)','Lastday上昇率(%)'], ascending=[False, False, False])

    # convert float to percentage
    df_csv_put['1Y上昇率(%)'] = df_csv_put[['1Y上昇率(%)']].values *100
    df_csv_put['1M上昇率(%)'] = df_csv_put[['1M上昇率(%)']].values *100
    df_csv_put['Lastday上昇率(%)'] = df_csv_put[['Lastday上昇率(%)']].values *100

    #1. 技術滿足
    #     1.1. 一年漲升率 > 35%
    #     1.2. 一個月漲升率 > 10%
    #     1.3. 當日漲幅超過 2%
    #     1.4. 大盤季線上彎
    df_csv_put_stock=df_csv_put.loc[(df_csv_put['1Y上昇率(%)'] > 35) & 
                                      (df_csv_put['1M上昇率(%)'] > 10) & 
                                      (df_csv_put['Lastday上昇率(%)'] > 2)]
    print('\nStock to sell by {}'.format(str_dirlogcsv))
    print(df_csv_put_stock)

    # output *_buy.csv
    str_dirlogcsv_sell = re.sub(r"bothprices", "_sellranking", str_dirlogcsv)
    #str_dirlogcsv_buy = re.search(r"both+?", str_dirlogcsv)
    df_csv_put.to_csv(str_dirlogcsv_sell, encoding = 'cp950')

    str_dirlogcsv_sell_stock = re.sub(r"bothprices", "_sellstock", str_dirlogcsv)
    df_csv_put_stock.to_csv(str_dirlogcsv_sell_stock, encoding = 'cp950')


def file2_main(list_excel_Seymour,dirnamelog,dirdatafolder):#"波段投機追蹤股"
    # Get present time
    local_time = time.localtime(time.time())

    localexcelrw = excelrw.ExcelRW()

    for excel_Seymour in list_excel_Seymour:
        print('將讀取Excel file:', excel_Seymour, '的資料')

        # Excel file including path
        dirlog_ExcelFile=os.path.join(dirnamelog,excel_Seymour)
        # Read values of each row
        list_row_value_price=localexcelrw.readExcel(dirlog_ExcelFile)

        # Output CSV file including path
        filename_csv_bothprices=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"bothprices"+".csv"
        filename_csv_belowprice=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"belowprice"+".csv"
        dirlog_csv_bothprices=os.path.join(dirnamelog,filename_csv_bothprices)
        dirlog_csv_belowprice=os.path.join(dirnamelog,filename_csv_belowprice)

        # Declare output contents
        list_rows_bothprices=[]
        list_rows_belowprice=[]
        head_rows=["代碼","公司","市價","1Q上昇率(%)","1Y下跌率(%)","Lastday上昇率(%)",
                    "1Q下跌率(%)","1Y上昇率(%)","Lastday下跌率(%)","價值比"]
        list_rows_bothprices.append(head_rows)
        list_rows_belowprice.append(head_rows)

        dict_rows = {}
        # get  all CSV files name under data folder
        for list_row_value in list_row_value_price:
            # get key=idx value=價值比 to store in dict
            dict_rows[list_row_value[0]] = list_row_value[2]
                
        list_temp2 =[]#to store return list
        # by key=idx value=價值比
        for key,value in dict_rows.items():
            print("\nStkIdx:{}, ValueRate:{}".format(key,value))
            local_pdDA = PandasDataAnalysis(key,dirnamelog,dirdatafolder)
            list_temp = local_pdDA.file2_updownrate_QuarterYear(value)
            list_temp2.append(list_temp)
            #print(list_temp2)

        list_rows_bothprices.extend(list_temp2)

        #print(list_rows_bothprices)
        print("Output file(s): {}".format(dirlog_csv_bothprices))
        # Output results to CSV files
        localexcelrw.writeCSVbyTable(dirlog_csv_bothprices,list_rows_bothprices)
    
    return dirlog_csv_bothprices    

# sorting stock to buy
def file2_call(str_dirlogcsv):#"波段投機追蹤股"
    
    # read daily csv file of 波段投機追蹤股
    # # pass to convertes param as a dict
    df_csv = pd.read_csv(str_dirlogcsv, encoding = 'cp950', engine='python',
                        #header = 0, 
                        index_col = False,
                        usecols = [0,1,2,3,4,5,9,10],
                        converters={'1Q上昇率(%)':percent2float,
                                    '1Y下跌率(%)':percent2float,
                                    'Lastday上昇率(%)':percent2float} )#sep=',',
    # sort by below citeria
    df_csv_call = df_csv.sort_values(['1Q上昇率(%)','1Y下跌率(%)','Lastday上昇率(%)','價值比'], ascending=[False, True, False, True])

    # convert float to percentage
    df_csv_call['1Q上昇率(%)'] = df_csv_call[['1Q上昇率(%)']].values *100
    df_csv_call['1Y下跌率(%)'] = df_csv_call[['1Y下跌率(%)']].values *100
    df_csv_call['Lastday上昇率(%)'] = df_csv_call[['Lastday上昇率(%)']].values *100

    #進場訊號: 成長股回檔反轉向上
    #           1. 價值比 > 60
    #           2. 一年回跌率 < -25%
    #           3. 季漲升率突破 10%
    df_csv_call_stock=df_csv_call.loc[(df_csv_call['1Y下跌率(%)'] > -25) & 
                                      (df_csv_call['1Q上昇率(%)'] > 10)]# & 
    #                                  (df_csv_call['價值比'] >= 60)]
    print('\nStock to buy by {}'.format(str_dirlogcsv))
    print(df_csv_call_stock)

    # output *_buy.csv
    str_dirlogcsv_buy = re.sub(r"bothprices", "_buyranking", str_dirlogcsv)
    df_csv_call.to_csv(str_dirlogcsv_buy, encoding = 'cp950')

    str_dirlogcsv_buy_stock = re.sub(r"bothprices", "_buystock", str_dirlogcsv)
    df_csv_call_stock.to_csv(str_dirlogcsv_buy_stock, encoding = 'cp950')                                 

# sorting stock to sell
def file2_put(str_dirlogcsv):#"波段投機追蹤股"
    
    # read daily csv file of 波段投機追蹤股
    # # pass to convertes param as a dict
    df_csv = pd.read_csv(str_dirlogcsv, encoding = 'cp950', engine='python',
                        #header = 0, 
                        index_col = False,
                        usecols = [0,1,2,6,7,8,9,10],
                        converters={'1Q下跌率(%)':percent2float,
                                    '1Y上昇率(%)':percent2float,
                                    'Lastday下跌率(%)':percent2float} )#sep=',',
    
    # sort by below citeria
    df_csv_put = df_csv.sort_values(['1Y上昇率(%)','1Q下跌率(%)','Lastday下跌率(%)'], ascending=[True, False, False])
    
    # convert float to percentage
    df_csv_put['1Y上昇率(%)'] = df_csv_put[['1Y上昇率(%)']].values *100
    df_csv_put['1Q下跌率(%)'] = df_csv_put[['1Q下跌率(%)']].values *100
    df_csv_put['Lastday下跌率(%)'] = df_csv_put[['Lastday下跌率(%)']].values *100

    #出場訊號:
    #1. 技術滿足: 高檔反轉向下
    #   1.1. 一年漲升率 > 35%
    #   1.2. 季回跌率破 -10%                                
    df_csv_put_stock=df_csv_put.loc[(df_csv_put['1Y上昇率(%)'] > 35) & 
                                      (df_csv_put['1Q下跌率(%)'] < -10)]
    print('\nStock to sell by {}'.format(str_dirlogcsv))
    print(df_csv_put_stock)

    # output *_buy.csv
    str_dirlogcsv_sell = re.sub(r"bothprices", "_sellranking", str_dirlogcsv)
    df_csv_put.to_csv(str_dirlogcsv_sell, encoding = 'cp950')

    str_dirlogcsv_sell_stock = re.sub(r"bothprices", "_sellstock", str_dirlogcsv)
    df_csv_put_stock.to_csv(str_dirlogcsv_sell_stock, encoding = 'cp950')

def file3_main(list_excel_Seymour,dirnamelog,dirdatafolder):#"景氣循環追蹤股"
    # Get present time
    local_time = time.localtime(time.time())

    localexcelrw = excelrw.ExcelRW()

    for excel_Seymour in list_excel_Seymour:
        print('將讀取Excel file:', excel_Seymour, '的資料')

        # Excel file including path
        dirlog_ExcelFile=os.path.join(dirnamelog,excel_Seymour)
        # Read values of each row
        list_row_value_price=localexcelrw.readExcel(dirlog_ExcelFile)

        # Output CSV file including path
        filename_csv_bothprices=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"bothprices"+".csv"
        filename_csv_belowprice=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"belowprice"+".csv"
        dirlog_csv_bothprices=os.path.join(dirnamelog,filename_csv_bothprices)
        dirlog_csv_belowprice=os.path.join(dirnamelog,filename_csv_belowprice)

        # Declare output contents
        list_rows_bothprices=[]
        list_rows_belowprice=[]
        head_rows=["代碼","公司","市價","3Y下跌率(%)","1Y下跌率(%)","1Q下跌率(%)",
                    "3Y上昇率(%)","1Y上昇率(%)","1Q上昇率(%)","PBR"]
        list_rows_bothprices.append(head_rows)
        list_rows_belowprice.append(head_rows)

        dict_rows = {}
        # get  all CSV files name under data folder
        for list_row_value in list_row_value_price:
            # get key=idx value=PBR to store in dict
            dict_rows[list_row_value[0]] = list_row_value[3]

        list_temp2 =[]#to store return list
        # by key=idx value=PBR
        for key,value in dict_rows.items():
            print("\nStkIdx:{}, PBR:{}".format(key,value))
            local_pdDA = PandasDataAnalysis(key,dirnamelog,dirdatafolder)
            list_temp = local_pdDA.file3_updownrate_threeYearoneYear(value)
            list_temp2.append(list_temp)
            #print(list_temp2)    

        list_rows_bothprices.extend(list_temp2)

        #print(list_rows_bothprices)
        print("Output file(s): {}".format(dirlog_csv_bothprices))
        # Output results to CSV files
        localexcelrw.writeCSVbyTable(dirlog_csv_bothprices,list_rows_bothprices)    
    
    return dirlog_csv_bothprices

# sorting stock to buy
def file3_call(str_dirlogcsv):#"景氣循環追蹤股"
    
    # read daily csv file of 景氣循環追蹤股
    # # pass to convertes param as a dict
    df_csv = pd.read_csv(str_dirlogcsv, encoding = 'cp950', engine='python',
                        #header = 0, 
                        index_col = False,
                        usecols = [0,1,2,3,4,5,9,10],
                        converters={'3Y下跌率(%)':percent2float,
                                    '1Y下跌率(%)':percent2float,
                                    '1Q下跌率(%)':percent2float} )#sep=',',

    # sort by below citeria
    df_csv_call = df_csv.sort_values(['3Y下跌率(%)','1Y下跌率(%)','1Q下跌率(%)','PBR'], ascending=[False, False, False, False])

    # convert float to percentage
    df_csv_call['3Y下跌率(%)'] = df_csv_call[['3Y下跌率(%)']].values *100
    df_csv_call['1Y下跌率(%)'] = df_csv_call[['1Y下跌率(%)']].values *100
    df_csv_call['1Q下跌率(%)'] = df_csv_call[['1Q下跌率(%)']].values *100

    #進場訊號: 景氣循環低點
    #         1. PBR < 1
    #         2. 三年回跌率 < -40%
    #         3. 一年回跌率 < -20%
    #         4. 5,20 日均線黃金交叉
    df_csv_call_stock=df_csv_call.loc[(df_csv_call['3Y下跌率(%)'] > -40) & 
                                      (df_csv_call['1Y下跌率(%)'] > -20) & 
                                      (df_csv_call['PBR'] <= 1)]
    print('\nStock to buy by {}'.format(str_dirlogcsv))
    print(df_csv_call_stock)

    # output *_buy.csv
    str_dirlogcsv_buy = re.sub(r"bothprices", "_buyranking", str_dirlogcsv)
    df_csv_call.to_csv(str_dirlogcsv_buy, encoding = 'cp950')

    str_dirlogcsv_buy_stock = re.sub(r"bothprices", "_buystock", str_dirlogcsv)
    df_csv_call_stock.to_csv(str_dirlogcsv_buy_stock, encoding = 'cp950') 

# sorting stock to sell
def file3_put(str_dirlogcsv):#"景氣循環追蹤股"
    
    # read daily csv file of 景氣循環追蹤股
    # # pass to convertes param as a dict
    df_csv = pd.read_csv(str_dirlogcsv, encoding = 'cp950', engine='python',
                        #header = 0, 
                        index_col = False,
                        usecols = [0,1,2,6,7,8,9,10],
                        converters={'3Y上昇率(%)':percent2float,
                                    '1Y上昇率(%)':percent2float,
                                    '1Q上昇率(%)':percent2float} )#sep=',',

    # sort by below citeria
    df_csv_put = df_csv.sort_values(['3Y上昇率(%)','1Y上昇率(%)','1Q上昇率(%)'], ascending=[False, False, False])

    # convert float to percentage
    df_csv_put['3Y上昇率(%)'] = df_csv_put[['3Y上昇率(%)']].values *100
    df_csv_put['1Y上昇率(%)'] = df_csv_put[['1Y上昇率(%)']].values *100
    df_csv_put['1Q上昇率(%)'] = df_csv_put[['1Q上昇率(%)']].values *100

    #出場訊號:
    #1. 技術滿足: 高檔反轉向下
    #       1.1. 三年漲升率 > 65%
    #       1.2. 一年漲升率 > 25%
    #       1.3. 5,20 日均線死亡交叉                               
    df_csv_put_stock=df_csv_put.loc[(df_csv_put['3Y上昇率(%)'] > 65) & 
                                      (df_csv_put['1Y上昇率(%)'] > 25)]
    print('\nStock to sell by {}'.format(str_dirlogcsv))
    print(df_csv_put_stock)

    # output *_buy.csv
    str_dirlogcsv_sell = re.sub(r"bothprices", "_sellranking", str_dirlogcsv)
    df_csv_put.to_csv(str_dirlogcsv_sell, encoding = 'cp950')

    str_dirlogcsv_sell_stock = re.sub(r"bothprices", "_sellstock", str_dirlogcsv)
    df_csv_put_stock.to_csv(str_dirlogcsv_sell_stock, encoding = 'cp950')
    


if __name__ == '__main__':

    strabspath=os.path.abspath(__file__)
    strdirname=os.path.dirname(strabspath)
    str_split=os.path.split(strdirname)
    prevdirname=str_split[0]
    dirnamelib=os.path.join(prevdirname,"lib")
    dirnamelog=os.path.join(prevdirname,"log")
    dirdatafolder = os.path.join(prevdirname,'test','data')

    sys.path.append(dirnamelib)

    import readConfig as readConfig
    import excelRW as excelrw
    import dataAnalysis as data_analysis
    import googleDrive as google_drive

    configPath=os.path.join(strdirname,"config.ini")
    localReadConfig = readConfig.ReadConfig(configPath)

    excel_file01 = localReadConfig.get_SeymourExcel("excelfile01") #"循環投資追蹤股"
    excel_file02 = localReadConfig.get_SeymourExcel("excelfile02") #"波段投機追蹤股"
    excel_file03 = localReadConfig.get_SeymourExcel("excelfile03") #"景氣循環追蹤股"
    excel_file04 = localReadConfig.get_SeymourExcel("excelfile04") #"公用事業追蹤股"
    stkidx_call_file01 = localReadConfig.get_SeymourExcel('stkidx_call_file01')
    stkidx_put_file01 = localReadConfig.get_SeymourExcel('stkidx_put_file01')
    stkidx_call_file02 = localReadConfig.get_SeymourExcel('stkidx_call_file02')
    stkidx_put_file02 = localReadConfig.get_SeymourExcel('stkidx_put_file02')
    stkidx_call_file03 = localReadConfig.get_SeymourExcel('stkidx_call_file03')
    stkidx_put_file03 = localReadConfig.get_SeymourExcel('stkidx_put_file03')
    stkidx_call_file04 = localReadConfig.get_SeymourExcel('stkidx_call_file04')
    stkidx_put_file04 = localReadConfig.get_SeymourExcel('stkidx_put_file04')
    str_first_year_month_day = localReadConfig.get_SeymourExcel("first_year_month_day")
    str_color_ma = localReadConfig.get_SeymourExcel('color_ma05_ma20_ma30')
    list_color_ma = str_color_ma.split(',')
    str_candlestick_weekly_subfolder = localReadConfig.get_SeymourExcel("candlestick_weekly_subfolder")

    #2018/10/31 remark casuse purge jpg files in def plotCandlestickandMA() 
    #Delete prvious candle stick jpg files.
    ###############################
    str_candlestick_filepath=os.path.join(dirnamelog,str_candlestick_weekly_subfolder)
    localgoogle_drive = google_drive.GoogleCloudDrive(str_candlestick_filepath)
    re_exp = r'\.jpg$'
    #list_filename = localgoogle_drive.querylocalfiles(re_exp)
    localgoogle_drive.purgelocalfiles(re_exp)

    ###############################
    # excute file1 #"循環投機追蹤股"
    ###############################
    list_excel_Seymour = [excel_file01]
    list_stkidx_call_file01 = stkidx_call_file01.split(',')
    list_stkidx_put_file01 = stkidx_put_file01.split(',')    
    
    #str_dirlogcsv_file01 = os.path.join(dirnamelog,'1031循環投資追蹤股bothprices.csv')
    #str_dirlogcsv_file01 = file1_main(list_excel_Seymour,dirnamelog,dirdatafolder)
    str_dirlogcsv_file01 = data_analysis.file1_main(list_excel_Seymour,dirnamelog,dirdatafolder,str_first_year_month_day)
    
    str_buysell_opt = 'call_循環'
    localdata_analysis = data_analysis.PandasDA_Excel(dirnamelog,list_excel_Seymour)

    # to filter whick stock to buy or to sell #"循環投機追蹤股"
    df_file01_stock_call = data_analysis.file1_call(str_dirlogcsv_file01)
    
    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA(df_file01_stock_call,dirnamelog,dirdatafolder,str_first_year_month_day,
                            list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)    
    
    print('Seymour suggests to buy in at {}:'.format(excel_file01))
    df_rtu = localdata_analysis.SeymourExcel01_call()
    df_file01_Seymour_call = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_call_file01,df_rtu)
    
    #to get stock index by diff df_file03_Seymour_call btw df_file03_stock_call
    df_file01_Seymour_call_compare = localdata_analysis.compare_twoarrarys(df_file01_Seymour_call,df_file01_stock_call)
    #print(df_file01_Seymour_call)

    # and then plot Candlestick and MA cruve if
    if len(df_file01_Seymour_call_compare) > 0:
        data_analysis.file_plotCandlestickMA(df_file01_Seymour_call_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    str_buysell_opt = 'put_循環'
    # to filter whick stock to buy or to sell #"循環投機追蹤股"
    df_file01_stock_put = data_analysis.file1_put(str_dirlogcsv_file01)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA(df_file01_stock_put,dirnamelog,dirdatafolder,str_first_year_month_day,
                            list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to sell out at {}:'.format(excel_file01))
    df_rtu = localdata_analysis.SeymourExcel01_put()
    df_file01_Seymour_put = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_put_file01,df_rtu)
    
    #to get stock index by diff df_file03_Seymour_put btw df_file03_stock_put
    df_file01_Seymour_put_compare = localdata_analysis.compare_twoarrarys(df_file01_Seymour_put,df_file01_stock_put)

    # and then plot Candlestick and MA cruve if
    if len(df_file01_Seymour_put_compare) > 0:
        data_analysis.file_plotCandlestickMA(df_file01_Seymour_put_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)
    ###############################
    # excute file2 #"波段投機追蹤股"
    ###############################
    list_excel_Seymour = [excel_file02]
    list_stkidx_call_file02 = stkidx_call_file02.split(',')#['2376','3450','3515','4144','4746','5388','6261','6271','8039','8042']
    list_stkidx_put_file02 = stkidx_put_file02.split(',')#['2439','2488','3653','5371','6202']
    
    #str_dirlogcsv_file02 = os.path.join(dirnamelog,'1012波段投機追蹤股bothprices.csv')
    #str_dirlogcsv_file02 = file2_main(list_excel_Seymour,dirnamelog,dirdatafolder)
    str_dirlogcsv_file02 = data_analysis.file2_main(list_excel_Seymour,dirnamelog,dirdatafolder,str_first_year_month_day)
    
    str_buysell_opt = 'call_波段'
    localdata_analysis = data_analysis.PandasDA_Excel(dirnamelog,list_excel_Seymour)
    
    # to filter whick stock to buy or to sell #"波段投機追蹤股"
    df_file02_stock_call = data_analysis.file2_call(str_dirlogcsv_file02)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA(df_file02_stock_call,dirnamelog,dirdatafolder,str_first_year_month_day,
                                list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('Seymour suggests to buy in at {}:'.format(excel_file02))
    df_rtu = localdata_analysis.SeymourExcel02_call()
    df_file02_Seymour_call = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_call_file02,df_rtu)

    #to get stock index by diff df_file03_Seymour_call btw df_file03_stock_call
    df_file02_Seymour_call_compare = localdata_analysis.compare_twoarrarys(df_file02_Seymour_call,df_file02_stock_call)
    #print(df_file02_Seymour_call)

    # and then plot Candlestick and MA cruve if
    if len(df_file02_Seymour_call_compare) > 0:
        data_analysis.file_plotCandlestickMA(df_file02_Seymour_call_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)        

    str_buysell_opt = 'put_波段'
    # to filter whick stock to buy or to sell #"波段投機追蹤股"
    df_file02_stock_put = data_analysis.file2_put(str_dirlogcsv_file02)

    # to get stock index then plot Candlestick and MA cruve
    data_analysis.file_plotCandlestickMA(df_file02_stock_put,dirnamelog,dirdatafolder,str_first_year_month_day,
                            list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)
    
    print('\nSeymour suggests to sell out at {}:'.format(excel_file02))
    df_rtu = localdata_analysis.SeymourExcel02_put()
    df_file02_Seymour_put = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_put_file02,df_rtu)
    
    #to get stock index by diff df_file03_Seymour_put btw df_file03_stock_put
    df_file02_Seymour_put_compare = localdata_analysis.compare_twoarrarys(df_file02_Seymour_put,df_file02_stock_put)

    # and then plot Candlestick and MA cruve if
    if len(df_file02_Seymour_put_compare) > 0:
        data_analysis.file_plotCandlestickMA(df_file02_Seymour_put_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
                                        list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    ###############################
    # excute file3 #"景氣循環追蹤股"
    ###############################
    list_excel_Seymour = [excel_file03]
    list_stkidx_call_file03 = stkidx_call_file03.split(',')
    list_stkidx_put_file03 = stkidx_put_file03.split(',')    

    #str_dirlogcsv_file03 = os.path.join(dirnamelog,'1012景氣循環追蹤股bothprices.csv')
    #str_dirlogcsv_file03 = file3_main(list_excel_Seymour,dirnamelog,dirdatafolder)  
    str_dirlogcsv_file03 = data_analysis.file3_main(list_excel_Seymour,dirnamelog,dirdatafolder,str_first_year_month_day)

    str_buysell_opt = 'call_景氣'
    localdata_analysis = data_analysis.PandasDA_Excel(dirnamelog,list_excel_Seymour)
    
    # to filter whick stock to buy or to sell #"景氣循環追蹤股"
    df_file03_stock_call = data_analysis.file3_call(str_dirlogcsv_file03)
    
    # to get stock index then plot Candlestick and MA cruve
    #data_analysis.file_plotCandlestickMA(df_file03_stock_call,dirnamelog,dirdatafolder,str_first_year_month_day,
    #                                    list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to buy in at {}:'.format(excel_file03))
    df_rtu = localdata_analysis.SeymourExcel03_call()
    df_file03_Seymour_call = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_call_file03,df_rtu)

    #to get stock index by diff df_file03_Seymour_call btw df_file03_stock_call
    df_file03_Seymour_call_compare = localdata_analysis.compare_twoarrarys(df_file03_Seymour_call,df_file03_stock_call)
    #print(df_file03_Seymour_call)

    # and then plot Candlestick and MA cruve if
    #if len(df_file03_Seymour_call_compare) > 0:
    #    data_analysis.file_plotCandlestickMA(df_file03_Seymour_call_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
    #                                    list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)                                    

    str_buysell_opt = 'put_景氣'
    # to filter whick stock to buy or to sell #"景氣循環追蹤股"
    df_file03_stock_put = data_analysis.file3_put(str_dirlogcsv_file03)

    # to get stock index then plot Candlestick and MA cruve
    #data_analysis.file_plotCandlestickMA(df_file03_stock_put,dirnamelog,dirdatafolder,str_first_year_month_day,
    #                                    list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to sold out at {}:'.format(excel_file03))
    df_rtu = localdata_analysis.SeymourExcel03_put()
    df_file03_Seymour_put = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_put_file03,df_rtu)

    #to get stock index by diff df_file03_Seymour_put btw df_file03_stock_put
    df_file03_Seymour_put_compare = localdata_analysis.compare_twoarrarys(df_file03_Seymour_put,df_file03_stock_put)

    # and then plot Candlestick and MA cruve if
    #if len(df_file03_Seymour_put_compare) > 0:
    #    data_analysis.file_plotCandlestickMA(df_file03_Seymour_put_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
    #                                    list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    ###############################
    # excute file4 #"公用事業追蹤股"
    ###############################
    list_excel_Seymour = [excel_file04]
    list_stkidx_call_file04 = stkidx_call_file04.split(',')
    list_stkidx_put_file04 = stkidx_put_file04.split(',')    

    #str_dirlogcsv_file04 = os.path.join(dirnamelog,'1012公用事業追蹤股bothprices.csv')
    #str_dirlogcsv_file04 = file4_main(list_excel_Seymour,dirnamelog,dirdatafolder)  
    #str_dirlogcsv_file04 = data_analysis.file4_main(list_excel_Seymour,dirnamelog,dirdatafolder,str_first_year_month_day)
    
    #2019/02/16 update to 低波固收
    str_dirlogcsv_file04 = data_analysis.file4_01_main_fromsqlite(list_excel_Seymour,dirnamelog,path_db,str_first_year_month_day,debug_verbose)

    #str_buysell_opt = 'call_公用'
    str_buysell_opt = 'call_低波固收'#2019/1/26 rename
    localdata_analysis = data_analysis.PandasDA_Excel(dirnamelog,list_excel_Seymour)
    
    # to filter whick stock to buy or to sell #"景氣循環追蹤股"
    #df_file04_stock_call = data_analysis.file4_call(str_dirlogcsv_file04)
    #2019/02/16 update to 低波固收
    df_file04_stock_call = data_analysis.file4_01_call(str_dirlogcsv_file04)

    # to get stock index then plot Candlestick and MA cruve
    #data_analysis.file_plotCandlestickMA(df_file04_stock_call,dirnamelog,dirdatafolder,str_first_year_month_day,
    #                                    list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to buy in at {}:'.format(excel_file04))
    df_rtu = localdata_analysis.SeymourExcel04_call()
    df_file04_Seymour_call = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_call_file04,df_rtu)

    #to get stock index by diff df_file04_Seymour_call btw df_file04_stock_call
    df_file04_Seymour_call_compare = localdata_analysis.compare_twoarrarys(df_file04_Seymour_call,df_file04_stock_call)
    #print(df_file03_Seymour_call)

    # and then plot Candlestick and MA cruve if
    #if len(df_file04_Seymour_call_compare) > 0:
    #    data_analysis.file_plotCandlestickMA(df_file04_Seymour_call_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
    #                                    list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    #str_buysell_opt = 'put_公用'
    str_buysell_opt = 'put_低波固收'#2019/1/26 rename

    # to filter whick stock to buy or to sell #"公用事業追蹤股"
    #df_file04_stock_put = data_analysis.file4_put(str_dirlogcsv_file04)
    #2019/02/16 update to 低波固收
    df_file04_stock_put = data_analysis.file4_01_put(str_dirlogcsv_file04)

    # to get stock index then plot Candlestick and MA cruve
    #data_analysis.file_plotCandlestickMA(df_file04_stock_put,dirnamelog,dirdatafolder,str_first_year_month_day,
    #                                    list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)

    print('\nSeymour suggests to sold out at {}:'.format(excel_file04))
    df_rtu = localdata_analysis.SeymourExcel04_put()
    df_file04_Seymour_put = localdata_analysis.SeymourExce_filterbystockidx(list_stkidx_put_file04,df_rtu)

    #to get stock index by diff df_file04_Seymour_put btw df_file04_stock_put
    df_file04_Seymour_put_compare = localdata_analysis.compare_twoarrarys(df_file04_Seymour_put,df_file04_stock_put)

    # and then plot Candlestick and MA cruve if
    #if len(df_file04_Seymour_put_compare) > 0:
    #    data_analysis.file_plotCandlestickMA(df_file04_Seymour_put_compare,dirnamelog,dirdatafolder,str_first_year_month_day,
    #                                    list_color_ma,str_candlestick_weekly_subfolder,str_buysell_opt)    