#2018/10/03 Intial test code related google firebase realtime database
#####################################################################
import os, sys, time
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

strabspath=os.path.abspath(__file__)
strdirname=os.path.dirname(strabspath)
dir_firebase_serviceaccount = os.path.join(strdirname,'twtseotc-moneyhunter-firebase.json')
db_firebase_URL = 'https://twtseotc-moneyhunter.firebaseio.com/'

# Fetch the service account key JSON file contents
cred = credentials.Certificate(dir_firebase_serviceaccount) # 替換成自己的 Service Account 本機位址
# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL' : db_firebase_URL # 替換成自己的 Firebase 網址
})

new_users = [
{'name': 'Richard Ho'},
{'name': 'Tom Wu'},
{'name': 'Judy Chen'},
{'name': 'Lind Chang'}
]

dict_new_users =\
{'dict_new_users': {{'name': 'Lind Chang'},
                    {'name': 'Richard Ho'},
                    {'name': 'Tom Wu'},
                    {'name': 'Judy Chen'}}
}

root = db.reference('/')
#root.child('new_users').push(new_users)
root.set(dict_new_users)
for u in new_users:
    print('Sotre the data:', u)
    root.child('user').push(u)
