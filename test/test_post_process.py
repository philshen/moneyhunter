# 2018/08/23 Move to .\main to trace code
# 2018/08/25 Move to .\test to trace code
#            Treat case "trade volume zero"
#            Pending test_SeymourExcel-02.py. Switch to test_post_process.py
# 2018/08/27 Add class SeymourExcel
# 2018/08/28 Add def file1_updownrate_LastMonthYear():#"循環投資追蹤股"
#######################################################
import os
from datetime import datetime
import sys

FOLDER = 'data'

def string_to_time(string):
    year, month, day = string.split('/')
    return datetime(int(year) + 1911, int(month), int(day))

def is_same(row1, row2):
    if not len(row1) == len(row2):
        return False

    for index in range(len(row1)):
        if row1[index] != row2[index]:
            return False
    else:
        return True

def main():
    file_names = os.listdir(FOLDER)
    for file_name in file_names:
        if not file_name.endswith('.csv'):
            continue

        dict_rows = {}

        # Load and remove duplicates (use newer)
        with open('{}/{}'.format(FOLDER, file_name), 'r') as file:
            for line in file.readlines():
                dict_rows[line.split(',', 1)[0]] = line
                

        # Sort by date
        rows = [row for date, row in sorted(
            dict_rows.items(), key=lambda x: string_to_time(x[0]))]
            # 2015/08/21 date shift to column 3
            #dict_rows.items(), key=lambda x: string_to_time(x[2]))]

        with open('{}/{}'.format(FOLDER, file_name), 'w') as file:
            file.writelines(rows)

def main_test():
    file_names = os.listdir(FOLDER)
    for file_name in file_names:
        if not file_name.endswith('.csv'):
            continue

        dict_rows = {}

        # Load and remove duplicates (use newer)
        with open('{}/{}'.format(FOLDER, file_name), 'r') as file:
            for line in file.readlines():
                #print(line.split(',', 1)[0])
                dict_rows[line.split(',', 1)[0]] = line
                #print(dict_rows)

        # Sort by date
        rows = [row for date, row in sorted(
            dict_rows.items(), key=lambda x: string_to_time(x[0]))]
        
        # trace code purpose
        #for key,value in dict_rows.items():
        #    print(key, value)
        #for row in rows:
        #    print(row)

        #with open('{}/{}'.format(FOLDER, file_name), 'w') as file:
        #    file.writelines(rows)

def main_datafoldercsv(twstockInfo,excel_flie):

    # declare
    localcrawlTSEOTC=twstockInfo.CrawlTSEOTC(excel_flie)
    list_sortedbydate=localcrawlTSEOTC.get_stockprice_datafoldercsv()
    list_sortedbydate_withoutfinalprice=localcrawlTSEOTC.exclude_nofinalprice_stockprice_datafoldercsv()

    # two neted lists difference
    filter_diff_sortedbydate = list(filter(lambda x:x not in list_sortedbydate_withoutfinalprice, list_sortedbydate)) 
    print(filter_diff_sortedbydate)
    print(len(filter_diff_sortedbydate))

    print(len(list_sortedbydate))       
    print(len(list_sortedbydate_withoutfinalprice))

    #print(list_sortedbydate[-1])
    print("{}{} final price of current day {}: {}".format(list_sortedbydate[-1][-2],
                                                          list_sortedbydate[-1][-1],
                                                          list_sortedbydate[-1][0],
                                                          list_sortedbydate[-1][1]))

    list_min_max_current_quarter = localcrawlTSEOTC.get_MinMax__byQuarter_fromLastday()
    list_min_max_current_year = localcrawlTSEOTC.get_MinMax__byYaer_fromLastday()

    print("Final price of Min, Max, Current by quarter from last day: {}, {}, {}".format(list_min_max_current_quarter[0],
                                                                                    list_min_max_current_quarter[1],
                                                                                    list_min_max_current_quarter[2]) )

    print("Final price of Min, Max, Current by year from last day: {}, {}, {}".format(list_min_max_current_year[0],
                                                                                    list_min_max_current_year[1],
                                                                                    list_min_max_current_year[2]) )

    list_rate_quarter = localcrawlTSEOTC.stockprice_raiserate_decreaserate(list_min_max_current_quarter)
    list_rate_year =  localcrawlTSEOTC.stockprice_raiserate_decreaserate(list_min_max_current_year)

    print("Raise/Decrease Rate by quarter: {:.3%} / {:.3%}".format(list_rate_quarter[0],list_rate_quarter[1]))
    print("Raise/Decrease Rate by year: {:.3%} / {:.3%}".format(list_rate_year[0],list_rate_year[1]))

import time


class SeymourExcel:
    FOLDER = 'data'

    def __init__(self,dirlog,list_excelSeymour):
        self.list_excel_Seymour = list_excelSeymour
        self.dirnamelog = dirlog

    def print_info_current(self,list_sortedbydate,list_min_max_lastday,list_rate_lastday):
        print("{}{} final price of last trading day {}: {}".format(list_sortedbydate[-1][-2],
                                                     list_sortedbydate[-1][-1],
                                                     list_sortedbydate[-1][0],
                                                     list_sortedbydate[-1][1]))
        print("Low, High, Close price of last trading day: {}, {}, {}".format(list_min_max_lastday[0],
                                                                            list_min_max_lastday[1],
                                                                            list_min_max_lastday[2]) )
        print("Raise/Decrease Rate of last trading day: {:.3%} / {:.3%}".format(list_rate_lastday[0],
                                                                                list_rate_lastday[1]))

    def print_info_month(self,list_min_max_current_month,list_rate_month):
        print("Final price of Min, Max, Current by month from last day: {}, {}, {}".format(list_min_max_current_month[0],
                                                                            list_min_max_current_month[1],
                                                                            list_min_max_current_month[2]) )
        
        print("Raise/Decrease Rate by month: {:.3%} / {:.3%}".format(list_rate_month[0],list_rate_month[1]))

    def print_info_quarter(self,list_min_max_current_quarter,list_rate_quarter):
        print("Final price of Min, Max, Current by quarter from last day: {}, {}, {}".format(list_min_max_current_quarter[0],
                                                                                    list_min_max_current_quarter[1],
                                                                                    list_min_max_current_quarter[2]))
        print("Raise/Decrease Rate by quarter: {:.3%} / {:.3%}".format(list_rate_quarter[0],list_rate_quarter[1]))

    def print_info_year(self,list_min_max_current_year,list_rate_year):
        print("Final price of Min, Max, Current by year from last day: {}, {}, {}".format(list_min_max_current_year[0],
                                                                            list_min_max_current_year[1],
                                                                            list_min_max_current_year[2]))
        print("Raise/Decrease Rate by year: {:.3%} / {:.3%}".format(list_rate_year[0],list_rate_year[1]))
    
    def file1_updownrate_LastMonthYear(self):#"循環投資追蹤股"
        # Get present time
        local_time = time.localtime(time.time())

        localexcelrw = excelrw.ExcelRW()

        for excel_Seymour in self.list_excel_Seymour:
            print('將讀取Excel file:', excel_Seymour, '的資料')

            # Excel file including path
            dirlog_ExcelFile=os.path.join(self.dirnamelog,excel_Seymour)
            # Read values of each row
            list_row_value_price=localexcelrw.readExcel(dirlog_ExcelFile)

            # Output CSV file including path
            filename_csv_bothprices=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"bothprices"+".csv"
            filename_csv_belowprice=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"belowprice"+".csv"
            dirlog_csv_bothprices=os.path.join(dirnamelog,filename_csv_bothprices)
            dirlog_csv_belowprice=os.path.join(dirnamelog,filename_csv_belowprice)

            # Declare output contents
            list_rows_bothprices=[]
            list_rows_belowprice=[]
            head_rows=["代碼","公司","市價","1Y下跌率","1M下跌率","Lastday下跌率",
                    "1Y上昇率","1M上昇率","Lastday上昇率",
                    "1YMin","1YMax","1MMin","1MMax","LastMin","LastMax","價格比"]
            list_rows_bothprices.append(head_rows)
            list_rows_belowprice.append(head_rows)

            dict_rows = {}

            # get  all CSV files name under data folder
            for list_row_value in list_row_value_price:
                # get key=idx value=價值比 to store in dict
                dict_rows[list_row_value[0]] = list_row_value[2]

            # get all infos by each CSV files under data folder
            for key,value in dict_rows.items():
                csv_file_datafolder='{}/{}.csv'.format(FOLDER, key)

                #print (csv_file_datafolder)
                # declare list type [csv_file_datafolder]
                localcrawlTSEOTC=twstockInfo.CrawlTSEOTC([csv_file_datafolder])
                list_sortedbydate=localcrawlTSEOTC.get_stockprice_datafoldercsv()
                list_sortedbydate_withoutfinalprice=localcrawlTSEOTC.exclude_nofinalprice_stockprice_datafoldercsv()

                # get Open, High, Low, Close price under data folder
                list_openhighlowclose_datafolder = localcrawlTSEOTC.get_OpenHighLowClose_stockprice()
                # get date, Open, High, Low, Close, Idx, CompName of last trading day
                list_openhighlowclose_lastday = list_openhighlowclose_datafolder[-1]
                # get Low, High, Close price
                list_min_max_lastday =[list_openhighlowclose_lastday[3],
                                       list_openhighlowclose_lastday[2],list_openhighlowclose_lastday[4] ]

                list_min_max_current_month = localcrawlTSEOTC.get_MinMax_byMonth_fromLastday()
                #list_min_max_current_quarter = localcrawlTSEOTC.get_MinMax_byQuarter_fromLastday()
                list_min_max_current_year = localcrawlTSEOTC.get_MinMax_byYaer_fromLastday()

                list_rate_lastday = localcrawlTSEOTC.stockprice_raiserate_decreaserate(list_min_max_lastday)
                list_rate_month = localcrawlTSEOTC.stockprice_raiserate_decreaserate(list_min_max_current_month)
                #list_rate_quarter = localcrawlTSEOTC.stockprice_raiserate_decreaserate(list_min_max_current_quarter)
                list_rate_year =  localcrawlTSEOTC.stockprice_raiserate_decreaserate(list_min_max_current_year)

                self.print_info_current(list_sortedbydate,list_min_max_lastday,list_rate_lastday)
                self.print_info_month(list_min_max_current_month,list_rate_month)
                #self.print_info_quarter(list_min_max_current_quarter,list_rate_quarter)
                self.print_info_year(list_min_max_current_year,list_rate_year)

                #head_rows=["代碼","公司","市價","1Y下跌率","1M下跌率","Lastday下跌率",
                #    "1Y上昇率","1M上昇率","Lastday上昇率",
                #    "1YMin","1YMax","1MMin","1MMax","LastMin","LastMax","價格比"]
                list_row_value_finalprice = [list_sortedbydate[-1][-2],
                                            list_sortedbydate[-1][-1],
                                            list_min_max_lastday[2],
                                            "%.3f%%" %(list_rate_year[1]*100),#"1Y下跌率"
                                            "%.3f%%" %(list_rate_month[1]*100),#"1M下跌率"
                                            "%.3f%%" %(list_rate_lastday[1]*100),#"Lastday下跌率"
                                            "%.3f%%" %(list_rate_year[0]*100),#"1Y上昇率"
                                            "%.3f%%" %(list_rate_month[0]*100),#"1M上昇率"
                                            "%.3f%%" %(list_rate_lastday[0]*100),#"Lastday上昇率"
                                            list_min_max_current_year[0],
                                            list_min_max_current_year[1],
                                            list_min_max_current_month[0],
                                            list_min_max_current_month[1],
                                            list_min_max_lastday[0],
                                            list_min_max_lastday[1],
                                            value]
                # append reslut to list
                list_rows_bothprices.append(list_row_value_finalprice)
            
            print("Output file(s):",dirlog_csv_bothprices)
            # Output results to CSV files
            localexcelrw.writeCSVbyTable(dirlog_csv_bothprices,list_rows_bothprices)

    def file2_updownrate_QuarterYear(self):#"波段投機追蹤股"
        # Get present time
        local_time = time.localtime(time.time())

        localexcelrw = excelrw.ExcelRW()

        for excel_Seymour in self.list_excel_Seymour:
            print('將讀取Excel file:', excel_Seymour, '的資料')

            # Excel file including path
            dirlog_ExcelFile=os.path.join(self.dirnamelog,excel_Seymour)
            # Read values of each row
            list_row_value_price=localexcelrw.readExcel(dirlog_ExcelFile)

            # Output CSV file including path
            filename_csv_bothprices=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"bothprices"+".csv"
            filename_csv_belowprice=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"belowprice"+".csv"
            dirlog_csv_bothprices=os.path.join(dirnamelog,filename_csv_bothprices)
            dirlog_csv_belowprice=os.path.join(dirnamelog,filename_csv_belowprice)

            # Declare output contents
            list_rows_bothprices=[]
            list_rows_belowprice=[]
            head_rows=["代碼","公司","市價","1Q上昇率","1Y下跌率",
                    "1Q下跌率","1Y上昇率","1QMin","1QMax","1YMin","1YMax","價格比"]
            list_rows_bothprices.append(head_rows)
            list_rows_belowprice.append(head_rows)

            dict_rows = {}

            # get  all CSV files name under data folder
            for list_row_value in list_row_value_price:
                # get key=idx value=價值比 to store in dict
                dict_rows[list_row_value[0]] = list_row_value[2]

            # get all infos by each CSV files under data folder
            for key,value in dict_rows.items():
                csv_file_datafolder='{}/{}.csv'.format(FOLDER, key)

                #print (csv_file_datafolder)
                # declare list type [csv_file_datafolder]
                localcrawlTSEOTC=twstockInfo.CrawlTSEOTC([csv_file_datafolder])
                list_sortedbydate=localcrawlTSEOTC.get_stockprice_datafoldercsv()
                list_sortedbydate_withoutfinalprice=localcrawlTSEOTC.exclude_nofinalprice_stockprice_datafoldercsv()

                # get Open, High, Low, Close price under data folder
                list_openhighlowclose_datafolder = localcrawlTSEOTC.get_OpenHighLowClose_stockprice()
                # get date, Open, High, Low, Close, Idx, CompName of last trading day
                list_openhighlowclose_lastday = list_openhighlowclose_datafolder[-1]
                # get Low, High, Close price
                list_min_max_lastday =[list_openhighlowclose_lastday[3],
                                       list_openhighlowclose_lastday[2],list_openhighlowclose_lastday[4] ]

                list_min_max_current_month = localcrawlTSEOTC.get_MinMax_byMonth_fromLastday()
                list_min_max_current_quarter = localcrawlTSEOTC.get_MinMax_byQuarter_fromLastday()
                list_min_max_current_year = localcrawlTSEOTC.get_MinMax_byYaer_fromLastday()

                list_rate_lastday = localcrawlTSEOTC.stockprice_raiserate_decreaserate(list_min_max_lastday)
                list_rate_month = localcrawlTSEOTC.stockprice_raiserate_decreaserate(list_min_max_current_month)
                list_rate_quarter = localcrawlTSEOTC.stockprice_raiserate_decreaserate(list_min_max_current_quarter)
                list_rate_year =  localcrawlTSEOTC.stockprice_raiserate_decreaserate(list_min_max_current_year)

                self.print_info_current(list_sortedbydate,list_min_max_lastday,list_rate_lastday)
                #self.print_info_month(list_min_max_current_month,list_rate_month)
                self.print_info_quarter(list_min_max_current_quarter,list_rate_quarter)
                self.print_info_year(list_min_max_current_year,list_rate_year)

                #head_rows=["代碼","公司","市價","上昇率_1Q","下跌率_1Y",
                #    "下跌率_1Q","上昇率_1Y","Min_1Q","Max_1Q","Min_1Y","Max_1Y","價值比"]
                list_row_value_finalprice = [list_sortedbydate[-1][-2],
                                            list_sortedbydate[-1][-1],
                                            list_min_max_current_quarter[2],
                                            "%.3f%%" %(list_rate_quarter[0]*100),#"上昇率_1Q"
                                            "%.3f%%" %(list_rate_year[1]*100),#"下跌率_1Y"
                                            "%.3f%%" %(list_rate_quarter[1]*100),#"下跌率_1Q"                                        
                                            "%.3f%%" %(list_rate_year[0]*100),#"上昇率_1Y"
                                            list_min_max_current_quarter[0],
                                            list_min_max_current_quarter[1],
                                            list_min_max_current_year[0],
                                            list_min_max_current_year[1],
                                            value]
                # append reslut to list
                list_rows_bothprices.append(list_row_value_finalprice)
            
            print("Output file(s):",dirlog_csv_bothprices)
            # Output results to CSV files
            localexcelrw.writeCSVbyTable(dirlog_csv_bothprices,list_rows_bothprices)

if __name__ == '__main__':
    strabspath=os.path.abspath(__file__)
    strdirname=os.path.dirname(strabspath)
    str_split=os.path.split(strdirname)
    prevdirname=str_split[0]
    dirnamelib=os.path.join(prevdirname,"lib")
    dirnamelog=os.path.join(prevdirname,"log")
    
    sys.path.append(dirnamelib)

    import readConfig as readConfig
    import excelRW as excelrw
    import twstockInfo as twstockInfo


    #main()
    #main_test()
    

    configPath=os.path.join(strdirname,"config.ini")
    localReadConfig = readConfig.ReadConfig(configPath)

    excel_file01 = localReadConfig.get_SeymourExcel("excelfile01") #"循環投資追蹤股"
    excel_file02 = localReadConfig.get_SeymourExcel("excelfile02") #"波段投機追蹤股"
    excel_file03 = localReadConfig.get_SeymourExcel("excelfile03") #"景氣循環追蹤股"
    excel_file04 = localReadConfig.get_SeymourExcel("excelfile04") #"公用事業追蹤股"

    #localexcelrw = excelrw.ExcelRW()

    #list_excel_Seymour = [excel_file01,excel_file02,excel_file03,excel_file04] #景氣循環追蹤股,合理價格為零,所以去除
    list_excel_Seymour = [excel_file02]

    localSeymourExcel = SeymourExcel(dirnamelog,list_excel_Seymour)
    localSeymourExcel.file2_updownrate_QuarterYear()    

    list_excel_Seymour = [excel_file01]
    localSeymourExcel = SeymourExcel(dirnamelog,list_excel_Seymour)
    localSeymourExcel.file1_updownrate_LastMonthYear()

    #file_names = os.listdir(FOLDER)
    #list_csv_file=[]

    # Get present time
    #local_time = time.localtime(time.time())