## https://github.com/philip-shen/google_sheets_update
## 2018/12/04 Adapter from UpdateGSheets.py             
#################################################################################
import os,sys,time,datetime
import matplotlib.pylab as mpl

strabspath=os.path.abspath(__file__)
strdirname=os.path.dirname(strabspath)
str_split=os.path.split(strdirname)
prevdirname=str_split[0]
dirnamelib=os.path.join(prevdirname,"lib")
dirnamelog=os.path.join(prevdirname,"log")
path_db = os.path.join(dirnamelog,'TWTSEOTCDaily.db')

sys.path.append(dirnamelib)
import readConfig as readConfig
import googleSS as googleSS
import googleDrive as google_drive

#2018/11/19 Only available on on matplotlib 2.x
mpl.rcParams['font.sans-serif'] = ['SimHei'] #將預設字體改用SimHei字體for中文

localReadConfig = readConfig.ReadConfig('config.ini')
gspreadsheet = localReadConfig.get_GSpread("sheet02")
list_worksheet_spread = localReadConfig.get_WorkSheet_Amy1210("worksheet").split(',')
str_delay_sec = localReadConfig.get_WorkSheet_Amy1210("delay_sec")

#2019/1/10(Thu) excute this code meet from Mon. to Fri unreamrk below.
#str_first_year_month_day = localReadConfig.get_SeymourExcel("first_year_month_day")
str_first_year_month_day = datetime.date.today().strftime('%Y,%m,%d')# ex:2018,10,16 get today date form system

list_color_ma = localReadConfig.get_SeymourExcel('color_ma05_ma20_ma30').split(',')
str_candlestick_weeklysubfolder = 'DailyReview'

#Delete prvious candle stick jpg files.
###############################
str_candlestick_filepath=os.path.join(dirnamelog,str_candlestick_weeklysubfolder)
localgoogle_drive = google_drive.GoogleCloudDrive(str_candlestick_filepath)
re_exp = r'\.jpg$'
#list_filename = localgoogle_drive.querylocalfiles(re_exp)
localgoogle_drive.purgelocalfiles(re_exp)

GDriveJSON = 'PythonGSheetUpload.json'
print('將讀取試算表' ,gspreadsheet , '的資料')

scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
# Declare GoogleSS() from googleSS.py
localGoogleSS=googleSS.GoogleSS()
localGoogleSS.auth_gss_client(GDriveJSON, scope)

for worksheet_spread in list_worksheet_spread:
    # measure time consumption
    start = time.time()

    localGoogleSS.open_GSworksheet(gspreadsheet,worksheet_spread)

    print('將讀取', gspreadsheet, '中WorkSheet:', worksheet_spread, '的資料')
    #inital row count value 2
    row_count = 2

    localGoogleSS.plotCandlestickMA_GSpreadworksheet_logfolderdb(row_count,str_delay_sec,
                                            dirnamelog,path_db,str_first_year_month_day,
                                            list_color_ma,str_candlestick_weeklysubfolder)

    duration = time.time() - start
    print('Update data of {} duration: {:.2f} seconds'.format(worksheet_spread,duration)) 