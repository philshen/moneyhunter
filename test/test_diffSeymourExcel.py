# 2018/09/10 Initial to diff Seymour's excel files.
#            adapt from test_diffSeymourExcel.py   
###########################################################
import pandas as pd
from datetime import datetime, timedelta
import time
import os
import sys

class PandasDA_Excel:
    def __init__(self,dirnamelog,list_xlsfile):

        xls01_logfolder = '{}/{}'.format(dirnamelog,list_xlsfile[0])
        xls02_logfolder = '{}/{}'.format(dirnamelog,list_xlsfile[1])
        self.dirnamelog = dirnamelog

        # get stkidx and CmpName from excel file
        xls01_Seymour = pd.read_excel(xls01_logfolder, encoding = 'cp950',
                            usecols = [1,2])
        
        df01 = xls01_Seymour.copy()
        self.df01 = df01

        xls02_Seymour = pd.read_excel(xls02_logfolder, encoding = 'cp950',
                            usecols = [1,2])
        
        df02 = xls02_Seymour.copy()
        self.df02 = df02
        #print(self.df)

        # get row count after sort index
        print("original row counts of {}: {}".format(list_xlsfile[0],len(self.df01.index)))
        print("original row counts of {}: {}".format(list_xlsfile[1],len(self.df02.index)))

    def diff_twodataframes(self):
        pd_diff = pd.concat([self.df01,self.df02]).drop_duplicates(keep=False)
        print(pd_diff)


if __name__ == '__main__':
    strabspath=os.path.abspath(__file__)
    strdirname=os.path.dirname(strabspath)
    str_split=os.path.split(strdirname)
    prevdirname=str_split[0]
    dirnamelib=os.path.join(prevdirname,"lib")
    dirnamelog=os.path.join(prevdirname,"log")
    dirdatafolder = os.path.join(prevdirname,'test','data')

    sys.path.append(dirnamelib)

    import readConfig as readConfig
    import excelRW as excelrw
    import dataAnalysis as data_analysis

    configPath=os.path.join(strdirname,"diffExcel.ini")
    localReadConfig = readConfig.ReadConfig(configPath)

    excel_file01_1st = localReadConfig.get_SeymourExcel("excelfile01_1st") #"循環投資追蹤股"
    excel_file02_1st = localReadConfig.get_SeymourExcel("excelfile02_1st") #"波段投機追蹤股"
    excel_file03_1st = localReadConfig.get_SeymourExcel("excelfile03_1st") #"景氣循環追蹤股"
    excel_file04_1st = localReadConfig.get_SeymourExcel("excelfile04_1st") #"公用事業追蹤股"

    excel_file01_2nd = localReadConfig.get_SeymourExcel("excelfile01_2nd") #"循環投資追蹤股"
    excel_file02_2nd = localReadConfig.get_SeymourExcel("excelfile02_2nd") #"波段投機追蹤股"
    excel_file03_2nd = localReadConfig.get_SeymourExcel("excelfile03_2nd") #"景氣循環追蹤股"
    excel_file04_2nd = localReadConfig.get_SeymourExcel("excelfile04_2nd") #"公用事業追蹤股"

    #print(excel_file01_1st,excel_file04_2nd)
    list_excel_file01 = [excel_file01_1st,excel_file01_2nd]
    #localpdda_excel= PandasDA_Excel(dirnamelog,list_excel_file01)
    localpdda_excel= data_analysis.PandasDA_Excel(dirnamelog,list_excel_file01)
    localpdda_excel.diff_twodataframes()

    list_excel_file02 = [excel_file02_1st,excel_file02_2nd]
    #localpdda_excel= PandasDA_Excel(dirnamelog,list_excel_file02)
    localpdda_excel= data_analysis.PandasDA_Excel(dirnamelog,list_excel_file02)
    localpdda_excel.diff_twodataframes()

    list_excel_file03 = [excel_file03_1st,excel_file03_2nd]
    #localpdda_excel= PandasDA_Excel(dirnamelog,list_excel_file03)
    localpdda_excel= data_analysis.PandasDA_Excel(dirnamelog,list_excel_file03)
    localpdda_excel.diff_twodataframes()

    list_excel_file04 = [excel_file04_1st,excel_file04_2nd]
    #localpdda_excel= PandasDA_Excel(dirnamelog,list_excel_file04)
    localpdda_excel= data_analysis.PandasDA_Excel(dirnamelog,list_excel_file04)
    localpdda_excel.diff_twodataframes()
