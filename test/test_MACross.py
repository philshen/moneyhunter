# 2018/08/31 Initial to use pandas, matplotlib,TAlib
# 2018/09/01 Using pandas to calculate MA
#            Calculate golden and dead MA
#            Add class PandasDataAnalysis
#            Adapte from test_TALib.py
########################################################

# -- coding: utf-8 --
import talib
import pandas as pd
import matplotlib.pyplot  as plt
import numpy as np
from datetime import datetime, timedelta
import time
import os
import sys

def plotchart(ptr_df,str_label,str_title):
    ptr_df.close.plot(color='#177A96', label=str_label)
    plt.title(str_title)
    plt.grid(True)
    plt.show()

def plotMAchart(list_ptr_df,list_label,str_title):
    plt.figure(figsize=(10,5))
    plt.plot(list_ptr_df[0], color='#DE5B49', label=list_label[0], alpha=0.8, linewidth=3)
    plt.plot(list_ptr_df[1], color='#324D5C', label=list_label[1], alpha=0.8, linewidth=3)
    plt.plot(list_ptr_df[2], color='#46B29D', label=list_label[2], alpha=0.8, linewidth=3)
    plt.legend(loc='upper left')
    plt.xlabel('date', color='c')
    plt.ylabel('value', color='c')
    plt.grid(True)
    plt.title(str_title)
    plt.tick_params(labelcolor='tab:orange')
    plt.show()

def getMidvalue_lengthNum(inp_num):
    list_lengthNum = []
    # from 0 to inp_num not including inp_num
    for i in range(inp_num):
        list_lengthNum.append(i)
    
    #四捨五入中位數取其位置
    median_value = int(round(np.median(list_lengthNum)))
    return median_value

def plotMA05MA20MA30(data_frame,str_title):
    #在talib中，輸入輸出都需要用array，參數二則是你要選擇的n天，第三參數選擇均線的類型
    SMA_05 = talib.MA(np.array(data_frame.close), timeperiod=5, matype=0)
    SMA_20 = talib.MA(np.array(data_frame.close), timeperiod=20, matype=0)
    SMA_30 = talib.MA(np.array(data_frame.close), timeperiod=30, matype=0)

    #使用matplotlib繪圖之前先將array轉成DataFrame
    df_SMA_05 = pd.DataFrame(SMA_05, index = data_frame.index, columns = ['SMA05'])
    df_SMA_20 = pd.DataFrame(SMA_20, index = data_frame.index, columns = ['SMA20'])
    df_SMA_30 = pd.DataFrame(SMA_30, index = data_frame.index, columns = ['SMA30'])

    list_ptr_df = [df_SMA_05,df_SMA_20,df_SMA_30]
    list_label = ['SMA_05','SMA_20','SMA_30']
    plotMAchart(list_ptr_df,list_label,str_title)

def main01(csv_datafolder):
    # get date, open, high, low, close price and volume from csv file
    ##################index_col = [0] ###############
    # dataframe as below:
    #               volume   open   high    low  close CmpName
    #date
    #2017-07-03    773400  13.75  13.85  13.70  13.80      台航
    #2017-07-04   2154492  13.85  14.30  13.75  13.75      台航
    #2017-07-05    504327  13.75  13.80  13.55  13.75      台航
    csv_stockfile = pd.read_csv(csv_datafolder, header = None, encoding = 'cp950', 
                                usecols = [0,1,3,4,5,6,9,10], index_col = [0], 
                                names = ['date', 'volume','open', 'high', 'low', 'close', 'Stkidx','CmpName'],
                                parse_dates = [0],
                                date_parser = lambda x:pd.datetime.strptime(x,'%Y/%m/%d'))
    df = csv_stockfile.copy()

    #df.sort_index(ascending=0,inplace=True)
    df.sort_index(ascending=1,inplace=True)

    #print(df.to_string())
    # get row count after sort index
    print("original row counts: {}".format(len(df.index)))

    df_delduplicates = df.drop_duplicates()
    
    # get row count after delet duplicated row
    print("row counts after drop duplicated rows: {}".format(len(df_delduplicates.index)) )
    print(df_delduplicates)

    # row idx from 0 to extend 10 items; column idx from 1 to extend 7 items including close price
    #print(df_delduplicates.iloc[0:10,1:7])
    #print(df_delduplicates.iloc[-1:,5])
    #print(df_delduplicates.iloc[-1:,6])
    list_str = [df_delduplicates.iloc[0,5].astype(str) , df_delduplicates.iloc[0,6]]
    title = ''.join(list_str)#stkidx+CmpName

    len_df_delduplicates = len(df_delduplicates.index)

    # get last day value
    ts_endday = df_delduplicates[-1:].index.tolist()[0]
    #print(ts_endday)
    # Pandas: Convert Timestamp to datetime.date
    dt_endday = pd.Timestamp(ts_endday).date()
    #print(dt_endday)

    #subtract 90 days
    dt_startdate = dt_endday - timedelta(days=90)
    print("Start Date:{} End Date:{}".format(dt_startdate,dt_endday))
    #print(df_delduplicates.index[0])
    #print(df_delduplicates.to_string())

    #chose start position from startpos_idx    
    #startpos_idx = getMidvalue_lengthNum(len_df_delduplicates)
    startpos_idx = -90
    #print("Half start position: {}".format(startpos_idx))
    plotMA05MA20MA30(df_delduplicates.iloc[startpos_idx:], title)



strabspath=os.path.abspath(__file__)
strdirname=os.path.dirname(strabspath)
str_split=os.path.split(strdirname)
prevdirname=str_split[0]
dirnamelib = os.path.join(prevdirname,"lib")
dirnamelog = os.path.join(prevdirname,"log")
dirdatafolder = os.path.join(prevdirname,'test','data')
    
sys.path.append(dirnamelib)

import readConfig as readConfig
import excelRW as excelrw
import dataAnalysis as data_analysis

configPath=os.path.join(strdirname,"config.ini")
localReadConfig = readConfig.ReadConfig(configPath)

excel_file01 = localReadConfig.get_SeymourExcel("excelfile01") #"循環投資追蹤股"
excel_file02 = localReadConfig.get_SeymourExcel("excelfile02") #"波段投機追蹤股"
excel_file03 = localReadConfig.get_SeymourExcel("excelfile03") #"景氣循環追蹤股"
excel_file04 = localReadConfig.get_SeymourExcel("excelfile04") #"公用事業追蹤股"
list_excel_Seymour = [excel_file03]

str_last_year_month_day = localReadConfig.get_SeymourExcel("last_year_month_day")
str_first_year_month_day = localReadConfig.get_SeymourExcel("first_year_month_day")


FOLDER = 'data'
#stkidx = '2617'#台航

list_all_stockidx = []
# get all stock idx from excelfile03 #"景氣循環追蹤股"
localexcelrw = excelrw.ExcelRW()
list_all_stockidx=localexcelrw.get_all_stockidx_SeymourExcel(dirnamelog,list_excel_Seymour)

#for stkidx in list_all_stockidx:
#    print('\n將讀取 {}/{}.csv 的資料作 90Days MA CrossOver分析。'.format(FOLDER,stkidx[0]))
#    localDA = data_analysis.PandasDataAnalysis(stkidx[0],dirnamelog,dirdatafolder)
#    localDA.MACrossOver()

for stkidx in list_all_stockidx:
    print('\n將讀取 {}/{}.csv 的資料作 90Days MA CrossOver分析。'.format(FOLDER,stkidx[0]))
    localDA = data_analysis.PandasDataAnalysis(stkidx[0],dirnamelog,dirdatafolder,str_first_year_month_day)
    rtu_value = localDA.MACrossOver()
    
    if bool(rtu_value):
        print('\n將讀取 {}/{}.csv 的資料作 90Days MA CrossOver圖形。'.format(dirdatafolder,stkidx[0]))
        localDA.plotMACrossOver()

    #csv_datafolder = '{}\{}.csv'.format(dirdatafolder,stkidx[0])
    #main01(csv_datafolder)