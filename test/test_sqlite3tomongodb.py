#2018/10/15 Intial test code to crawl weekly targets csv files.
#2018/10/27 Adapte from test_sqilte3.py for daily crawl.
#2019/01/10 Excute this code meet from Mon. to Fri unreamrk below.
#2019/02/02 Adapt from test_crawl2sqlite3.py to read from sqliteDB to mongoDB
####################################################
import os,sys,time,logging,datetime

strabspath=os.path.abspath(__file__)
strdirname=os.path.dirname(strabspath)
str_split=os.path.split(strdirname)
prevdirname=str_split[0]
dirnamelib=os.path.join(prevdirname,"lib")
dirnamelog=os.path.join(prevdirname,"log")
dirdatafolder = os.path.join(prevdirname,'test','data')

sys.path.append(dirnamelib)
import excelRW as excelrw
import readConfig as readConfig
import googleDrive as google_drive
import twstockInfo as twstock_info

# Set logging
if not os.path.isdir('log'):
    os.makedirs('log')

logging.basicConfig(filename='log/crawl-error.log',level=logging.ERROR,
                    format='%(asctime)s\t[%(levelname)s]\t%(message)s',
                    datefmt='%Y/%m/%d %H:%M:%S')

# Get present time
t0 = time.time()
local_time = time.localtime(t0)
start_time = local_time
print('Start Time is ', local_time.tm_year,'/',local_time.tm_mon,'/',local_time.tm_mday,' ',local_time.tm_hour,":",local_time.tm_min,":",local_time.tm_sec)
logging.error('Begin Time:')

configPath=os.path.join(strdirname,"config.ini")
localReadConfig = readConfig.ReadConfig(configPath)

#2019/1/10(Thu) excute this code dosen't meet from Mon. to Fri unremak below.
str_last_year_month_day = localReadConfig.get_SeymourExcel("last_year_month_day")
str_first_year_month_day = localReadConfig.get_SeymourExcel("first_year_month_day")

#2019/1/10(Thu) excute this code meet from Mon. to Fri unreamrk below.
#str_last_year_month_day = datetime.date.today().strftime('%Y,%m,%d')# ex:2018,10,16 get today date form system
#str_first_year_month_day = datetime.date.today().strftime('%Y,%m,%d')# ex:2018,10,16 get today date form system


# 2018/10/24 Initial to sqlite test code
path_db = os.path.join(dirnamelog,'TWTSEOTCDaily.db')

localtwstock_info_db_sqlite = twstock_info.DB_sqlite(path_db)
# create a database connection
conn = localtwstock_info_db_sqlite.create_connection()
if conn is not None:
    # create projects table
    #localtwstock_info_db.create_table(conn, sql_create_TseOtcDaily_table)
    pass
else:
    print("Error! cannot create the database connection.")


#2019/2/2 cause return value from query_date_fromfirst_tolast_fromsqlite() is tuple
'''
list_tseotc_dailytradeinfo = []

# 2018/10/27 Optimize flow to get all stock idxs from Excel files and then execute main_fromfirst_tolast()
localtwstock_info = twstock_info.CrawlTSEOTC([],os.path.join(strdirname,"log"),str_first_year_month_day,str_last_year_month_day)
list_tseotc_dailytradeinfo_bydate = localtwstock_info.query_date_fromfirst_tolast_fromsqlite(localtwstock_info_db_sqlite,conn)


list_element = []

for tuple_tseotc_dailytradeinfo_bydate in list_tseotc_dailytradeinfo_bydate:
    for tseotc_dailytradeinfo_bydate in tuple_tseotc_dailytradeinfo_bydate:
        dic_element={'date':tseotc_dailytradeinfo_bydate[0],
                     'stockno':tseotc_dailytradeinfo_bydate[-2],
                     'cmpname':tseotc_dailytradeinfo_bydate[-1],
                     'shares':tseotc_dailytradeinfo_bydate[1],
                     'amount':tseotc_dailytradeinfo_bydate[2],
                     'open':tseotc_dailytradeinfo_bydate[3],
                     'high':tseotc_dailytradeinfo_bydate[4], 
                     'low':tseotc_dailytradeinfo_bydate[5],
                     'close':tseotc_dailytradeinfo_bydate[6],
                     'diff':tseotc_dailytradeinfo_bydate[7],
                     'turnover':tseotc_dailytradeinfo_bydate[8]};  #製作MongoDB的插入元素    
        list_element.append(dic_element)#append all dic_element for bluck insert

print(list_element)
'''

mongo_host = localReadConfig.get_MongoDB('mongo_host')
mongo_db = localReadConfig.get_MongoDB('mongo_db')
mongo_collection = localReadConfig.get_MongoDB('mongo_collection')
mongo_username = localReadConfig.get_MongoDB('mongo_username') 
mongo_password = localReadConfig.get_MongoDB('mongo_password')

localtwstock_info_db_mongo = twstock_info.DB_mongo()
# create a database connection

collection = localtwstock_info_db_mongo.connect_mongo(mongo_host,mongo_db,
                    mongo_collection,mongo_username,mongo_password)

# insert daily data to mongodb
localtwstock_info = twstock_info.CrawlTSEOTC([],os.path.join(strdirname,"log"),str_first_year_month_day,str_last_year_month_day)
localtwstock_info.insert_date_fromfirst_tolast_tomongodb(localtwstock_info_db_sqlite,conn,collection)

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()

# Get the last time
t1 = time.time()
local_time = time.localtime(t1)
print('Final Time is ', local_time.tm_year,'/',local_time.tm_mon,'/',local_time.tm_mday,' ',local_time.tm_hour,":",local_time.tm_min,":",local_time.tm_sec)

msg = 'Collection(s) insertion in {:.2f} seconds.'
print(msg.format(t1 - t0))
logging.info(msg.format(t1 - t0))
logging.error('Finish Time:')