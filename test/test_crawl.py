# -*- coding: utf-8 -*-
#2018/08/19 Verify to uesful current TWSE/OTC status.
#           Modify crawl.py to meet my need.
#2018/08/20 Check stock indexs if meet to Seymour's
#           Start to trace code
#2018/08/21 Add test_targetstock()
#2018/08/22 Change logout date to AD
#           Add def main_fromfirst_tolast()
#           To get first and last date from config.ini
#2018/08/23 Add get_all_stockidx_SeymourExcel()
#           Optimize flow to get all stock idxs from Excel files and then execute main_fromfirst_tolast()
#           Modify from test_crawl.py to main/crawl_TSEOTC.py       
#######################################################

import os
import re
import sys
import csv
import time
import string
import logging
import requests
import argparse
from datetime import datetime, timedelta

from os import mkdir
from os.path import isdir

class Crawler():
    def __init__(self,list_stkidx_stkname, prefix="data"):
        ''' Make directory if not exist when initialize '''
        if not isdir(prefix):
            mkdir(prefix)
        self.prefix = prefix
        self.list_stkidx_stkname = list_stkidx_stkname
    
    ''' 7/20/2019 record
    ['0050', '元大台灣50', '9,780,922', '3,437', '814,129,776', '83.00', '83.45', '83.00', '83.00', 'X', '0.00', '83.00', '323', '83.05', '30', '0.00'], 
    ['0051', '元大中型100', '15,400', '12', '512,940', '33.35', '33.50', '33.25', '33.29', '<p style= color:red>+</p>', '0.14', '33.29', '19', '33.48', '2', '0.00'], 
    ['0052', '富邦科技', '117,908', '22', '6,708,795', '56.45', '56.95', '56.45', '56.85', '<p style= color:red>+</p>', '0.95', '56.85', '1', '56.95', '1', '0.00'], 
    ['0053', '元大電子', '12,000', '5', '431,420', '36.00', '36.14', '35.89', '35.89', '<p style= color:red>+</p>', '0.44', '35.81', '30', '36.03', '30', '0.00'],
    ['0054', '元大台商50', '1,000', '1', '22,600', '22.60', '22.60', '22.60', '22.60', 'X', '0.00', '22.60', '1', '22.70', '6', '0.00'], 
    ['0055', '元大MSCI金融', '36,000', '10', '667,460', '18.52', '18.59', '18.52', '18.53', '<p style= color:red>+</p>', '0.01', '18.53', '9', '18.57', '11', '0.00'], 
    ['0056', '元大高股息', '5,264,174', '1,846', '142,772,960', '27.13', '27.17', '27.03', '27.06', ' ', '0.00', '27.06', '4', '27.07', '18', '0.00'], 
    ['0061', '元大寶滬深', '415,564', '155', '7,564,043', '18.08', '18.29', '18.08', '18.23', '<p style= color:red>+</p>', '0.15', '18.22', '23', '18.23', '32', '0.00'], 
    ['006205', '富邦上証', '1,761,093', '228', '55,969,255', '31.55', '31.91', '31.55', '31.74', '<p style= color:red>+</p>', '0.26', '31.72', '53', '31.74', '89', '0.00'], 
    ['00736', '國泰新興市場', '124,000', '13', '2,505,010', '20.20', '20.25', '20.18', '20.21', '<p style= color:green>-</p>', '0.12', '20.19', '12', '20.21', '48', '0.00'], 
    ['00710B', 'FH彭博高收益債', '1,056,000', '208', '21,971,560', '20.82', '20.82', '20.79', '20.79', '<p style= color:green>-</p>', '0.05', '20.79', '4', '20.80', '2', '0.00'], 

    ['1101', '台泥', '18,104,510', '5,577', '808,328,336', '44.70', '44.90', '44.55', '44.80', '<p style= color:red>+</p>', '0.20', '44.75', '9', '44.80', '401', '10.72'], 
    ['1101B', '台泥乙特', '15,254', '15', '797,806', '52.30', '52.40', '52.20', '52.40', ' ', '0.00', '52.20', '5', '52.40', '26', '0.00'], 
    ['1102', '亞泥', '11,560,108', '3,257', '535,613,275', '46.20', '46.65', '46.10', '46.20', '<p style= color:green>-</p>', '0.20', '46.20', '145', '46.25', '4', '12.80'],
    ['1103', '嘉泥', '813,103', '214', '13,648,077', '16.85', '16.85', '16.75', '16.85', ' ', '0.00', '16.80', '4', '16.85', '70', '16.05'], 
    ['1104', '環泥', '462,508', '192', '8,814,502', '19.05', '19.10', '19.00', '19.05', ' ', '0.00', '19.00', '167', '19.05', '60', '10.30'], 
    ['1108', '幸福', '62,002', '17', '430,713', '6.92', '6.98', '6.92', '6.97', '<p style= color:red>+</p>', '0.06', '6.97', '2', '6.98', '15', '0.00'], 
    ['1109', '信大', '740,250', '290', '13,421,423', '18.35', '18.35', '18.00', '18.10', '<p style= color:green>-</p>', '0.25', '18.05', '36', '18.10', '5', '7.33'], 
    ['1110', '東泥', '100,000', '18', '1,672,300', '16.80', '16.80', '16.70', '16.80', ' ', '0.00', '16.75', '1', '16.80', '6', '120.00'], 
    ['1201', '味全', '2,801,653', '1,252', '89,454,660', '31.65', '32.25', '31.55', '31.80', '<p style= color:red>+</p>', '0.10', '31.80', '73', '31.90', '2', '8.11'],
    '''
    def _clean_row(self, row):
        ''' Clean comma and spaces '''
        for index, content in enumerate(row):
            row[index] = re.sub(",", "", content.strip())
        return row

    def _record(self, stock_id, row):
        ''' Save row to csv file '''
        f = open('{}/{}.csv'.format(self.prefix, stock_id), 'a')
        cw = csv.writer(f, lineterminator='\n')
        cw.writerow(row)
        f.close()

    def _get_tse_data(self, date_tuple):
        date_str = '{0}{1:02d}{2:02d}'.format(date_tuple[0], date_tuple[1], date_tuple[2])
        url = 'http://www.twse.com.tw/exchangeReport/MI_INDEX'

        query_params = {
            'date': date_str,
            'response': 'json',
            'type': 'ALL',
            '_': str(round(time.time() * 1000) - 500)
        }

        # Get json data
        page = requests.get(url, params=query_params)

        if not page.ok:
            logging.error("Can not get TSE data at {}".format(date_str))
            return

        content = page.json()
        #print(content) 
        #logging.error("{}".format(content))

        # For compatible with original data
        date_str_mingguo = '{0}/{1:02d}/{2:02d}'.format(date_tuple[0] - 1911, date_tuple[1], date_tuple[2])
        date_str_AD = '{0}/{1:02d}/{2:02d}'.format(date_tuple[0], date_tuple[1], date_tuple[2])

        #print(content['data5']) #7/20/2019 cause can't get TSE stock info
        
        #print(content['data9'])
        logging.error("{}".format(content['data9']))
        print("Check TSE stock")
        for data in content['data9']:
            sign = '-' if data[9].find('green') > 0 else ''
            row = self._clean_row([
                #date_str_mingguo, # 日期
                date_str_AD, # 西元日期
                data[2], # 成交股數
                data[4], # 成交金額
                data[5], # 開盤價
                data[6], # 最高價
                data[7], # 最低價
                data[8], # 收盤價
                sign + data[10], # 漲跌價差
                data[3], # 成交筆數
                data[0],#代碼
                data[1]#名稱
            ])

            #print(self.list_stkidx_stkname)
            
            # Check stock idx if meets from Seymour's 
            for stkidx in self.list_stkidx_stkname:
            #    print("stkidx:",stkidx[0],"data[0]:",data[0])
                #2019/07/20 to check stock name not sotock idx
                if stkidx[1] == data[1]:
                    print("TSE stock idx:",data[0],"stock name:",data[1])
            #        self._record(data[0].strip(), row)

            #print("TSE stock idx:",data[0],"stock name:",data[1])
            #self._record(data[0].strip(), row)

        #for web refuse connection
        time.sleep(1)

    def _get_otc_data(self, date_tuple):
        date_str = '{0}/{1:02d}/{2:02d}'.format(date_tuple[0] - 1911, date_tuple[1], date_tuple[2])
        ttime = str(int(time.time()*100))
        url = 'http://www.tpex.org.tw/web/stock/aftertrading/daily_close_quotes/stk_quote_result.php?l=zh-tw&d={}&_={}'.format(date_str, ttime)
        page = requests.get(url)

        if not page.ok:
            logging.error("Can not get OTC data at {}".format(date_str))
            return

        #print(page)
        result = page.json()
        #print(result)

        if result['reportDate'] != date_str:
            logging.error("Get error date OTC data at {}".format(date_str))
            return

        print("Check OTC stock")
        date_str_AD = '{0}/{1:02d}/{2:02d}'.format(date_tuple[0], date_tuple[1], date_tuple[2])

        for table in [result['mmData'], result['aaData']]:
            for tr in table:
                row = self._clean_row([
                    #date_str,
                    date_str_AD,
                    tr[8], # 成交股數
                    tr[9], # 成交金額
                    tr[4], # 開盤價
                    tr[5], # 最高價
                    tr[6], # 最低價
                    tr[2], # 收盤價
                    tr[3], # 漲跌價差
                    tr[10], # 成交筆數
                    tr[0],#代碼
                    tr[1]#名稱
                ])

                # Check stock idx if meets from Seymour's 
                for stkidx_stkname in self.list_stkidx_stkname:
                    #2019/07/20 to check stock name not sotock idx                
                    #if stkidx[0] == tr[0]:
                    if stkidx_stkname[1] == tr[1]:
                        print("OTC stock idx:",tr[0],"stock name:",tr[1])
                        #self._record(tr[0], row)

        #for web refuse connection
        time.sleep(1)

    def get_data(self, date_tuple):
        print('Crawling {}'.format(date_tuple))
        self._get_tse_data(date_tuple)
        self._get_otc_data(date_tuple)

# Import personal library
strabspath=os.path.abspath(__file__)
strdirname=os.path.dirname(strabspath)
str_split=os.path.split(strdirname)
prevdirname=str_split[0]
dirnamelib=os.path.join(prevdirname,"lib")
dirnamelog=os.path.join(prevdirname,"log")

sys.path.append(dirnamelib)

import excelRW as excelrw
import readConfig as readConfig

configPath=os.path.join(strdirname,"config.ini")
localReadConfig = readConfig.ReadConfig(configPath)

delay_sec = localReadConfig.get_WorkSheet_Amy1210("delay_sec")

excel_file01 = localReadConfig.get_SeymourExcel("excelfile01") #"循環投資追蹤股"
excel_file02 = localReadConfig.get_SeymourExcel("excelfile02") #"波段投機追蹤股"
excel_file03 = localReadConfig.get_SeymourExcel("excelfile03") #"景氣循環追蹤股"
excel_file04 = localReadConfig.get_SeymourExcel("excelfile04") #"公用事業追蹤股"
excel_file05 = localReadConfig.get_SeymourExcel("excelfile05") #"追蹤股_增加遞補"2018/11/10

#str_year_year = localReadConfig.get_SeymourExcel("year_yearnum")
#str_year_month = localReadConfig.get_SeymourExcel("year_monthnum")

str_last_year_month_day = localReadConfig.get_SeymourExcel("last_year_month_day")
str_first_year_month_day = localReadConfig.get_SeymourExcel("first_year_month_day")

def get_stockidxname_SeymourExcel(excelfname):
    localexcelrw = excelrw.ExcelRW()

    print('將讀取Excel file:', excelfname, '的資料')
    # Excel file including path
    dirlog_ExcelFile=os.path.join(dirnamelog,excelfname)
    list_row_value_price=localexcelrw.readExcel(dirlog_ExcelFile)

    list_rtu_stockidxname=[]
    
    # Get  stock idx and company name from Excel files
    for list_row_value in list_row_value_price:
        list_stockidx_name=[list_row_value[0],list_row_value[1]]
        list_rtu_stockidxname.append(list_stockidx_name)

    return list_rtu_stockidxname

def get_stockidx_SeymourExcel(excelfname):
    localexcelrw = excelrw.ExcelRW()

    print('將讀取Excel file:', excelfname, '的資料')
    logging.error('將讀取Excel file: {}'.format(excelfname))

    # Excel file including path
    dirlog_ExcelFile=os.path.join(dirnamelog,excelfname)
    list_row_value_price=localexcelrw.readExcel(dirlog_ExcelFile)

    list_rtu_stockidx=[]
    
    # Get  stock idx and company name from Excel files
    for list_row_value in list_row_value_price:
        list_stockidx=[list_row_value[0]]
        list_rtu_stockidx.append(list_stockidx)

    return list_rtu_stockidx

def get_all_stockidx_SeymourExcel(list_excel_files):

    list_rtu_all_stockidx=[]

    for excel_file in list_excel_files:
        list_stockidx=get_stockidx_SeymourExcel(excel_file)
        list_rtu_all_stockidx.extend(list_stockidx)

    return list_rtu_all_stockidx    

def test_targetstock(list_excel_file,str_first_year_month_day):

    first_date = str_first_year_month_day.split(',')
    first_day = datetime(int(first_date[0]), int(first_date[1]), int(first_date[2]))
    print(first_day.year, first_day.month, first_day.day)

    # read each Excel file content ot get stock idx and name
    for excel_file in list_excel_file:
        list_stockidx=get_stockidx_SeymourExcel(excel_file)

        localcrawler = Crawler(list_stockidx)
        localcrawler._get_otc_data((first_day.year, first_day.month, first_day.day))
        localcrawler._get_tse_data((first_day.year, first_day.month, first_day.day))

# crawl TSE and OTC stock price to limit from first to last day 
def main_fromfirst_tolast(list_stockidx_stockname,str_first_y_m_d,str_last_y_m_d):
    # Set logging
    if not os.path.isdir('log'):
        os.makedirs('log')
    logging.basicConfig(filename='log/crawl-error.log',
        level=logging.ERROR,
        format='%(asctime)s\t[%(levelname)s]\t%(message)s',
        datefmt='%Y/%m/%d %H:%M:%S')

    # treat first date and last date
    first_date = str_first_y_m_d.split(',')
    last_date = str_last_y_m_d.split(',')
    #print(last_date)
    first_day = datetime(int(first_date[0]), int(first_date[1]), int(first_date[2]))
    last_day = datetime(int(last_date[0]), int(last_date[1]), int(last_date[2]))
    first_date_str_AD = '{0}/{1:02d}/{2:02d}'.format(first_date[0], int(first_date[1]), int(first_date[2]))
    last_date_str_AD = '{0}/{1:02d}/{2:02d}'.format(last_date[0], int(last_date[1]), int(last_date[2]))

    print("From First Date:", first_date_str_AD, "To Last Date:", last_date_str_AD)

    crawler = Crawler(list_stockidx_stockname)

    max_error = 5
    error_times = 0

    while error_times < max_error and first_day >= last_day:
        try:
            crawler.get_data((first_day.year, first_day.month, first_day.day))
            error_times = 0
        except:
            date_str = first_day.strftime('%Y/%m/%d')
            logging.error('Crawl raise error {}'.format(date_str))
            error_times += 1
            continue
        finally:
            first_day -= timedelta(1)


def main(list_stockidx_stockname):
    # Set logging
    if not os.path.isdir('log'):
        os.makedirs('log')
    logging.basicConfig(filename='log/crawl-error.log',
        level=logging.ERROR,
        format='%(asctime)s\t[%(levelname)s]\t%(message)s',
        datefmt='%Y/%m/%d %H:%M:%S')

    # Get arguments
    parser = argparse.ArgumentParser(description='Crawl data at assigned day')
    parser.add_argument('day', type=int, nargs='*',
        help='assigned day (format: YYYY MM DD), default is today')
    parser.add_argument('-b', '--back', action='store_true',
        help='crawl back from assigned day until 2004/2/11')
    parser.add_argument('-c', '--check', action='store_true',
        help='crawl back 10 days for check data')

    args = parser.parse_args()

    # Day only accept 0 or 3 arguments
    if len(args.day) == 0:
        first_day = datetime.today()
    elif len(args.day) == 3:
        first_day = datetime(args.day[0], args.day[1], args.day[2])
    else:
        parser.error('Date should be assigned with (YYYY MM DD) or none')
        return

    crawler = Crawler(list_stockidx_stockname)

    # If back flag is on, crawl till 2004/2/11, else crawl one day
    if args.back or args.check:
        # otc first day is 2007/04/20
        # tse first day is 2004/02/11

        last_day = datetime(2004, 2, 11) if args.back else first_day - timedelta(380)#timedelta(10)
        print(last_day)
        max_error = 5
        error_times = 0

        while error_times < max_error and first_day >= last_day:
            try:
                crawler.get_data((first_day.year, first_day.month, first_day.day))
                error_times = 0
            except:
                date_str = first_day.strftime('%Y/%m/%d')
                logging.error('Crawl raise error {}'.format(date_str))
                error_times += 1
                continue
            finally:
                first_day -= timedelta(1)
    else:
        crawler.get_data((first_day.year, first_day.month, first_day.day))


if __name__ == '__main__':
  #main()

  # Set logging
  if not os.path.isdir('log'):
      os.makedirs('log')

  logging.basicConfig(filename='log/crawl-error.log',
    level=logging.ERROR,
    format='%(asctime)s\t[%(levelname)s]\t%(message)s',
    datefmt='%Y/%m/%d %H:%M:%S')        

  # Get present time
  local_time = time.localtime(time.time())
  start_time = local_time
  print('Start Time is ', local_time.tm_year,'/',local_time.tm_mon,'/',local_time.tm_mday,' ',local_time.tm_hour,":",local_time.tm_min,":",local_time.tm_sec)
  logging.error('Begin Time:')

  #"循環投資追蹤股" #"波段投機追蹤股" #"景氣循環追蹤股" #"公用事業追蹤股"#"追蹤股_增加遞補"
  list_excel_file = [excel_file01,excel_file02,excel_file03,excel_file04]
  #list_excel_file = [excel_file03,excel_file04]
  #list_excel_file = [excel_file03]#"景氣循環追蹤股"
  #list_excel_file = [excel_file02,excel_file03]
  #list_excel_file = [excel_file04]
  #list_excel_file = [excel_file01,excel_file02,excel_file03,excel_file04]
  #list_excel_file = [excel_file05]

  #test_targetstock(list_excel_file,str_first_year_month_day)
  #list_all_stockidx=get_all_stockidx_SeymourExcel(list_excel_file)
  
  # Test class by excelRW.py
  localexcelrw = excelrw.ExcelRW()
  #list_all_stockidx=localexcelrw.get_all_stockidx_SeymourExcel(dirnamelog,list_excel_file)

  #for stockidx in list_all_stockidx:
  #    print("SotckIdx:{}, Idx_Len:{}".format(stockidx,len(stockidx)))
  #print(len(list_all_stockidx))    
  
  #2019/07/20 Cause 低波固收追蹤股 contnet of '代碼' column excexx 4 digits
  #           try to get stock name
  '''
    StockIdx:736.0, SotckName:國泰新興市場
    StockIdx:00740B, SotckName:富邦全球投等債
    StockIdx:00741B, SotckName:富邦全球高收債
    StockIdx:00751B, SotckName:元大AAA至A公司債
    StockIdx:00761B, SotckName:國泰A級公司債
    StockIdx:1101.0, SotckName:台泥
    StockIdx:1102.0, SotckName:亞泥
  '''
  list_all_stockidxname=localexcelrw.get_all_stockidxname_SeymourExcel(dirnamelog,list_excel_file)  
  #for stockidxname in list_all_stockidxname:
  #    print("StockIdx:{}, SotckName:{}".format(stockidxname[0], stockidxname[1]))
    
  # read each Excel file content ot get stock idx and name
  # 2018/08/23 Optimize flow to get all stock idxs from Excel files and then execute main_fromfirst_tolast()
  #main_fromfirst_tolast(list_all_stockidx,str_first_year_month_day,str_last_year_month_day)

  #2019/07/20
  main_fromfirst_tolast(list_all_stockidxname,str_first_year_month_day,str_last_year_month_day)

  #for excel_file in list_excel_file:
      #list_stockidx=get_stockidx_SeymourExcel(excel_file)
      #main_fromfirst_tolast(list_stockidx,str_first_year_month_day,str_last_year_month_day)

      #main(list_stockidx)
  
  # Get the last time
  local_time = time.localtime(time.time())
  print('Final Time is ', local_time.tm_year,'/',local_time.tm_mon,'/',local_time.tm_mday,' ',local_time.tm_hour,":",local_time.tm_min,":",local_time.tm_sec)
  logging.error('Finish Time:')
  #final_time = time.localtime(time.time())
  #str_StartTime = 'Start Time is '+str(start_time.tm_year) +'/'+str(start_time.tm_mon)+'/' +str(start_time.tm_mday)+' '+str(start_time.tm_hour)+":"+str(start_time.tm_min)+":"+str(start_time.tm_sec)
  #str_FinalTime = 'Final Time is ' + str(final_time.tm_year) + '/' + str(final_time.tm_mon)+'/'+str(final_time.tm_mday +' '+str(final_time.tm_hour)+":"+str(final_time.tm_min)+":"+str(final_time.tm_sec)
  #print('Start Time is '+str(start_time.tm_year) +'/'+str(start_time.tm_mon)+'/' +str(start_time.tm_mday)+' '+str(start_time.tm_hour)+":"+str(start_time.tm_min)+":"+str(start_time.tm_sec))
  #print('Final Time is ' + str(final_time.tm_year) + '/' + str(final_time.tm_mon)+'/'+str(final_time.tm_mday +' '+str(final_time.tm_hour)+":"+str(final_time.tm_min)+":"+str(final_time.tm_sec))