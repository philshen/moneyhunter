#2018/09/26 Intial test code related google drive
####################################################
import os,sys,re
from pydrive.drive import GoogleDrive

strabspath=os.path.abspath(__file__)
strdirname=os.path.dirname(strabspath)
str_split=os.path.split(strdirname)
prevdirname=str_split[0]
dirnamelib=os.path.join(prevdirname,"lib")
dirnamelog=os.path.join(prevdirname,"log")
dirdatafolder = os.path.join(prevdirname,'test','data')

sys.path.append(dirnamelib)
import readConfig as readConfig
import googleDrive as google_drive

configPath=os.path.join(strdirname,"config.ini")
localReadConfig = readConfig.ReadConfig(configPath)
str_candlestick_weekly_subfolder = localReadConfig.get_SeymourExcel("candlestick_weekly_subfolder")

str_candlestick_filepath = os.path.join(dirnamelog,str_candlestick_weekly_subfolder)#'D:\project\Python\TestEnv\log\20180923'
localgoogle_drive = google_drive.GoogleCloudDrive(str_candlestick_filepath)
#re_exp = r'\.jpg$'
#re_exp = r'\.txt$'
#list_filename = localgoogle_drive.querylocalfiles(re_exp)
#localgoogle_drive.purgelocalfiles(re_exp)

str_client_credentials = 'client_secrets.json'
str_dir_client_credentials = os.path.join(strdirname,str_client_credentials)
#print(str_dir_client_credentials)

gauth = localgoogle_drive.GDriveAuth(str_dir_client_credentials)
#Make GoogleDrive instance with Authenticated GoogleAuth instance
drive = GoogleDrive(gauth)

parent_id = '1JLEwwVtDeVH4vZm3hdaW_Dgbjr7TyiH0'#'MoneyHunter_**僅作個人紀錄之用，盈虧個人自負**'
#upload each file to Google driver under folder:str_candlestick_filepath
localgoogle_drive.upload_folder(drive, parent_id)

# pretest upload() upload_folder() in googleDrive.py
#for folder_name, subfolders, filenames in os.walk(str_candlestick_filepath):
#    print('folder_name:{} in {}'.format(folder_name,dirnamelog))
#    print('subfolders:{} in {}'.format(subfolders,folder_name))
    
#    for filename in filenames:
#        print('filename:{} in folder_name:{}'.format(filename,folder_name))

#folder_name = os.path.split(str_candlestick_filepath)[1]
#print('folder_name:{} in {}'.format(folder_name,str_candlestick_filepath))
# pretest upload() upload_folder() in googleDrive.py

#for uploadfilename in list_filename:
#    localgoogle_drive.uploadtoGDrive(drive,parent,GDfolderName,uploadfilename)
    
#rtu_dict_filelists = localgoogle_drive.listfolder(drive,parent)
#print(rtu_dict_filelists)