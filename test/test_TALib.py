# 2018/08/31 Initial to use pandas, matplotlib,TAlib
# 2018/09/01 Using pandas to calculate MA
#            Calculate golden and dead MA
#            Add class PandasDataAnalysis
#            Slove  matplotlib to show Chinese charter
# 2018/09/02 Match '---' and '--' in close price by using regular express
#            Caluclate uprate and downrate by rolling max min of dataframe
# 2018/09/03 add def file1_updownrate_LastMonthYear()
# 2018/09/04 dtype of close price icluding '---' and '--' is object except float64
#            Save as test_SeymourTarget.py
#            Add def file1_updownrate_LastMonthYear()
# 2018/09/05 Add def get_tradedays_dfinfo () and def file2_updownrate_QuarterYear
#            in class PandasDataAnalysis
#            Save as test_SeymourTarget.py
# 2018/09/06 Debug output CSV dulplicated rows.
#            Add def file2_updownrate_threeYearoneYear in class PandasDataAnalysis
# 2018/09/11 Add def MACrossOverDate_Intervalfromlastdate()       
#########################################################################

# -- coding: utf-8 --
import talib
import pandas as pd
import matplotlib.pyplot  as plt
import numpy as np
from datetime import datetime, timedelta
import time
import os
import sys

def plotchart(ptr_df,str_label,str_title):
    ptr_df.close.plot(color='#177A96', label=str_label)
    plt.title(str_title)
    plt.grid(True)
    plt.show()

def plotMAchart(list_ptr_df,list_label,str_title):
    plt.figure(figsize=(20,5))    
    plt.plot(list_ptr_df[0], color='#DE5B49', label=list_label[0], alpha=0.8, linewidth=3)
    plt.plot(list_ptr_df[1], color='#324D5C', label=list_label[1], alpha=0.8, linewidth=3)
    plt.plot(list_ptr_df[2], color='#46B29D', label=list_label[2], alpha=0.8, linewidth=3)
    plt.legend(loc='upper left')
    plt.grid(True)
    plt.title(str_title)
    plt.show()

def getMidvalue_lengthNum(inp_num):
    list_lengthNum = []
    # from 0 to inp_num not including inp_num
    for i in range(inp_num):
        list_lengthNum.append(i)
    
    #四捨五入中位數取其位置
    median_value = int(round(np.median(list_lengthNum)))
    return median_value

def plotMA05MA20MA30(data_frame,str_title):
    #在talib中，輸入輸出都需要用array，參數二則是你要選擇的n天，第三參數選擇均線的類型
    #chose start position from startpos_idx
    SMA_05 = talib.MA(np.array(data_frame.close), timeperiod=5, matype=0)
    SMA_20 = talib.MA(np.array(data_frame.close), timeperiod=20, matype=0)
    SMA_30 = talib.MA(np.array(data_frame.close), timeperiod=30, matype=0)

    #使用matplotlib繪圖之前先將array轉成DataFrame
    df_SMA_05 = pd.DataFrame(SMA_05, index = data_frame.index, columns = ['SMA05'])
    df_SMA_20 = pd.DataFrame(SMA_20, index = data_frame.index, columns = ['SMA20'])
    df_SMA_30 = pd.DataFrame(SMA_30, index = data_frame.index, columns = ['SMA30'])

    list_ptr_df = [df_SMA_05,df_SMA_20,df_SMA_30]
    list_label = ['SMA_05','SMA_20','SMA_30']
    plotMAchart(list_ptr_df,list_label,str_title)

def main01(csv_datafolder):
    # get date, open, high, low, close price and volume from csv file
    ##################index_col = [0] ###############
    # dataframe as below:
    #               volume   open   high    low  close CmpName
    #date
    #2017-07-03    773400  13.75  13.85  13.70  13.80      台航
    #2017-07-04   2154492  13.85  14.30  13.75  13.75      台航
    #2017-07-05    504327  13.75  13.80  13.55  13.75      台航
    csv_stockfile = pd.read_csv(csv_datafolder, header = None, encoding = 'cp950', 
                                usecols = [0,1,3,4,5,6,10], index_col = [0], 
                                names = ['date', 'volume','open', 'high', 'low', 'close', 'CmpName'],
                                parse_dates = [0],
                                date_parser = lambda x:pd.datetime.strptime(x,'%Y/%m/%d'))
    df = csv_stockfile.copy()

    #df.sort_index(ascending=0,inplace=True)
    df.sort_index(ascending=1,inplace=True)

    #print(df.to_string())
    # get row count after sort index
    print("original row counts: {}".format(len(df.index)))

    df_delduplicates = df.drop_duplicates()
    
    # get row count after delet duplicated row
    print("row counts after drop duplicated rows: {}".format(len(df_delduplicates.index)) )
    #print(df.duplicated().to_string())

    # row idx from 0 to extend 10 items; column idx from 1 to extend 5 items including close price
    #print(df_delduplicates.iloc[0:10,1:5])
    title = stkidx + df_delduplicates.iloc[0,5]
    #title = df_delduplicates.iloc[0,5]
    print(title)

    len_df_delduplicates = len(df_delduplicates.index)

    # get last day value
    ts_endday = df_delduplicates[-1:].index.tolist()[0]
    print(ts_endday)
    # Pandas: Convert Timestamp to datetime.date
    dt_endday = pd.Timestamp(ts_endday).date()
    #print(dt_endday)

    #subtract 90 days
    dt_startdate = dt_endday - timedelta(days=90)
    print("Start Date:{} End Date:{}".format(dt_startdate,dt_endday))
    #print(df_delduplicates.index[0])
    #print(df_delduplicates.to_string())

    startpos_idx = getMidvalue_lengthNum(len_df_delduplicates)
    #print("Half start position: {}".format(startpos_idx))
    plotMA05MA20MA30(df_delduplicates.iloc[startpos_idx:], title)

class PandasDataAnalysis:
    
    def __init__(self,stkidx,dirnamelog,dirdatafolder,str_first_year_month_day):
        
        FOLDER = dirdatafolder
        csv_datafolder = '{}/{}.csv'.format(FOLDER,stkidx)
        self.stkidx = stkidx
        self.dirnamelog = dirnamelog
        self.str_first_year_month_day = str_first_year_month_day

        # get date, open, high, low, close price and volume from csv file
        ################## remark index_col = [0] ###############
        ## then 'date' become a column name
        #          date    volume   open   high    low  close CmpName
        #0   2018-05-02   4715058  17.20  18.10  17.00  17.05      台航
        #1   2018-05-03    956738  16.85  16.95  16.65  16.85      台航
        #2   2018-05-04    612524  17.00  17.30  16.90  16.95      台航
        #3   2018-05-07    776401  17.15  17.25  16.70  16.75      台航

        # get date and close from csv file
        csv_stockfile = pd.read_csv(csv_datafolder, header = None, encoding = 'cp950', 
                            usecols = [0,3,4,5,6,10], #index_col = [0], 
                            names = ['date', 'open', 'high', 'low', 'close', 'CmpName'],
                            parse_dates = [0],
                            date_parser = lambda x:pd.datetime.strptime(x,'%Y/%m/%d'))
        df = csv_stockfile.copy()
        self.df = df
        #self.df.sort_index(ascending=1,inplace=True)

        # get row count after sort index
        print("original row counts: {}".format(len(self.df.index)))
        # check close price if match '---' and '--' or not
        if self.df['close'].dtype == np.float64:
            print("dtype of column 'close': {}".format(self.df['close'].dtype) )
        elif self.df['close'].dtype == np.object:
            print("column 'close' includes none trading records.")     

    def MACrossOver(self):
        # Get present time
        local_time = time.localtime(time.time())

        df_delduplicates = self.df.drop_duplicates()
        # get row count after delet duplicated row
        print("row counts after drop duplicated rows: {}".format(len(df_delduplicates.index)) )
        #print(df.duplicated().to_string())

        # sort pandas dataframe from one column
        df_delduplicates_sortasc = df_delduplicates.sort_values('date',ascending=1)
        # filter rows of pandas dataframe by timestamp column backward 90 days.
        df_delduplicates_back90D = df_delduplicates_sortasc.iloc[-90:,0:6]
        #print(df_delduplicates_back90D)

        # to add the calculated Moving Average as a new column to the right after 'Value'
        # to get 2 digitals after point by using np
        df_delduplicates_back90D['SMA_05'] = np.round(df_delduplicates_back90D['close'].rolling(window=5).mean(),2 )
        df_delduplicates_back90D['SMA_20'] = np.round(df_delduplicates_back90D['close'].rolling(window=20).mean(),2 )
        df_delduplicates_back90D['SMA_30'] = np.round(df_delduplicates_back90D['close'].rolling(window=30).mean(),2 )
        #print(df_delduplicates_back90D)

        # calculate SMA_05 Moving Average Crossover SMA_20
        previous_05 = df_delduplicates_back90D['SMA_05'].shift(1)
        previous_20 = df_delduplicates_back90D['SMA_20'].shift(1)
        crossing = (((df_delduplicates_back90D['SMA_05'] <= df_delduplicates_back90D['SMA_20']) & (previous_05 >= previous_20))
                    | ((df_delduplicates_back90D['SMA_05'] >= df_delduplicates_back90D['SMA_20']) & (previous_05 <= previous_20)))
        
        golden_crossing = ((df_delduplicates_back90D['SMA_05'] >= df_delduplicates_back90D['SMA_20']) 
                            & (previous_05 <= previous_20))
        dead_crossing = ((df_delduplicates_back90D['SMA_05'] <= df_delduplicates_back90D['SMA_20']) 
                            & (previous_05 >= previous_20))

        #crossing_dates = df_delduplicates_back90D.loc[crossing, 'date']
        #print(crossing_dates)
        crossing = df_delduplicates_back90D.loc[crossing]
        golden_crossing = df_delduplicates_back90D.loc[golden_crossing]
        dead_crossing = df_delduplicates_back90D.loc[dead_crossing]
        #print(crossing)
        print('MA Godlen CrossOver')
        print(golden_crossing)
        print('\n')
        print('MA Deaded CrossOver')
        print(dead_crossing)

        # Output CSV file including path
        filename_csv_macross=str(local_time.tm_mon)+str(local_time.tm_mday)+'_'+self.stkidx+'_'+"MACrossOver"+".csv"
        dirlog_csv_macross=os.path.join(self.dirnamelog,filename_csv_macross)
        golden_crossing.to_csv(dirlog_csv_macross, mode = 'w',sep='\t', header='Golden Crossing',encoding='cp950')
        dead_crossing.to_csv(dirlog_csv_macross, mode = 'a',sep='\t', header='Dead Crossing',encoding='cp950')

        # check both golden and dead MACrossOver is below 20 days
        timedelta_golden_crossing = self.MACrossOverDate_Interval_lastdate(golden_crossing)
        timedelta_dead_crossing = self.MACrossOverDate_Interval_lastdate(dead_crossing)
        if (timedelta_golden_crossing <= timedelta(days=20)).bool() | (timedelta_dead_crossing<= timedelta(days=16)).bool():
            #print('\n{} or {} <= 16 days.'.format(timedelta_golden_crossing,timedelta_dead_crossing))
            return 1
        else:
            #print('\n{} or {} > 16 days.'.format(timedelta_golden_crossing,timedelta_dead_crossing))    
            return 0
        
        
    
    # to calcualte interval days
    def MACrossOverDate_Interval_lastdate(self,df_macrossover):
        last_date = self.str_first_year_month_day.split(',')
        dt_last_date = datetime(int(last_date[0]), int(last_date[1]), int(last_date[2]))
        
        # get date of last row to calculate delta
        interval = dt_last_date - df_macrossover['date'].iloc[-1:]
        #if interval <= timedelta(15):
        #    print('{} from {} is {} day(s). '.format(dt_last_date.date(),i.date(),interval))
        #print('{} from {} is {} day(s). '.format(df_macrossover['date'].iloc[-1:],
        #                                        dt_last_date.date(),interval))
        return interval
            

    def file1_updownrate_LastMonthYear(self,valuerate):#"循環投資追蹤股"

        df_delduplicates_sortasc_tradeday = self.get_tradedays_dfinfo()
        # filter Pandas Dataframe rolling max min backward Month,Quarter,Year    
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_30D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=30).min()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_30D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=30).max()
        #df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_60D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=60).min()
        #df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_60D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=60).max()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_250D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=250).min()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_250D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=250).max()

        #calcuate raiserate_decreaserate
        df_delduplicates_sortasc_tradeday.loc[:,'uprate_01D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['low'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['low'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_01D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['high'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['high'].astype(float) )
    
        df_delduplicates_sortasc_tradeday.loc[:,'uprate_30D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_min_30D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_min_30D'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_30D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_max_30D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_max_30D'].astype(float) )

        df_delduplicates_sortasc_tradeday.loc[:,'uprate_250D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_min_250D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_min_250D'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_250D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_max_250D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_max_250D'].astype(float) )

        # 2018/08/31,0,0,---,---,---,---,---,0,4747,強生
        # 2018/09/03,0,0,---,---,---,---,---,0,4747,強生
        # 2018/09/04 last trade day maybe no trade happening, so forget assign date to index
        # Assigning an index column (and drop index column) to pandas dataframe to filter specific row
        #df_delduplicates_sortasc_tradeday_dateidx = df_delduplicates_sortasc_tradeday.set_index("date", drop = True)
        #print(df_delduplicates_sortasc_tradeday_dateidx)

        #df_delduplicates_sortasc_tradeday_dateidx_lastday = df_delduplicates_sortasc_tradeday_dateidx.loc[str_lastday,:]

        df_delduplicates_sortasc_tradeday_lastday = df_delduplicates_sortasc_tradeday.iloc[-1:,:]

        # flatten the lists then get its value like [['27.70']]-->27.7
        #list_temp = df_delduplicates_sortasc_tradeday_lastday[['close']].values.flatten()[0]
        #print( list_temp)
        
        list_rows_bothprices=[]
        #head_rows=["代碼","公司","市價","1Y下跌率","1M下跌率","Lastday下跌率",
                    #    "1Y上昇率","1M上昇率","Lastday上昇率",
                    #    "價格比","last trade day"]
        list_row_value_finalprice = [self.stkidx,
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['CmpName']],
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['close']],
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['downrate_250D']],
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['downrate_30D']],
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['downrate_01D']],
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['uprate_250D']],
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['uprate_30D']],
                                    #df_delduplicates_sortasc_tradeday_dateidx_lastday[['uprate_01D']],
                                     df_delduplicates_sortasc_tradeday_lastday[['CmpName']].values.flatten()[0],
                                    df_delduplicates_sortasc_tradeday_lastday[['close']].values.flatten()[0],
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_250D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_30D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_01D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_250D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_30D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_01D']].values.flatten()[0] *100),
                                    valuerate,
                                    df_delduplicates_sortasc_tradeday_lastday[['date']].values.flatten()[0]
                                    ]
        
        return list_row_value_finalprice

    # delete dataframe of both duplicates and nonetradeday
    def get_tradedays_dfinfo(self):

        df_delduplicates = self.df.drop_duplicates()
        # get row count after delet duplicated row
        print("row counts after drop duplicated rows: {}".format(len(df_delduplicates.index)) )

        # sort pandas dataframe from column 'date'
        df_delduplicates_sortasc = df_delduplicates.sort_values('date',ascending=1)

        # check clsoe price if includes '---' or '--' or not, but
        # 2018/09/04 dtype of close price icluding '---' and '--' is object except float64
        # convert value to string if value does have digitals
        if self.df['close'].dtype == np.object:
            # DataFrame filter close column by regex
            df_delduplicates_sortasc_nonetradeday = df_delduplicates_sortasc.loc[
                                                    df_delduplicates_sortasc['close'].str.contains(r'^-+-$')]
            #print(df_delduplicates_sortasc_nonetradeday)
            print("row counts with none trade: {}".format(len(df_delduplicates_sortasc_nonetradeday)) )

            # df_delduplicates_sortasc['close'] exclude (r'^-+-$')
            df_delduplicates_sortasc_tradeday = df_delduplicates_sortasc[~df_delduplicates_sortasc['close'].str.contains(r'^-+-$')]
        elif self.df['close'].dtype == np.float64:
            df_delduplicates_sortasc_tradeday = df_delduplicates_sortasc

        print("row counts with trade: {}".format(len(df_delduplicates_sortasc_tradeday)) )

        return df_delduplicates_sortasc_tradeday

    def file2_updownrate_QuarterYear(self,valuerate):#"波段投機追蹤股"

        df_delduplicates_sortasc_tradeday = self.get_tradedays_dfinfo()

        # filter Pandas Dataframe rolling max min backward Month,Quarter,Year    
        #df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_30D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=30).min()
        #df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_30D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=30).max()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_60D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=60).min()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_60D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=60).max()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_250D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=250).min()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_250D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=250).max()
        
        #calcuate raiserate_decreaserate
        df_delduplicates_sortasc_tradeday.loc[:,'uprate_01D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['low'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['low'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_01D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['high'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['high'].astype(float) )
    
        df_delduplicates_sortasc_tradeday.loc[:,'uprate_60D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_min_60D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_min_60D'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_60D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_max_60D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_max_60D'].astype(float) )

        df_delduplicates_sortasc_tradeday.loc[:,'uprate_250D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_min_250D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_min_250D'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_250D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_max_250D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_max_250D'].astype(float) )

        df_delduplicates_sortasc_tradeday_lastday = df_delduplicates_sortasc_tradeday.iloc[-1:,:]

        list_rows_bothprices=[]
        #head_rows=["代碼","公司","市價","1Q上昇率","1Y下跌率","Lastday上昇率",
                    #    "1Q下跌率","1Y上昇率","Lastday下跌率",
                    #    "價格比","last trade day"]
        list_row_value_finalprice = [self.stkidx,
                                    df_delduplicates_sortasc_tradeday_lastday[['CmpName']].values.flatten()[0],
                                    df_delduplicates_sortasc_tradeday_lastday[['close']].values.flatten()[0],
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_60D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_250D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_01D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_60D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_250D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_01D']].values.flatten()[0] *100),
                                    valuerate,
                                    df_delduplicates_sortasc_tradeday_lastday[['date']].values.flatten()[0]
                                    ]
        
        return list_row_value_finalprice

    def file3_updownrate_threeYearoneYear(self,pbr):#"景氣循環追蹤股"
        df_delduplicates_sortasc_tradeday = self.get_tradedays_dfinfo()

        # filter Pandas Dataframe rolling max min backward Quarter,Year, 3Year
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_60D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=60).min()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_60D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=60).max()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_250D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=250).min()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_250D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=250).max()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_min_730D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=730).min()
        df_delduplicates_sortasc_tradeday.loc[:,'rolling_max_730D'] = df_delduplicates_sortasc_tradeday['close'].astype(float).rolling(window=730).max()

        #calcuate raiserate_decreaserate
        df_delduplicates_sortasc_tradeday.loc[:,'uprate_60D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_min_60D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_min_60D'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_60D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_max_60D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_max_60D'].astype(float) )

        df_delduplicates_sortasc_tradeday.loc[:,'uprate_250D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_min_250D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_min_250D'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_250D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_max_250D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_max_250D'].astype(float) )

        df_delduplicates_sortasc_tradeday.loc[:,'uprate_730D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_min_730D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_min_730D'].astype(float) )
        df_delduplicates_sortasc_tradeday.loc[:,'downrate_730D'] = ( (df_delduplicates_sortasc_tradeday['close'].astype(float)-
                                                            df_delduplicates_sortasc_tradeday['rolling_max_730D'].astype(float))/
                                                            df_delduplicates_sortasc_tradeday['rolling_max_730D'].astype(float) )
    
        df_delduplicates_sortasc_tradeday_lastday = df_delduplicates_sortasc_tradeday.iloc[-1:,:]

        list_rows_bothprices=[]
        #head_rows=["代碼","公司","市價","3Y下跌率","1Y下跌率","1Q下跌率",
                    #    "3Y上昇率","1Y上昇率","1Q上昇率",
                    #    "PBR","last trade day"]
        list_row_value_finalprice = [self.stkidx,
                                    df_delduplicates_sortasc_tradeday_lastday[['CmpName']].values.flatten()[0],
                                    df_delduplicates_sortasc_tradeday_lastday[['close']].values.flatten()[0],
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_730D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_250D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['downrate_60D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_730D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_250D']].values.flatten()[0] *100),
                                    "%.3f%%" %(df_delduplicates_sortasc_tradeday_lastday[['uprate_60D']].values.flatten()[0] *100),
                                    pbr,
                                    df_delduplicates_sortasc_tradeday_lastday[['date']].values.flatten()[0]
                                    ]
        
        return list_row_value_finalprice



def file1_main(list_excel_Seymour,dirnamelog,dirdatafolder):#"循環投資追蹤股"
    # Get present time
    local_time = time.localtime(time.time())

    localexcelrw = excelrw.ExcelRW()

    for excel_Seymour in list_excel_Seymour:
        print('將讀取Excel file:', excel_Seymour, '的資料')

        # Excel file including path
        dirlog_ExcelFile=os.path.join(dirnamelog,excel_Seymour)
        # Read values of each row
        list_row_value_price=localexcelrw.readExcel(dirlog_ExcelFile)

        # Output CSV file including path
        filename_csv_bothprices=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"bothprices"+".csv"
        filename_csv_belowprice=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"belowprice"+".csv"
        dirlog_csv_bothprices=os.path.join(dirnamelog,filename_csv_bothprices)
        dirlog_csv_belowprice=os.path.join(dirnamelog,filename_csv_belowprice)

        # Declare output contents
        list_rows_bothprices=[]
        list_rows_belowprice=[]
        head_rows=["代碼","公司","市價","1Y下跌率","1M下跌率","Lastday下跌率",
                    "1Y上昇率","1M上昇率","Lastday上昇率","價格比"]
        list_rows_bothprices.append(head_rows)
        list_rows_belowprice.append(head_rows)

        dict_rows = {}
        # get  all CSV files name under data folder
        for list_row_value in list_row_value_price:
            # get key=idx value=價值比 to store in dict
            dict_rows[list_row_value[0]] = list_row_value[2]

        list_temp2 =[]#to store return list
        # by key=idx value=價值比
        for key,value in dict_rows.items():
            print("\nStkIdx:{}, ValueRate:{}".format(key,value))
            local_pdDA = PandasDataAnalysis(key,dirnamelog,dirdatafolder)
            list_temp = local_pdDA.file1_updownrate_LastMonthYear(value)
            list_temp2.append(list_temp)
            #print(list_temp2)

        list_rows_bothprices.extend(list_temp2)

        #print(list_rows_bothprices)
        print("Output file(s): {}".format(dirlog_csv_bothprices))
        # Output results to CSV files
        localexcelrw.writeCSVbyTable(dirlog_csv_bothprices,list_rows_bothprices)      

def file2_main(list_excel_Seymour,dirnamelog,dirdatafolder):#"波段投機追蹤股"
    # Get present time
    local_time = time.localtime(time.time())

    localexcelrw = excelrw.ExcelRW()

    for excel_Seymour in list_excel_Seymour:
        print('將讀取Excel file:', excel_Seymour, '的資料')

        # Excel file including path
        dirlog_ExcelFile=os.path.join(dirnamelog,excel_Seymour)
        # Read values of each row
        list_row_value_price=localexcelrw.readExcel(dirlog_ExcelFile)

        # Output CSV file including path
        filename_csv_bothprices=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"bothprices"+".csv"
        filename_csv_belowprice=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"belowprice"+".csv"
        dirlog_csv_bothprices=os.path.join(dirnamelog,filename_csv_bothprices)
        dirlog_csv_belowprice=os.path.join(dirnamelog,filename_csv_belowprice)

        # Declare output contents
        list_rows_bothprices=[]
        list_rows_belowprice=[]
        head_rows=["代碼","公司","市價","1Q上昇率","1Y下跌率","Lastday上昇率",
                    "1Q下跌率","1Y上昇率","Lastday下跌率","價格比"]
        list_rows_bothprices.append(head_rows)
        list_rows_belowprice.append(head_rows)

        dict_rows = {}
        # get  all CSV files name under data folder
        for list_row_value in list_row_value_price:
            # get key=idx value=價值比 to store in dict
            dict_rows[list_row_value[0]] = list_row_value[2]
                
        list_temp2 =[]#to store return list
        # by key=idx value=價值比
        for key,value in dict_rows.items():
            print("\nStkIdx:{}, ValueRate:{}".format(key,value))
            local_pdDA = PandasDataAnalysis(key,dirnamelog,dirdatafolder)
            list_temp = local_pdDA.file2_updownrate_QuarterYear(value)
            list_temp2.append(list_temp)
            #print(list_temp2)

        list_rows_bothprices.extend(list_temp2)

        #print(list_rows_bothprices)
        print("Output file(s): {}".format(dirlog_csv_bothprices))
        # Output results to CSV files
        localexcelrw.writeCSVbyTable(dirlog_csv_bothprices,list_rows_bothprices)

def file3_main(list_excel_Seymour,dirnamelog,dirdatafolder):#"景氣循環追蹤股"
    # Get present time
    local_time = time.localtime(time.time())

    localexcelrw = excelrw.ExcelRW()

    for excel_Seymour in list_excel_Seymour:
        print('將讀取Excel file:', excel_Seymour, '的資料')

        # Excel file including path
        dirlog_ExcelFile=os.path.join(dirnamelog,excel_Seymour)
        # Read values of each row
        list_row_value_price=localexcelrw.readExcel(dirlog_ExcelFile)

        # Output CSV file including path
        filename_csv_bothprices=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"bothprices"+".csv"
        filename_csv_belowprice=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"belowprice"+".csv"
        dirlog_csv_bothprices=os.path.join(dirnamelog,filename_csv_bothprices)
        dirlog_csv_belowprice=os.path.join(dirnamelog,filename_csv_belowprice)

        # Declare output contents
        list_rows_bothprices=[]
        list_rows_belowprice=[]
        head_rows=["代碼","公司","市價","3Y下跌率","1Y下跌率","1Q下跌率",
                    "3Y上昇率","1Y上昇率","1Q上昇率","PBR"]
        list_rows_bothprices.append(head_rows)
        list_rows_belowprice.append(head_rows)

        dict_rows = {}
        # get  all CSV files name under data folder
        for list_row_value in list_row_value_price:
            # get key=idx value=PBR to store in dict
            dict_rows[list_row_value[0]] = list_row_value[3]

        list_temp2 =[]#to store return list
        # by key=idx value=PBR
        for key,value in dict_rows.items():
            print("\nStkIdx:{}, PBR:{}".format(key,value))
            local_pdDA = PandasDataAnalysis(key,dirnamelog,dirdatafolder)
            list_temp = local_pdDA.file3_updownrate_threeYearoneYear(value)
            list_temp2.append(list_temp)
            #print(list_temp2)    

        list_rows_bothprices.extend(list_temp2)

        #print(list_rows_bothprices)
        print("Output file(s): {}".format(dirlog_csv_bothprices))
        # Output results to CSV files
        localexcelrw.writeCSVbyTable(dirlog_csv_bothprices,list_rows_bothprices)    


if __name__ == '__main__':

    strabspath=os.path.abspath(__file__)
    strdirname=os.path.dirname(strabspath)
    str_split=os.path.split(strdirname)
    prevdirname=str_split[0]
    dirnamelib=os.path.join(prevdirname,"lib")
    dirnamelog=os.path.join(prevdirname,"log")
    dirdatafolder = os.path.join(prevdirname,'test','data')

    sys.path.append(dirnamelib)

    import readConfig as readConfig
    import excelRW as excelrw
    import dataAnalysis as data_analysis

    configPath=os.path.join(strdirname,"config.ini")
    localReadConfig = readConfig.ReadConfig(configPath)

    excel_file01 = localReadConfig.get_SeymourExcel("excelfile01") #"循環投資追蹤股"
    excel_file02 = localReadConfig.get_SeymourExcel("excelfile02") #"波段投機追蹤股"
    excel_file03 = localReadConfig.get_SeymourExcel("excelfile03") #"景氣循環追蹤股"
    excel_file04 = localReadConfig.get_SeymourExcel("excelfile04") #"公用事業追蹤股"
    
    str_last_year_month_day = localReadConfig.get_SeymourExcel("last_year_month_day")
    str_first_year_month_day = localReadConfig.get_SeymourExcel("first_year_month_day")

    list_excel_Seymour = [excel_file03]
    
    list_all_stockidx = []
    
    # get all stock idx from excelfile03 #"景氣循環追蹤股"
    localexcelrw = excelrw.ExcelRW()
    #list_all_stockidx=localexcelrw.get_all_stockidx_SeymourExcel(dirnamelog,list_excel_Seymour)

    list_all_stockidx = [['2013',40],['5534',40]]#Test purpose:2013	中鋼構;5534	長虹
 
    for stkidx in list_all_stockidx:
        print('\n將讀取 {}/{}.csv 的資料作 90Days MA CrossOver分析。'.format(dirdatafolder,stkidx[0]))
        localDA = PandasDataAnalysis(stkidx[0],dirnamelog,dirdatafolder,str_first_year_month_day)
        rtu_value = localDA.MACrossOver()
        if bool(rtu_value):
            print('\nMACrossOver <= 20 days from last day.')
        else:
            print('\nMACrossOver > 20 days from last day.')

    # excute file1 #"循環投機追蹤股"
    #list_excel_Seymour = [excel_file01]    
    #file1_main(list_excel_Seymour,dirnamelog,dirdatafolder)

    # excute file2 #"波段投機追蹤股"
    #list_excel_Seymour = [excel_file02]    
    #file2_main(list_excel_Seymour,dirnamelog,dirdatafolder)
    
    # excute file3 #"景氣循環追蹤股"
    #list_excel_Seymour = [excel_file03]    
    #file3_main(list_excel_Seymour,dirnamelog,dirdatafolder)

    #for excel_Seymour in list_excel_Seymour:
    #    print('將讀取Excel file:', excel_Seymour, '的資料')

        # Excel file including path
    #    dirlog_ExcelFile=os.path.join(dirnamelog,excel_Seymour)
        # Read values of each row
    #    list_row_value_price=localexcelrw.readExcel(dirlog_ExcelFile)

        # Output CSV file including path
    #    filename_csv_bothprices=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"bothprices"+".csv"
    #    filename_csv_belowprice=str(local_time.tm_mon)+str(local_time.tm_mday)+excel_Seymour[8:14]+"belowprice"+".csv"
    #    dirlog_csv_bothprices=os.path.join(dirnamelog,filename_csv_bothprices)
    #    dirlog_csv_belowprice=os.path.join(dirnamelog,filename_csv_belowprice)

        # Declare output contents
    #    list_rows_bothprices=[]
    #    list_rows_belowprice=[]
    #    head_rows=["代碼","公司","市價","1Y下跌率","1M下跌率","Lastday下跌率",
    #                "1Y上昇率","1M上昇率","Lastday上昇率","價格比"]
    #    list_rows_bothprices.append(head_rows)
    #    list_rows_belowprice.append(head_rows)

    #    dict_rows = {}

        # get  all CSV files name under data folder
    #    for list_row_value in list_row_value_price:
            # get key=idx value=價值比 to store in dict
    #        dict_rows[list_row_value[0]] = list_row_value[2]

        #str_lastday = '2018-09-03'
        #print('Last trade date:'.format(str_lastday))

        #dict_rows = {'1258':'40', '4747':'40'}# for test output csv file

    #    list_temp2 =[]#to store return list
        # by key=idx value=價值比
    #    for key,value in dict_rows.items():
    #        print("\nStkIdx:{}, ValueRate:{}".format(key,value))
    #        local_pdDA = PandasDataAnalysis(key,dirnamelog,dirdatafolder)
    #        list_temp = local_pdDA.file1_updownrate_LastMonthYear(value)
    #        list_temp2.append(list_temp)
            #print(list_temp2)

    #    list_rows_bothprices.extend(list_temp2)

        #print(list_rows_bothprices)
    #    print("Output file(s): {}".format(dirlog_csv_bothprices))
        # Output results to CSV files
    #    localexcelrw.writeCSVbyTable(dirlog_csv_bothprices,list_rows_bothprices)    

    #FOLDER = 'data'
    #stkidx = '1258'#其祥-KY#'2617'#台航
    
    #csv_datafolder = '{}/{}.csv'.format(FOLDER,stkidx)
    ##main01(csv_datafolder)

    ##local_pdDA = PandasDataAnalysis(stkidx,dirnamelog,dirdatafolder)
    ##local_pdDA.MACrossOver()