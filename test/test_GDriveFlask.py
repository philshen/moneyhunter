#2018/09/26 Intial test code related google drive
#2018/11/18 For flask demo webpage on Heroku
####################################################
import os,sys,re
from pydrive.drive import GoogleDrive

strabspath=os.path.abspath(__file__)
strdirname=os.path.dirname(strabspath)
str_split=os.path.split(strdirname)
prevdirname=str_split[0]
dirnamelib=os.path.join(prevdirname,"lib")
dirnamelog=os.path.join(prevdirname,"log")
dirdatafolder = os.path.join(prevdirname,'test','data')

sys.path.append(dirnamelib)
import readConfig as readConfig
import googleDrive as google_drive

configPath=os.path.join(strdirname,"config.ini")
localReadConfig = readConfig.ReadConfig(configPath)
str_candlestick_weekly_subfolder = localReadConfig.get_SeymourExcel("candlestick_weekly_subfolder")

str_candlestick_filepath = os.path.join(dirnamelog,str_candlestick_weekly_subfolder)#'D:\project\Python\TestEnv\log\20180923'
localgoogle_drive = google_drive.GoogleCloudDrive(str_candlestick_filepath)
#re_exp = r'\.jpg$'
#re_exp = r'\.txt$'
#list_filename = localgoogle_drive.querylocalfiles(re_exp)
#localgoogle_drive.purgelocalfiles(re_exp)

str_client_credentials = 'client_secrets.json'
str_dir_client_credentials = os.path.join(strdirname,str_client_credentials)
#print(str_dir_client_credentials)

gauth = localgoogle_drive.GDriveAuth(str_dir_client_credentials)
#Make GoogleDrive instance with Authenticated GoogleAuth instance
drive = GoogleDrive(gauth)

parent_id = '1JLEwwVtDeVH4vZm3hdaW_Dgbjr7TyiH0'#'MoneyHunter_**僅作個人紀錄之用，盈虧個人自負**'
#upload each file to Google driver under folder:str_candlestick_filepath
#localgoogle_drive.upload_folder(drive, parent_id)
title='put_波段_5225_東科-KY.jpg'

#file_id=localgoogle_drive.id_of_title(drive,title,parent_id)
#print(file_id)

list_foldersfiles=localgoogle_drive.listfolder(drive,parent_id)
#list_file=localgoogle_drive.list_folder(drive,parent_id)
'''{'id': '14mFtlnxOE79JMCIIaC8-PNx9QZA8GBko', 'title': '20181118', 
    'list':[{'title': 'put_波段_6176_瑞儀.jpg', 'title1': 'https://drive.google.com/file/d/1GaM7zn9_QFbH0Wg2_u9gOd6UkNjA7Li4/view?usp=drivesdk', 
    'modifiedDate': '2018-11-17T08:25:05.280Z'}, .....
    {'title': '僅作個人紀錄，盈虧個人自負', 'title1': 'https://docs.google.com/document/d/17GYHTa7g-umwQyodd9tFWpNbnkxydsN5Bv1t7iNfU2o/edit?usp=drivesdk', 'modifiedDate': '2018-09-24T04:38:07.577Z'}, {'id': '1Bx-lHazUXYhxQNe9zupA5TrA_oJWX97a', 'title': '20180923', 'list': [{'title': 'put_波段_6202_盛群.jpg', 'title1': 'https://drive.google.com/file/d/1rBJZFsYChQ4B4QqWW5KRVqXwp_fhgaVt/view?usp=drivesdk', 'modifiedDate': '2018-09-29T13:02:48.014Z'}, {'title': 'put_波段_5371_中光電.jpg', 'title1': 'https://drive.google.com/file/d/1idCEKBuiMoTvsJCktTKvLKOrC6d7k8_j/view?usp=drivesdk', 'modifiedDate': '2018-09-29T13:02:36.092Z'}, {'title': 'put_波段_2488_漢平.jpg', 'title1': 'https://drive.google.com/file/d/1U7uR7dvqabrJowxVNNmSQ8qtGKmTZuJU/view?usp=drivesdk', 'modifiedDate': '2018-09-29T13:02:31.419Z'}, {'title': 'put_景氣_2617_台航.jpg', 'title1': 'https://drive.google.com/file/d/163XJWtgm7Y4c5IRDHTfkIaYBQkfcGD33/view?usp=drivesdk', 
    'modifiedDate': '2018-09-29T13:02:26.760Z'},#no key:"list"
'''
#print(list_foldersfiles)
#with open(os.path.join(dirnamelog,'GD_foldersfiles.txt'), "w") as text_file:
#    text_file.write("{}".format(list_foldersfiles))
for dic_folderfiles in list_foldersfiles:
    try:
        for list_files in dic_folderfiles['list']:
            print('title:{}, modifiedDate:{}'.format(list_files['title'],list_files['modifiedDate']))
            print('URL:{}'.format(list_files['title1']))
    except KeyError as error:
            print(error)
    except Exception as exception:
            print(exception)
        
print(len(list_foldersfiles))    