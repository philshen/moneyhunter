#2018/10/15 Intial test code to crawl weekly targets csv files.
#2018/10/27 Adapte from test_sqilte3.py for daily crawl.
#2019/01/10 Excute this code meet from Mon. to Fri unreamrk below.
#2019/01/28 Adapt frmo test_crawl2sqlite.
####################################################
import os,sys,time,logging,datetime
import urllib.request
from lxml import html
import httplib2
from apiclient import discovery

class Flag:
    auth_host_name = 'localhost'
    noauth_local_webserver = False
    auth_host_port = [8080, 8090]
    logging_level = 'ERROR'

try:
    import argparse

    # flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
    flags = Flag()
except ImportError:
    flags = None

strabspath=os.path.abspath(__file__)
strdirname=os.path.dirname(strabspath)
str_split=os.path.split(strdirname)
prevdirname=str_split[0]
dirnamelib=os.path.join(prevdirname,"lib")
dirnamelog=os.path.join(prevdirname,"log")
dirdatafolder = os.path.join(prevdirname,'test','data')

sys.path.append(dirnamelib)
import excelRW as excelrw
import readConfig as readConfig
import googleDrive as google_drive
import twstockInfo as twstock_info

# Set logging
if not os.path.isdir('log'):
    os.makedirs('log')

logging.basicConfig(filename='log/crawl-error.log',level=logging.ERROR,
                    format='%(asctime)s\t[%(levelname)s]\t%(message)s',
                    datefmt='%Y/%m/%d %H:%M:%S')

# Get present time
t0 = time.time()
local_time = time.localtime(t0)
start_time = local_time
print('Start Time is ', local_time.tm_year,'/',local_time.tm_mon,'/',local_time.tm_mday,' ',local_time.tm_hour,":",local_time.tm_min,":",local_time.tm_sec)
logging.error('Begin Time:')

configPath=os.path.join(strdirname,"config.ini")
localReadConfig = readConfig.ReadConfig(configPath)

url_moneyhunter =localReadConfig.get_SeymourExcel('url_moneyhunterblog')#'http://twmoneyhunter.blogspot.com/'
#2019/1/10(Thu) excute this code dosen't meet from Mon. to Fri unremak below.
str_last_year_month_day = localReadConfig.get_SeymourExcel("last_year_month_day")
str_first_year_month_day = localReadConfig.get_SeymourExcel("first_year_month_day")

#2019/1/10(Thu) excute this code meet from Mon. to Fri unreamrk below.
#str_last_year_month_day = datetime.date.today().strftime('%Y,%m,%d')# ex:2018,10,16 get today date form system
#str_first_year_month_day = datetime.date.today().strftime('%Y,%m,%d')# ex:2018,10,16 get today date form system

xpath_url_file01 = '//*[@id="LinkList1"]/div/ul/li[4]/a/@href'#循環投資
xpath_url_file02 = '//*[@id="LinkList1"]/div/ul/li[3]/a/@href'#波段投機
xpath_url_file03 = '//*[@id="LinkList1"]/div/ul/li[5]/a/@href'#景氣循環
xpath_url_file04 = '//*[@id="LinkList1"]/div/ul/li[6]/a/@href'#20190126低波固收

#Python urllib urlopen not working
#https://stackoverflow.com/questions/25863101/python-urllib-urlopen-not-working
###########################################
with urllib.request.urlopen(url_moneyhunter) as response:
    raw = response.read()
html_doc = html.fromstring(raw)

credential_dir = os.getcwd()

localgoogle_drive = google_drive.GoogleDrivebyFileID(dirnamelog,flags)
credentials = localgoogle_drive.get_credentials(credential_dir)
http = credentials.authorize(httplib2.Http())
service = discovery.build('drive', 'v3', http=http)

fileid_file01 = localgoogle_drive.ggdrive_fileid(html_doc,xpath_url_file01)
fileid_file02 = localgoogle_drive.ggdrive_fileid(html_doc,xpath_url_file02)
fileid_file03 = localgoogle_drive.ggdrive_fileid(html_doc,xpath_url_file03)
fileid_file04 = localgoogle_drive.ggdrive_fileid(html_doc,xpath_url_file04)

list_xlsfile, excel_file01 = localgoogle_drive.check_xlsfile_MHunterblog_logfolder(service,fileid_file01,dirnamelog)#"循環投資追蹤股"
list_xlsfile, excel_file02 = localgoogle_drive.check_xlsfile_MHunterblog_logfolder(service,fileid_file02,dirnamelog)#"波段投機追蹤股"
list_xlsfile, excel_file03 = localgoogle_drive.check_xlsfile_MHunterblog_logfolder(service,fileid_file03,dirnamelog)#"景氣循環追蹤股"
list_xlsfile, excel_file04 = localgoogle_drive.check_xlsfile_MHunterblog_logfolder(service,fileid_file04,dirnamelog)#"公用事業追蹤股"
excel_file05 = localReadConfig.get_SeymourExcel("excelfile05") #"追蹤股_增加遞補"2018/11/10

#"循環投資追蹤股" #"波段投機追蹤股" #"景氣循環追蹤股" #"公用事業追蹤股"#"追蹤股_增加遞補"
list_excel_file = [excel_file01,excel_file02,excel_file03,excel_file04]
#list_excel_file = [excel_file05]
#list_excel_file = [excel_file03,excel_file04]
#list_excel_file = [excel_file03]#"景氣循環追蹤股"
#list_excel_file = [excel_file02,excel_file03]
#list_excel_file = [excel_file01]
#list_excel_file = [excel_file01,excel_file02]

# Test class by excelRW.py
# read each Excel file content ot get stock idx and name
localexcelrw = excelrw.ExcelRW()
list_all_stockidx=localexcelrw.get_all_stockidx_SeymourExcel(dirnamelog,list_excel_file)

mongo_host = localReadConfig.get_MongoDB('mongo_host')
mongo_db = localReadConfig.get_MongoDB('mongo_db')
mongo_collection = localReadConfig.get_MongoDB('mongo_collection')
mongo_username = localReadConfig.get_MongoDB('mongo_username') 
mongo_password = localReadConfig.get_MongoDB('mongo_password')

localtwstock_info_db = twstock_info.DB_mongo()
# create a database connection

collection = localtwstock_info_db.connect_mongo(mongo_host,mongo_db,
                    mongo_collection,mongo_username,mongo_password)

# only for testing - of course do not do drop() in production
#collection.drop()
  
# 2018/10/27 Optimize flow to get all stock idxs from Excel files and then execute main_fromfirst_tolast()
localtwstock_info = twstock_info.CrawlTSEOTC([],os.path.join(strdirname,"log"),str_first_year_month_day,str_last_year_month_day)
localtwstock_info.crawl_date_fromfirst_tolast_tomongodb(list_all_stockidx,collection)


# Get the last time
t1 = time.time()
local_time = time.localtime(t1)
print('Final Time is ', local_time.tm_year,'/',local_time.tm_mon,'/',local_time.tm_mday,' ',local_time.tm_hour,":",local_time.tm_min,":",local_time.tm_sec)
msg = 'Collection(s) insertion in {:.2f} seconds.'
print(msg.format(t1 - t0))
logging.info(msg.format(t1 - t0))
logging.error('Finish Time:')